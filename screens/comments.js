import React, { Component } from 'react';
import { AlertIOS, AsyncStorage, ActivityIndicator, FlatList, Text, View, StyleSheet, Button, Image, SearchBar, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');
import Loading from 'react-native-loader-overlay';

class Comments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: [],
      comment: '',
      token: '',
      counter: true,
      newComment: '',
      defaultId: ''
    }
  }
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}


async componentWillMount() {
  const userToken = await AsyncStorage.getItem('userToken');
  let commentId = JSON.stringify(this.props.navigation.state.params.id) || {}
  let idComment = JSON.parse(commentId);

  this.state.token = userToken; 
  // alert(abc)
  fetch('https://bbn-eg.com/broker/api/comments/all/'+idComment+'?api_token='+userToken)
    .then((response) => response.json())
    .then((findresponse) => {
      this.setState({
        isLoading: false,
        dataSource: findresponse['comment'],
        counter:false,
        defaultId: findresponse['project_id']
      })       
   //   alert(JSON.stringify(this.state.dataSource));
    }) 
}

openThread(){
  this.loading = Loading.show({
    color: '#2CA095',
    size: 10,
    overlayColor: 'rgba(0,0,0,0.5)',
    closeOnTouch: false,
    loadingType: 'Bubbles'
  })

  if (this.state.newComment != '') {

//  alert(this.state.defaultId);
    fetch('https://bbn-eg.com/broker/api/comments/?api_token=' + this.state.token, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        project_id: this.state.defaultId,
        thread_id: 0,
        comment: this.state.newComment
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        AlertIOS.alert(
          responseJson['message'],
        );
        this.setState({
          newComment: ''
        })
        Loading.hide(this.loading);
        this.componentWillMount();
      })
      .catch((error) => {
        console.error(error);
      });
  } else {
    
    Loading.hide(this.loading);
    AlertIOS.alert(
      "Please Enter Comment First!",
    );
  }


}

postComment(threadID, projectID) {

  this.loading = Loading.show({
    color: '#2CA095',
    size: 10,
    overlayColor: 'rgba(0,0,0,0.5)',
    closeOnTouch: false,
    loadingType: 'Bubbles'
  })

  if (this.state.comment != '') {
    fetch('https://bbn-eg.com/broker/api/comments/?api_token=' + this.state.token, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        project_id: projectID,
        thread_id: threadID,
        comment: this.state.comment
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        AlertIOS.alert(
          responseJson['message']
        );
        this.setState({
          comment: ''
        })
        Loading.hide(this.loading);
        this.componentWillMount();
      })
      .catch((error) => {
        console.error(error);
      });
  } else {
    
    Loading.hide(this.loading);
    AlertIOS.alert(
      "Please Enter Comment First!"
    );
  }
}

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
        </View>
      );
    }
    return (
      <ScrollView>
 
  <Text style={{backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center', color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:20, fontWeight:'bold'}} >Comments</Text>
              <View style={styles.footer}>
              <View style={styles.inputContainer}>
                <TextInput style={styles.inputs}
                    multiline={true}
                    placeholder="Open New Chat"
                    onChangeText={(comment) => this.setState({ newComment: comment })}
                    numberOfLines={6}
                    underlineColorAndroid='transparent'
                    value={this.state.newComment}/>
              </View>
                <TouchableOpacity style={styles.btnSend} onPress={() => this.openThread()}>
                  <Image source={{uri:"https://png.icons8.com/small/75/ffffff/filled-sent.png"}} style={styles.iconSend}  />
                </TouchableOpacity>
            </View>



       
                   <FlatList listKey="Parent" style={{ flex: 1 }} data={this.state.dataSource} renderItem={({ item }) => <View>
                   <View style={{ width: WIDTH, height: 'auto' }}>
                     <View style={{ paddingBottom: 10, flexDirection: 'row', justifyContent: 'flex-start', paddingStart: 15, width: WIDTH - 30 }}>
                       <View>
                         <Image source={{ uri: item.sender_image }} style={{ padding: 10, width: 40, height: 42 }} />
                       </View>
                       <View style={{ flex: 1, flexWrap: 'wrap' }}>
                         <Text style={{ paddingStart: 12, marginTop: 10, fontFamily: "29LTKaff-Regular", fontSize: 15, fontWeight: 'normal', fontSize: 18, color: 'black' }}><Text style={{ fontFamily: "29LTKaff-Regular", fontSize: 18, fontWeight: 'bold', color: 'black' }}>{item.sender_name}</Text> {item.main_comment}
                         </Text>
                       </View>
                     </View>
         
                     <FlatList listKey="2.1" style={{ flex: 1 }} data={item.comments} renderItem={({ item }) => <View>
                       <View style={{ paddingTop: 10, paddingStart: 45, width: WIDTH - 60 }}>
                         <View style={{ paddingBottom: 20, flexDirection: 'row', justifyContent: 'flex-start', paddingStart: 15, width: WIDTH - 80 }}>
                           <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                             <Image source={{ uri: item.user_image }} style={{ padding: 10, width: 40, height: 42 }} />
                           </View>
                           <View style={{ flex: 1, flexWrap: 'wrap' }}>
                             <Text style={{ paddingStart: 12, marginTop: 7, fontFamily: "29LTKaff-Regular", fontWeight: 'normal', fontSize: 16, color: 'black' }}><Text style={{ fontFamily: "29LTKaff-Regular", fontSize: 18, fontWeight: 'bold', color: 'black' }}>{item.username}</Text> <Text style={{ fontFamily: "29LTKaff-Regular", fontSize: 16, fontWeight: 'bold', color: '#4f67ab' }}></Text>{item.comment}
                             </Text>
                           </View>
                         </View> 
                       </View>  
                     </View>} listKey={(item, index) => 'Nested' + index.toString()} />
         
                    

                     <View style={styles.footer}>
              <View style={styles.inputContainer}>
                <TextInput style={styles.inputs}
                multiline={true}
                    placeholder="Enter your reply ..."
                    underlineColorAndroid='transparent'
                    //value={this.state.comment}
                    onChangeText={(comment) => this.setState({ comment: comment })}/>
                    {
                        /* 
                    onChangeText={(name_address) => this.setState({name_address})} */
                    }
              </View>
    
                <TouchableOpacity style={styles.btnSend} onPress={() => this.postComment(item.id, item.project_id)}>
                  <Image source={{uri:"https://png.icons8.com/small/75/ffffff/filled-sent.png"}} style={styles.iconSend}  />
                </TouchableOpacity>
            </View>
                   </View>
         
                 </View>} keyExtractor={(item, index) => item.toString()} />
           


       
      </ScrollView>
    );
  }
}
export default Comments;
const styles = StyleSheet.create({

  footer:{
    flexDirection: 'row',
    height:60,
    paddingHorizontal:10,
    marginTop:15,
    marginBottom:15
  },
  btnSend:{
    backgroundColor:"#2CA095",
    width:37,
    height:37,
    borderRadius:360,
    alignItems:'center',
    justifyContent:'center',
    fontFamily: "29LTKaff-Regular",
    marginTop:10

 
  },
  iconSend:{
    width:25,
    height:25,
    alignSelf:'center',
    marginLeft:5
   },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    height:50,
    flexDirection: 'row',
    alignItems:'center',
    flex:1,
    fontFamily: "29LTKaff-Regular",
    marginRight:10
  },
  inputs:{
    height:50,
    marginLeft:15,
    paddingLeft:15,
    borderBottomColor: '#FFFFFF',
    fontFamily: "29LTKaff-Regular",
    flex:1,
    borderColor:'#2CA095',
    borderWidth:1,
    paddingTop:15,
    borderBottomLeftRadius: 12, borderTopRightRadius: 12,
  
  },
  item: {
    marginVertical: 15,
   // flex: 1,
    flexDirection: 'row',
    backgroundColor:"#ffffff",
    fontFamily: "29LTKaff-Regular",
    borderRadius:0,
  },
});  
