import React, { Component } from 'react';
import { AsyncStorage, Picker, RefreshControl, ActivityIndicator, FlatList, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');

import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/Ionicons';

import { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['location', 'title'];

import ViewMoreText from 'react-native-view-more-text';

class Projectdetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      developer: [],
      projects: [],
      dataSource: [],
      refreshing: false,
      searchTerm: '',
      projectLocation: [
        {
          label: 'Project Location',
          value: '',
        },
        {
          label: 'Cairo, Egypt',
          value: 'Cairo, Egypt',
        },
        {
          label: 'Alexandria, Egypt',
          value: 'Alexandria, Egypt',
        },
        {
          label: 'Luxor, Egypt',
          value: 'Luxor, Egypt',
        },
        {
          label: 'Giza, Egypt',
          value: 'Giza, Egypt',
        },
        {
          label: 'Aswan, Egypt',
          value: 'Aswan, Egypt',
        },
        {
          label: 'Hurgada, Egypt',
          value: 'Hurgada, Egypt',
        },
        {
          label: 'Asyut, Egypt',
          value: 'Asyut, Egypt',
        }
      ],
      itemValue: null
    };
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.componentWillMount();
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  getScreen(item) {
   this.props.navigation.navigate('Projectlocationdetails', { id: item })
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    let projectId = JSON.stringify(this.props.navigation.state.params.id) || {}
    fetch('https://bbn-eg.com/broker/api/developers/'+projectId+'/?api_token='+userToken)
      .then((response) => response.json())
      .then((findresponse) => {
        this.setState({
          isLoading: false,
          refreshing: false,
          developer: findresponse.developer,
          projects: findresponse.projects
        })
      })
  }
  
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}

  renderViewMore(onPress){
    return(
      <Text style={{textAlign:'right', paddingRight: 10, fontSize:12}} onPress={onPress}>View more</Text>
    )
    }
  
    renderViewLess(onPress){
      return(
        <Text style={{textAlign:'right', paddingRight: 10, fontSize:12}} onPress={onPress}>View less</Text>
      )
    }

  render() {
    const filteredEmails = this.state.projects.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
    const pickerStyle = {
      inputIOS: {
          color: 'black',
          paddingTop: 13,
          paddingHorizontal: 10,
          paddingBottom: 10,
          fontFamily: "29LTKaff-Regular",
          fontSize:14,
          height:38

      },
      inputAndroid: {
          color: 'black',
      },
      placeholderColor: 'grey',
      underline: { borderTopWidth: 0 },
      icon: {
          position: 'absolute',
          backgroundColor: 'transparent',
          borderTopWidth: 5,
          borderTopColor: '#00000099',
          borderRightWidth: 5,
          borderRightColor: 'transparent',
          borderLeftWidth: 5,
          borderLeftColor: 'transparent',
          width: 0,
          height: 10,
          top: 18,
          right: 15,
      },
  };
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
        </View>
      ); 
    }
    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          colors={['#2CA095']}
        />
      }>

          <FlatList data={this.state.developer} renderItem={({ item }) =>
            <View>
              <View style={{ borderBottomColor: '#bdbdbd', borderBottomWidth: 1, backgroundColor: '#dbdcdd', flexDirection: 'row', marginTop: 0, justifyContent: 'center', width: WIDTH }}>
                <View style={{ flexDirection: 'row', marginTop:10 }}>
                    <View elevation={3} style={{ borderWidth: 1, borderColor: '#bdbdbd', borderBottomLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: "#2CA095",
    fontFamily: "29LTKaff-SemiBold", justifyContent: 'center', alignItems: 'center', marginLeft: 16, marginTop: 6, marginBottom: 6, backgroundColor: '#ffffff', width: 150, height: 100 }}>
                      <View style={{ justifyContent: 'center', alignItems: 'center', width: WIDTH / 3, height: WIDTH / 2 - 60 }}>
                        <Image source={{ uri: item.photo }} style={{ borderRadius:12, width: WIDTH / 3, height:WIDTH / 2 - 60 , resizeMode: 'contain', flexWrap: 'wrap' }} />
                      </View>
                    </View>
              
                  <View style={{ marginLeft: 5, borderRadius: 12, width: WIDTH / 2 , height: 'auto'}}>
                    <Text style={{ marginTop: 15, paddingStart: 10, fontFamily: "29LTKaff-Regular", fontSize: 19, color: 'black', fontWeight: '900' }}>{item.title}<Text style={{marginTop: 15, paddingStart: 10, fontFamily: "29LTKaff-Regular", fontSize: 15, color: 'black', fontWeight: '700'}}> ({item.projects} projects)</Text></Text>               
                       <ViewMoreText style={{ color:'#2CA095', maringStart: 10, width:WIDTH/3 - 10}}
          numberOfLines={3}
          renderViewMore={this.renderViewMore}
          renderViewLess={this.renderViewLess}
          textStyle={{ paddingStart: 10, fontFamily: "29LTKaff-Regular", fontSize: 13, color: 'black', fontWeight: 'normal', paddingTop: 2, padding: 5, margin: 0}}
        >
          <Text style={{ fontFamily: "29LTKaff-Regular", fontSize: 13, }}>
          {item.description} </Text>
        </ViewMoreText>
                  </View>
                </View>
              </View>
            </View>
          } keyExtractor={(item, index) => index.toString()} />
       
        <View style={{ justifyContent: 'center', width: WIDTH, height: 50, backgroundColor:'#f1f1f1' }}>

        <View style={{ width: 60, height: 20, position: 'absolute', left: 35, right: 'auto', top: 'auto', bottom: 10 }}>
          <Text style={{color:'black', fontSize:16, fontFamily:'29LTKaff-Regular', width: 55, height: 20}}>
          Search
          </Text>
              </View>

          <TextInput
            style={{
              width:WIDTH - 140, paddingStart:15, position: 'absolute', left:90, right:'auto', justifyContent: 'center', flexDirection:'row', fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 16, paddingBottom: 5, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30
            }}
            onChangeText={(term) => { this.searchUpdated(term) }}
            placeholderTextColor={'grey'}
            underlineColorAndroid='black'
            autoCorrect={false}
          />
           <View style={{ marginStart:5, justifyContent:"center", flexDirection:"row",  }}>
            <View style={{borderBottomColor: 'grey', borderBottomWidth: 2, width:WIDTH - 150, paddingStart:15, position: 'absolute', left:90, right:'auto', justifyContent: 'center', ontSize: 16, paddingBottom: 5, paddingTop: 5, fontWeight: '400', textAlign: 'left', paddingStart: 30 }} />
            </View>

            <View style={{ width: 20, height: 20, position: 'absolute', left: 'auto', right: 32, top: 'auto', bottom: 10 }}>
            <Icon name="md-search" color={'black'} size={20} />           
              </View>
        </View>

          <View style={{ justifyContent: 'center', marginBottom: 10 }}>
            <Text style={{ fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 20, padding: 2, fontWeight: 'bold', textAlign: 'center', marginTop:5, marginBottom:5 }}>Projects</Text>
          </View>
          <View style={{ fontFamily: "29LTKaff-Regular", flexDirection: 'row', justifyContent: 'center', marginBottom: 15 }}>
              <View style={{ justifyContent: 'center', borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: "#2CA095",
    fontFamily: "Kaffregularbold", width:200 , height: 30, marginBottom: 2, borderWidth: 1, borderColor: '#2CA095', backgroundColor: 'white' }}>
               <RNPickerSelect
                                placeholder={{
                                    label: 'Project Location',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.projectLocation}
                                onValueChange={(value) => {
                                    this.setState({
                                      searchTerm: value,
                                    });
                                }}
                                onUpArrow={() => {
                                   // this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;

                                    //underline: { opacity: 0 }, justifyContent: 'center', borderRadius: 50, width: WIDTH - 65, height: 40, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' 
                                }}
                            />
              </View>
          </View>
  
          <ScrollView>
            {filteredEmails.map(item => {
              return (
                 <TouchableOpacity onPress={() => this.getScreen(item.id)}>
                    <View style={{ flex: 1 }}>
                      <View elevation={1} style={{ borderBottomColor: '#d8d8d8', borderBottomWidth: 0.5, backgroundColor: 'ffffff', flexDirection: 'row', marginBottom: 0, justifyContent: 'center', width: WIDTH }}>
                        <View style={{ flexDirection: 'row', paddingTop:10, paddingBottom:10 }}>
                          <View style={{ borderRadius: 12, justifyContent: 'center', alignItems: 'center', marginLeft: 16, marginTop: 6, marginBottom: 0, backgroundColor: '#d8d8d8', width: 150, height: 100 }}>
                            <Image source={{ uri: item.photo }} style={{ borderRadius:12, padding: 10, height: 100, width: 150, flex: 1, resizeMode: 'contain' }} />
                          </View>

                          <View style={{ marginTop:0, marginLeft: 10, borderRadius: 12, width: WIDTH / 2 + 8, height: 'auto' }}>
                            <Text style={{ marginTop: 15, paddingStart: 10, fontFamily: "29LTKaff-Regular", fontSize: 14, color: 'black', fontWeight: '900' }}>{item.title}</Text>
                            <Text style={{ paddingStart: 10, fontFamily: "29LTKaff-Regular", fontSize: 13, color: '#2CA095', fontWeight: '400', marginTop: 0, textAlign: 'justify', padding: 0 }}>{item.location}</Text>
                            <Text numberOfLines={2} ellipsizeMode ={'tail'} style={{ width:WIDTH/2, paddingStart: 10, fontFamily: "29LTKaff-Regular", fontSize: 12, color: 'black', fontWeight: 'normal', paddingTop: 4, textAlign: 'justify', padding: 10, margin: 0 }}>
                              {item.description}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>)
            })}
          </ScrollView>
      </ScrollView>
    );
  }
}
export default Projectdetails;
