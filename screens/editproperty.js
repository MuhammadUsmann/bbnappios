import React, { Component } from 'react';
import { Keyboard, AsyncStorage, Picker, Text, View, StyleSheet, Alert, Image, SearchBar, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');
import Ionicons from 'react-native-vector-icons/Ionicons';

import RNPickerSelect from 'react-native-picker-select';
import ImagePicker from 'react-native-image-crop-picker';
import * as Animatable from 'react-native-animatable';

import Spinner from 'react-native-loading-spinner-overlay';

import Toast, { DURATION } from 'react-native-easy-toast';

class Editproperty extends React.Component {
  constructor(props) {
    super(props); 
    this.selectImages = this.selectImages.bind(this);

    this._show = this._show.bind(this);
    this._hide = this._hide.bind(this);
    this.addProperty =  this.addProperty.bind(this);

    this.state = { 
      spinner: false,
      photo:'',
      avatar:'', 
      price:'',
      title:'', 
      name:'',
      projectName:'',
      location:'',
      description:'',
      type:'',
      landArea:'',
      bua:'',
      bathrooms:'',
      rooms:'', 
      keywords:'',
      paymentType: '',
      paymentFields:false,
      downpayment:'',
      years:'',
      editId:'',
      items: [
        {
          label: 'Payment Type',
          value: '',
        },
        {
            label: 'Cash',
            value: 'Cash',
        },
        {
            label: 'Installments',
            value: 'Installments',
        }
    
    ],
      downpayment:null,
      years:'',
      comments:'',
      token:'wYYLf2tDlkkLzYA2JyyTjosI7MoO9LpMOnV4kZJFMtTgh4GSJHaozVk5EQln'
    };
  }

  _show() {
    this.setState({spinner:true});
    }
    
    _hide(){
    this.setState({spinner:false});
    }
  
  async componentWillMount() {

     let propertyInfo = this.props.navigation.state.params.id || {}
        this.setState({
          isLoading: false,
          refreshing: false,
         
          editId:propertyInfo['id'],
          photo:propertyInfo['photo'], 
          price:propertyInfo['max_price'],
         
          title:propertyInfo['title'], 
       //   name:propertyInfo['name'],
          projectName:propertyInfo['project'],
          location:propertyInfo['location'],
          description:propertyInfo['details'],
          type:propertyInfo['type'],
          landArea:propertyInfo['area'],
          bua:propertyInfo['bua'],
          bathrooms:propertyInfo['baths'],
          rooms:propertyInfo['rooms'],
          keywords:propertyInfo['keywords'],
          paymentType: propertyInfo['payment_type'],
          comments:propertyInfo['comment'],
          downpayment:propertyInfo['down_payment'],
          years:propertyInfo['years']
      })

  }

  CheckTextInput = () => {
    if (this.state.price != '') {
        if (this.state.projectName != '') {
            if (this.state.location != '') {
                if (this.state.type != '') {
                  this.addProperty()
                } else {
                    this.refs.toast.show('Enter Property Type!', 1000, () => {
                    })
                  }
                } else {
                    this.refs.toast.show('Enter Property Location!', 1000, () => {
                    })
                }
            } else {
                this.refs.toast.show('Enter Project Name!', 1000, () => {
                })
            }
        } 
        else {
          this.refs.toast.show('Enter Property Price!', 1000, () => {
          })
        }
    }

    async addProperty() {
      const userToken = await AsyncStorage.getItem('userToken');
    fetch('https://bbn-eg.com/broker/api/property/'+this.state.editId+'?'+userToken, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
             api_token: this.state.token,
             name: this.state.title,
             photo: this.state.photo,
             projectName: this.state.projectName,
             details: this.state.description, 
             landmarks: this.state.landArea, 
             location: this.state.location,
             price_to: this.state.price,
             type: this.state.type,
             bua: this.state.bua,
             land_area: this.state.landArea,
             rooms:this.state.rooms,
             baths:this.state.bathrooms,
             keywords:this.state.keywords, 
             payment_type:this.state.paymentType,//[],//'Cash',
             down_payment: this.state.downpayment,
             years: this.state.years,
             comment: this.state.comments,
        })

    }).then((response) => response.json())
        .then((responseJson) => {

       //   alert(JSON.stringify(responseJson));
        
              this.refs.toast.show(responseJson['message'], 1000, () => {
                this.props.navigation.goBack()
            });
        })
        .catch((error) => {
            console.error(error);
        });
} 


static navigationOptions = {
  headerTitleStyle: { justifyContent: 'center' },
  headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}

  selectImages(){
    ImagePicker.openPicker({
      mediaType: 'photo',
      multiple: true,
      includeBase64: true,
      maxFiles: 3
    }).then(images => {
      this.setState({
        photo:"data:image/png;base64,"+images[0]['data']
      })
    })/*
      .catch(err =>{
      alert("Error"+err);
    })
    */
  }
  render() {
   
    const pickerStyle = {
      inputIOS: {
          color: 'black',
          paddingTop: 13,
          paddingHorizontal: 10,
          paddingBottom: 10,
          fontFamily: "29LTKaff-Regular",
          fontSize:14,
          height:38

      },
      inputAndroid: {
          color: 'black',
      },
      placeholderColor: 'grey',
      underline: { borderTopWidth: 0 },
      icon: {
          position: 'absolute',
          backgroundColor: 'transparent',
          borderTopWidth: 5,
          borderTopColor: '#00000099',
          borderRightWidth: 5,
          borderRightColor: 'transparent',
          borderLeftWidth: 5,
          borderLeftColor: 'transparent',
          width: 0,
          height: 10,
          top: 18,
          right: 15,
      },
  };
    return (
      <ScrollView>

            
<Spinner
          visible={this.state.spinner}
          //textContent={'Loading...'}
          textStyle={style.spinnerTextStyle}
        />
 
        <Text style={{ backgroundColor:'transparent', fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center' }}>Edit Property</Text>

       
        <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10, marginLeft: 15, justifyContent: 'center', width: WIDTH - 30 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ borderBottomLeftRadius: 12,
    borderTopRightRadius: 12, borderWidth: 1, borderColor: '#2CA095', justifyContent: 'center', alignItems: 'center', marginRight: 10, width: WIDTH / 2 - 50, height: WIDTH / 2 - 50 }}>
             
            <Image source={{ uri: this.state.photo }} style={{ backgroundColor: '#2CA095', borderBottomLeftRadius: 12,
    borderTopRightRadius: 12, borderWidth: 1, width: WIDTH / 2 - 50, height: WIDTH / 2 - 50  }} />

            </View>
           <TouchableOpacity onPress={this.selectImages}>
            <View style={{ 
    borderBottomLeftRadius: 18,
    backgroundColor:'#2CA095',
    borderTopRightRadius: 18,
    fontFamily: "29LTKaff-SemiBold", borderWidth: 1, borderColor: '#2CA095', justifyContent: 'center', alignItems: 'center', marginLeft: 10, width: WIDTH / 2 - 50, height: WIDTH / 2 - 50 }}>
              <Ionicons name={'md-add'} size={40} color={'white'} />
            </View>
            </TouchableOpacity>
          </View>
        </View>

      
      
        <Text style={{  backgroundColor:'transparent', fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center', backgroundColor: 'white' }}>Basic Info</Text>
        {/* Input Field Area Start  */}
     
     
       
        <View style={{ backgroundColor:'#f1f1f1',borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Price</Text>
          </View>
          <View style={{ backgroundColor:'#f1f1f1',width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'000'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(price) => this.setState({ price: price })}
              placeholderTextColor={'grey'}
              keyboardType = 'numeric'
              value={`${this.state.price}`}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>

        {/* Input Field Area End  */}

        {/* Input Field Area Start  */}
      
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ backgroundColor:'transparent',fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Title</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Type Title Here'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(title) => this.setState({ title: title })}
              placeholderTextColor={'grey'}
              value={this.state.title}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
       
        <View style={{ backgroundColor:'#FAF9F9',flexDirection: 'row', justifyContent: 'center' }}>
          <View elevation={3} style={{ marginTop: 10, marginBottom: 10, justifyContent: 'center', width: WIDTH - 40, height: 45, borderRadius: 50, backgroundColor: 'white' }}>
            
        <TextInput style={{ fontFamily: "29LTKaff-Regular", color: "grey", fontSize: 16, paddingBottom: 2, paddingTop: 10, fontWeight: "400", textAlign: "left", paddingStart: 20 }}
           placeholder={"Enter your Project"}
           returnKeyLabel='Done' 
           returnKeyType='done' 
           onSubmitEditing={Keyboard.dismiss}
           placeholderTextColor={"grey"}
           onChangeText={(projectName) => this.setState({ projectName: projectName })}
           value={this.state.projectName}
           underlineColorAndroid="transparent" />
          </View>
        </View>

        {/* Input Field Area Start  */}
      
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{  backgroundColor:'transparent', fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Location</Text>
          </View>
          <View style={{ backgroundColor:'#f1f1f1', width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Type Location Here'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(location) => this.setState({ location: location })}
              placeholderTextColor={'grey'}
              value={this.state.location}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        {/* Input Field Area Start  */}
       
        <View style={{backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Description</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'More info'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(description) => this.setState({ description: description })}
              placeholderTextColor={'grey'}
              value={this.state.description}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        <Text style={{ backgroundColor:'transparent',fontFamily: "Avenir", color: 'black', fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center', backgroundColor: 'white' }}>Details</Text>

        {/* Input Field Area Start  */}
       
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Type</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Property Type'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(type) => this.setState({ type: type })}
              placeholderTextColor={'grey'}
              value={this.state.type}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        {/* Input Field Area Start  */}
      
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Land Area</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13,fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Optional'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(landArea) => this.setState({ landArea: landArea })}
              placeholderTextColor={'grey'}
              keyboardType = 'numeric'
              value={`${this.state.landArea}`}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        {/* Input Field Area Start  */}
       
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>BUA</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(bua) => this.setState({ bua: bua })}
              placeholderTextColor={'grey'}
              keyboardType = 'numeric'
              value={`${this.state.bua}`}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
     
        <View style={{  backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Bathrooms</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(bathrooms) => this.setState({ bathrooms: bathrooms })}
              placeholderTextColor={'grey'}
              value={`${this.state.bathrooms}`}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
       
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Rooms</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(rooms) => this.setState({ rooms: rooms })}
              placeholderTextColor={'grey'}
              value={`${this.state.rooms}`}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
     
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Keywords</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'#'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(keywords) => this.setState({ keywords: keywords })}
              placeholderTextColor={'grey'}
              value={this.state.keywords}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        <Text style={{ backgroundColor:'#FAF9F9', fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center', backgroundColor: 'white' }}>Payment Details</Text>

       
        <Toast
                        ref="toast"
                        style={{ backgroundColor: '#59BF8F', width: WIDTH - 40, height: 50 }}
                        position='top'
                        positionValue={-50}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        opacity={0.9}
                        textStyle={{ color: 'white', textAlign: 'center', paddingTop: 5 }}
                    />
      

        
        <View style={{ fontFamily: "29LTKaff-Regular", flexDirection: 'row', fontFamily: "Avenir", justifyContent: 'center', marginBottom: 15 }}>
        
            <View elevation={3} style={{ justifyContent: 'center', 
    borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    fontFamily: "29LTKaff-SemiBold'", width: WIDTH - 40, height: 45, marginBottom: 2, borderWidth: 1, borderColor: '#2CA095', backgroundColor: 'white' }}>
             
             <RNPickerSelect
                                placeholder={{
                                    label: 'Payment Type',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.items}
                                onValueChange={(value) => {
                                    this.setState({
                                      paymentType: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;

                                    //underline: { opacity: 0 }, justifyContent: 'center', borderRadius: 50, width: WIDTH - 65, height: 40, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' 
                                }}
                            />
           
            </View>
        </View>

        {/* Input Field Area Start  */}
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>DownPayment</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2,  paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(downpayment) => this.setState({ downpayment: downpayment })}
              placeholderTextColor={'grey'}
              value={`${this.state.downpayment}`}
              editable={this.state.paymentType == 'Installments'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Years</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(years) => this.setState({ years: years })}
              placeholderTextColor={'grey'}
              value={`${this.state.years}`}
              editable={this.state.paymentType == 'Installments'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Comment</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Type your comments'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(comments) => this.setState({ comments: comments })}
              placeholderTextColor={'grey'}
              value={this.state.comments}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity style={style.btnLogin} onPress={() => this.CheckTextInput()}>
            <Text style={{ fontFamily: "29LTKaff-SemiBold", fontSize: 16, fontWeight: 'bold', color: 'black' }}>Update Property</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const style = StyleSheet.create({
  btnLogin: {
    width: 160,
    height: 50,
    borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: "#2CA095",
    fontFamily: "29LTKaff-SemiBold",
    marginTop: 15,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  }, container: {
    flex: 1
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 1,
    paddingLeft: 155,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  projectDetails: {
    flexDirection: 'row',
    width: 140,
    height: 100,
    borderRadius: 12,
    paddingTop: 10,
    backgroundColor: 'white'
  },
  input: {
    width: WIDTH / 2 + 30,
    height: 10,
    fontSize: 16,
    textAlign: 'center',
    fontFamily: "29LTKaff-Regular",
    color: '#ffffff',
    justifyContent: "center",
    alignItems: "stretch",
    borderBottomWidth: 2,
    height: 50,
    borderColor: "red",
    marginHorizontal: 25,

  }
});


export default Editproperty;