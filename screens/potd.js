import React, { Component } from 'react';
import { TouchableHighlight, TouchableOpacity, AsyncStorage, RefreshControl, FlatList, ActivityIndicator, Text, View, Image, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');

class Potd extends React.Component {
    constructor(props) {
        super(props); 
        this.state = {
          isLoading: true,
          dataSource: [],
          refreshing: false,
        }; 
      }

      async componentWillMount() {
        fetch('https://bbn-eg.com/broker/api/potds')
          .then((response) => response.json())
          .then((findresponse) => {
            this.setState({
              isLoading: false,
              dataSource: findresponse,
              refreshing: false
             
            })
         // alert(JSON.stringify(this.state.dataSource[0]))
       
            })
            
      }

      _onRefresh = () => {
        this.setState({ refreshing: true });
        this.componentWillMount();
      }


      openPotd(id){
      //  alert(id);
        this.props.navigation.navigate('PropertyDetails', { id: id })
      }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
        </View>
      );
    }
    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          colors={['#2CA095']}
        />
      }>
         
         <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:18, fontWeight:'bold', backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center'}} >POTDs</Text>
     
             <ScrollView style={{ paddingStart: 10, paddingTop:20, backgroundColor:'#f5f5f7' }} horizontal={false} showsHorizontalScrollIndicator={false}>
               <FlatList pagingEnabled={true} style={{ flexDirection: "column", flex: 1 }} data={this.state.dataSource} renderItem={({ item }) => <View elevation={3} style={{  justifyContent:'center', flexDirection:'row'}}>
               <TouchableOpacity onPress={() => this.openPotd(item.potd.id)}>
              
               <View elevation={3} style={{  backgroundColor:'white',   flexDirection: 'row', marginLeft: 0, justifyContent: 'center', width: WIDTH - 30, height:140, marginBottom:10 }}>
               
               <View style={{  shadowColor: "#000", elevation: 2,  marginTop:0,  justifyContent: 'flex-start', flexDirection: 'row', width: WIDTH - 15, height: 'auto', paddingBottom:10 }}>
         
                <View style={{   paddingLeft: 0,  width: 150, height: 135, padding: 0, marginStart:0, marginTop:4, borderRadius:0 }}>
                   <Image source={{ uri: item.potd.photo }} style={{ borderRadius: 0, flex: 1, height: 135, width: 150 }} />
                 </View>
               
                 <View style={{   marginTop:15, marginStart:0, borderRadius: 0, width: WIDTH/2 + 45, minHeight:120, maxHeight:'auto', height:120, paddingBottom:5 }}>
                  
                
                   <View >   
                
                    <View style={{ marginLeft: 5, height: 'auto', width: WIDTH / 2 - 30}}>

                     <View style={{  marginLeft: 10, height: 'auto', width: WIDTH / 2 - 15, paddingBottom: 0 }}>
                      
                     <Text style={{fontFamily: "29LTKaff-Regular", fontSize: 14, color: '#141414', fontWeight: 'normal', marginLeft: 0, lineHeight:15, marginTop:10, marginBottom:5 }}>Type: {item.potd.type}</Text>
                      
                       <Text style={{fontFamily: "29LTKaff-SemiBold", fontSize: 18, color: 'black', fontWeight: 'bold', height:18, marginBottom:5, marginLeft: 0, lineHeight:20}}>{item.potd.max_price} EGP</Text>
                      
                       <Text style={{ height:30, fontFamily: "29LTKaff-Regular", fontSize: 12, color: '#141414', fontWeight: 'normal', marginLeft: 0, lineHeight:15 }}>{item.potd.project}, {item.potd.payment_type}, {item.potd.location}</Text>
                     
                     
                       <View style={{ flexDirection: 'row', justifyContent: 'flex-start', width:WIDTH/2 - 15, height:30 }}>

                           <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                          
                          
                           <Image style={{position:"absolute", left:0, top:12}}source={require('../src/assets/rooms.png')}  color={'black'} size={22} />
                    
                          <Text style={{position:"absolute", left:27, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.potd.rooms > 0 ? item.potd.rooms : 0}</Text>
 
                           </View>
                           <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                           <Image  style={{position:"absolute", left:0, top:5, height:20}}source={require('../src/assets/washrooms.png')}  color={'black'} size={20} />
                    
                          <Text style={{position:"absolute", left:27, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.potd.baths > 0 ? item.potd.baths : 0}</Text>
 
                           </View>
                           <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                           <Image style={{position:"absolute", left:5, top:8}}source={require('../src/assets/area.png')}  color={'black'} size={22} />
                    
                         <Text style={{position:"absolute", left:27, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.potd.area > 0 ? item.potd.area : 0}</Text>

 
                           </View>

                       </View>

                     </View> 

                    

                     </View>

                     </View>
              
                    
                 </View>
                 
               </View>
       

           </View>


                </TouchableOpacity>

               
               </View>} keyExtractor={(item, index) => item.toString()} />
             </ScrollView>
          
      </ScrollView>
    );
  }
}

export default Potd;
