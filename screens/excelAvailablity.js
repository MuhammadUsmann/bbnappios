import React, { Component } from "react";
import {
  FlatList,
  ActivityIndicator,
  Slider,
  Text,
  View,
  StyleSheet,
  Button,
  Image,
  Dimensions,
  TouchableOpacity,
  Linking,
  Picker,
  NetInfo,
  ToastAndroid,
  AsyncStorage,
  Animated,
  Easing,
  TouchableWithoutFeedback,
  RefreshControl
} from "react-native";

const { width: WIDTH, height: HEIGHT } = Dimensions.get("window");

import { ScrollView } from 'react-native-gesture-handler';

import * as Animatable from 'react-native-animatable';

class ExcelAvailablity extends React.Component {
    constructor(props) {
        super(props); 
        this.state = {
          isLoading: true,
          dataSource: []
        }; 
      }
      async componentWillMount() {
        this.setState({ 
            dataSource:this.props.navigation.state.params.id || []
        })
      }
    
      
  render() {
    return (
      <ScrollView>
            <Animatable.Text style={{ paddingTop:  15, fontFamily: "Avenir", color: "black", fontSize: 18, padding: 10, fontWeight: "bold", textAlign: "center", paddingBottom:10 }} animation="slideInLeft" duration={1200} iterationCount={1}  direction="alternate">
        <Text>
          Construction Images
        </Text>
      </Animatable.Text>

           <Animatable.View animation="slideInLeft" duration={2000} iterationCount={1} direction="alternate">
            <ScrollView style={{ paddingStart: 10 }} horizontal={false} showsHorizontalScrollIndicator={false}>
              <FlatList pagingEnabled={true} style={{ flexDirection: "column", flex: 1 }} data={this.state.dataSource} renderItem={({ item }) => <View style={{ justifyContent:'center', flexDirection:'row'}}>
                <View style={{ paddingBottom:10, flex: 1, height: null, width: null, resizeMode: "contain", paddingStart: 5, width: WIDTH, height: 200 }}>
                   <Image source={{ uri: item.url }} style={{ marginRight: 15, width: WIDTH - 35, height:200 , flex: 1, borderRadius: 12 }} />
                </View>
 
              </View>} keyExtractor={(item, index) => item.toString()} />
            </ScrollView>
          </Animatable.View>
      </ScrollView>
    );
  }
  static navigationOptions = {
    headerTitle: (<Image style={{ marginRight: 56, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}
}
export default ExcelAvailablity;