import React, { Component } from 'react';
import { AsyncStorage, Picker, RefreshControl, TouchableHighlight, ActivityIndicator, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');
import { createFilter } from 'react-native-search-filter';
import Icon from 'react-native-vector-icons/Ionicons';
const KEYS_TO_FILTERS = ['project', 'title', 'rating'];

import RNPickerSelect from 'react-native-picker-select';

class Properties extends React.Component {
  constructor(props) {
    super(props);
    this.editProperty = this.editProperty.bind(this);
    this.state = {
      refreshing: false,
      isLoading: true,
      dataSource: [],
      searchTerm: '',
      itemValue: null,
      items: [
        {
          label: 'Highest Price',
          value: '0',
        },
        {
          label: 'Lowest Price',
          value: '1',
        }
      ]
    }
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.componentWillMount();
  }

  async componentWillMount(){
    const userToken = await AsyncStorage.getItem('userToken');
    try { 
      let response = await fetch(
        "https://bbn-eg.com/broker/api/property/own/list?api_token="+userToken
      );
      let responseJson = await response.json();
      this.setState({
        isLoading: false,
        refreshing: false,
        dataSource: responseJson,
      }
      );
      
      //alert(JSON.stringify(responseJson))
    } catch (error) {
      console.error(error);
    }
    
    
  }

  getScreen(item) {
    navigationOptions = {
      title: 'Welcome',
    };
    this.props.navigation.navigate('PropertyDetails', { id: item.id })
  }

  searchUpdated(term) {
  
    this.setState({ searchTerm: term })
  }

  editProperty(item){
    this.props.navigation.navigate('EditProperty', { id: item })
   //   alert("ID"+JSON.stringify(item));
  }


  
  onButtonPress = () => {
    this.setState({ buttonColor: 'blue' }); 
    alert(this.state.buttonColor)
  }
  


  async sortData(id) {

    const userToken = await AsyncStorage.getItem('userToken');


    this.state.isLoading = true;
    if (id == '0') {
      fetch('https://bbn-eg.com/broker/api/property/own?'+userToken).then((res) => res.json())
        .then((data) => {
         // this.setState({ itemValue: itemValue }); 
          
          data.sort((a, b) => b.max_price - a.max_price);
          this.setState({ dataSource: data, isLoading:false });
        })
    }
    if (id == '1') {
      fetch('https://bbn-eg.com/broker/api/property/own?'+userToken).then((res) => res.json())
        .then((data) => {
          data.sort((a, b) => a.max_price - b.max_price);
          this.setState({dataSource: data, isLoading:false });
        })  
    }
    if(id == 'null'){
      this.setState({ isLoading:false });
    }
  }

  render() {
    const filteredEmails = this.state.dataSource.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
    const pickerStyle = {
      inputIOS: {
          color: 'black',
          paddingTop: 13,
          paddingHorizontal: 10,
          paddingBottom: 10,
          fontFamily: "29LTKaff-Regular",
          fontSize:14,
          height:38
      },
      inputAndroid: {
          color: 'black',
      },
      placeholderColor: 'grey',
      paddingTop:5,
      underline: { borderTopWidth: 0 },
      icon: {
          position: 'absolute',
          backgroundColor: 'transparent',
          borderTopWidth: 5,
          borderTopColor: '#00000099',
          borderRightWidth: 5,
          borderRightColor: 'transparent',
          borderLeftWidth: 5,
          borderLeftColor: 'transparent',
          width: 0,
          height: 10,
          top: 18,
          right: 15,
      },
  };

    
    
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#26a69a" />
        </View>
      );
    }


    return <ScrollView refreshControl={
      <RefreshControl
        refreshing={this.state.refreshing}
        onRefresh={this._onRefresh}
        colors={['#2CA095']}
      />
    }>

   

        <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:18, fontWeight:'bold', backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center'}} >My Properties</Text>
     
        <View style={{ justifyContent: 'center', width: WIDTH, height: 50 }}>

<View style={{ width: 60, height: 20, position: 'absolute', left: 35, right: 'auto', top: 'auto', bottom: 10 }}>
  <Text style={{color:'black', fontSize:16, fontFamily:'29LTKaff-Regular', width: 55, height: 20}}>
  Search
  </Text>
      </View>

  <TextInput
    style={{ 
      width:WIDTH - 140, paddingStart:15, position: 'absolute', left:90, right:'auto', justifyContent: 'center', flexDirection:'row', fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 16, paddingBottom: 5, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30
    }}
 //   placeholder={'Search'} 
    onChangeText={(term) => { this.searchUpdated(term) }}
    placeholderTextColor={'grey'}
    underlineColorAndroid='black'
    autoCorrect={false}
  />
   <View style={{ marginStart:5, justifyContent:"center", flexDirection:"row",  }}>
    <View style={{borderBottomColor: 'grey', borderBottomWidth: 2, width:WIDTH - 150, paddingStart:15, position: 'absolute', left:90, right:'auto', justifyContent: 'center', ontSize: 16, paddingBottom: 5, paddingTop: 5, fontWeight: '400', textAlign: 'left', paddingStart: 30 }} />
    </View>
    
    <View style={{ width: 20, height: 20, position: 'absolute', left: 'auto', right: 32, top: 'auto', bottom: 10 }}>
    <Icon name="md-search" color={'black'} size={20} />

    
    
      </View>
    
</View>

        <View style={{ flexDirection: 'row', marginTop: 0, marginBottom: 5, marginLeft: 5, justifyContent: 'center', width: WIDTH - 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('AddProperty')}>
              <View style={{ 
    borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    fontFamily: "29LTKaff-Regular", justifyContent: 'center', alignItems: 'center', width: WIDTH/3 - 10, height: 30 }}>
                <Text style={{ color:'#2CA095', padding: 0, justifyContent: 'center', height: 'auto', fontFamily: "29LTKaff-Regular", fontSize: 15, fontWeight: 'bold', textAlign: 'justify' }}>Add New</Text>
              
              
              
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('RequestInformation')}>
              <View style={{ borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    fontFamily: "29LTKaff-Regular", justifyContent: 'center', alignItems: 'center', marginLeft: 4, width:  WIDTH/3 - 10, height: 30 }}>
                <Text style={{ padding: 0, justifyContent: 'center', height: 'auto', fontFamily: "29LTKaff-Regular", fontSize: 15, color: '#2CA095', fontWeight: 'bold', textAlign: 'justify' }}>Request</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('RequestResults')}>
              <View style={{ borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    fontFamily: "29LTKaff-Regular",justifyContent: 'center', alignItems: 'center', marginLeft: 4, width:  WIDTH/3 - 10, height: 30 }}>
                <Text style={{ padding: 0, justifyContent: 'center', height: 'auto', fontFamily: "29LTKaff-Regular", fontSize: 15, color: '#2CA095', fontWeight: 'bold', textAlign: 'justify' }}>Pending</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View>
        </View>



        <View style={{ flexDirection: 'row', fontFamily: "29LTKaff-Regular", justifyContent: 'center' }}>
      
          <View style={{ width: WIDTH, height: 45, paddingTop:5, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' }}>
          <RNPickerSelect
                                placeholder={{
                                    label: 'Price Range',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.items}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedprice: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;
                                }}
                            />
          
          </View> 
      </View>
 


      
 
      <ScrollView style={{ paddingBottom:10}}>
        {filteredEmails.map(item => {
          return (  
              <View elevation={3} style={{ backgroundColor:'#f5f5f7',  flexDirection: 'row', backgroundColor:'white', marginLeft: 0, justifyContent: 'center', width: WIDTH - 0, height:140, marginBottom:10 }}>
               
                  <View style={{  shadowColor: "#000", elevation: 2,  marginTop:0,  justifyContent: 'flex-start', flexDirection: 'row', width: WIDTH - 15, height: 'auto', paddingBottom:10 }}>
            
                  <TouchableHighlight
                  onPress={() => this.getScreen(item)}
                  underlayColor='transparent'>
                    <View style={{   paddingLeft: 0,  width: 150, height: 135, padding: 0, marginStart:0, marginTop:4, borderRadius:0 }}>
                      <Image source={{ uri: item.photo }} style={{ borderRadius: 0, flex: 1, height: 135, width: 150 }} />
                    </View>
                    </TouchableHighlight>

                    <View style={{   marginTop:15, marginStart:0, borderRadius: 0, width: WIDTH/2 + 45, minHeight:120, maxHeight:'auto', height:120, paddingBottom:5 }}>
                     
                   
                      <View >   
                   
                        <TouchableHighlight underlayColor='transparent' style={{width: 40, height: 40, position: 'absolute', left: 'auto' , right: 30, top: 10}} onPress={() => this.editProperty(item)}>
                          <View style={{  width: 20, height: 26 }}>
                          <Image source={require('../src/assets/icons/PenIcon.png')} style={{ width: 16, height: 20, position: 'absolute', left: 20, right: 0, top: 10}} />
                       
                          </View> 
                          </TouchableHighlight>


                          <TouchableHighlight underlayColor='transparent' style={{ marginLeft: 5, height: 'auto', width: WIDTH / 2 - 30}} onPress={() => this.getScreen(item)}>
                       

                        <View style={{  marginLeft: 10, height: 'auto', width: WIDTH / 2 - 15, paddingBottom: 0 }}>
                         
                        <Text style={{fontFamily: "29LTKaff-Regular", fontSize: 14, color: '#141414', fontWeight: 'normal', marginLeft: 0, lineHeight:17, marginTop:10, marginBottom:5 }}>Type: {item.type}</Text>
                         
                          <Text style={{fontFamily: "29LTKaff-SemiBold", fontSize: 18, color: 'black', fontWeight: 'bold', height:18, marginBottom:5, marginLeft: 0, lineHeight:19, }}>{item.max_price} EGP</Text>
                         
                          <Text style={{ height:30, fontFamily: "29LTKaff-Regular", fontSize: 12, color: '#141414', fontWeight: 'normal', marginLeft: 0, lineHeight:15 }}>{item.project}, {item.payment_type}, {item.location}</Text>
                        
                          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', width:WIDTH/2 - 15, height:30 }}>

                              <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                             
                             
                              <Image style={{position:"absolute", left:0, top:12}}source={require('../src/assets/rooms.png')}  color={'black'} size={22} />
                       
                             <Text style={{position:"absolute", left:27, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.rooms > 0 ? item.rooms : 0}</Text>
    
                              </View>
                              <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                              <Image  style={{position:"absolute", left:0, top:5, height:20}}source={require('../src/assets/washrooms.png')}  color={'black'} size={20} />
                       
                             <Text style={{position:"absolute", left:27, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.baths > 0 ? item.baths : 0}</Text>
    
                              </View>
                              <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                              <Image style={{position:"absolute", left:5, top:8}}source={require('../src/assets/area.png')}  color={'black'} size={22} />
                       
                       <Text style={{position:"absolute", left:26, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.area > 0 ? item.area : 0}</Text>

    
                              </View>

                          </View>

                        </View> 

                       

                        </TouchableHighlight>
 
                        </View>
                 
                       
                    </View>
                    
                  </View>
          

              </View>
          )
        })}
      </ScrollView>
    </ScrollView>
  }
}
export default Properties;
