import React, { Component } from 'react';
import { Keyboard, AsyncStorage, ToastAndroid, PermissionsAndroid, Platform, Picker, Text, View, StyleSheet, Alert, Image, SearchBar, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');
import Ionicons from 'react-native-vector-icons/Ionicons';

import ImagePicker from 'react-native-image-crop-picker';

import RNPickerSelect from 'react-native-picker-select';
import Toast, { DURATION } from 'react-native-easy-toast';


class Addproperty extends React.Component {
  constructor(props) {
    super(props); 
    this.selectImages = this.selectImages.bind(this);
    this.state = { 
    //  itemValue: null,
      photo:'', 
      price:'',
      title:'', 
      name:'',
      projectName:'',
      project:'',
      location:'',
      description:'',
      type:'',
      landArea:'',
      bua:'',
      bathrooms:'',
      rooms:'',
      keywords:'',
      paymentType: '',
      items: [
        /*
          {
          label: 'Payment Type',
          value: '',
        },
        */
        {
            label: 'Cash',
            value: 'Cash',
        },
        {
            label: 'Installments',
            value: 'Installments',
        }
    
    ],
      downpayment:'',
      years:'',
      comments:'',
      //token:'wYYLf2tDlkkLzYA2JyyTjosI7MoO9LpMOnV4kZJFMtTgh4GSJHaozVk5EQln'
    };
  }


  async newProperty() {
    const userToken = await AsyncStorage.getItem('userToken');

    fetch('https://bbn-eg.com/broker/api/property?api_token='+userToken, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
             api_token: this.state.token,
             name: this.state.name,
             project: this.state.projectName,
             photo: this.state.photo,
             details: this.state.description, 
             landmarks: this.state.landArea, 
             location: this.state.location,
             price_to: this.state.price,
             type: this.state.type,
             bua: this.state.bua,
             land_area: this.state.landArea,
             rooms:this.state.rooms,
             baths:this.state.bathrooms,
             keywords:this.state.keywords, 
             payment_type:this.state.paymentType,//[],//'Cash',
             down_payment: this.state.downpayment,
             years: this.state.years,
             comment: this.state.comments,
        })

    }).then((response) => response.json())
        .then((responseJson) => {

       //   alert(JSON.stringify(responseJson));
        
              this.refs.toast.show(responseJson['message'], 1500, () => {
                this.props.navigation.goBack()
            });
        })
        .catch((error) => {
            console.error(error);
        });
} 



 static navigationOptions = {
  headerTitleStyle: { justifyContent: 'center' },
  headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}

  async selectImages(){
    ImagePicker.openPicker({
      mediaType: 'photo',
      multiple: true,
      includeBase64: true,
      maxFiles: 3
    }).then(images => {
      console.log("ImageData", images);
      this.setState({
        photo:"data:image/png;base64,"+images[0]['data']
      })
      console.log("NewData" , this.state.photo);
    })

  }


  CheckTextInput = () => {
    if (this.state.price != '' && (this.state.price.trim().length >= 1)) {
        if (this.state.name != '' && (this.state.name.trim().length >= 1)) {
            if (this.state.location != '') {
                if (this.state.type != '') {
                  this.newProperty()
                } else {
                    this.refs.toast.show('Enter Property Type!', 1000, () => {
                    })
                  }
                } else {
                    this.refs.toast.show('Enter Property Location!', 1000, () => {
                    })
                }
            } else {
                this.refs.toast.show('Enter Property Title!', 1000, () => {
                })
            }
        } 
        else {
          this.refs.toast.show('Enter Property Price!', 1000, () => {
          })
        }
    }


  render() {

    const pickerStyle = {
      inputIOS: {
          color: 'black',
          paddingTop: 13,
          paddingHorizontal: 10,
          paddingBottom: 10,
          fontFamily: "29LTKaff-Regular",
          fontSize:14,
          height:38

      },
      inputAndroid: {
          color: 'black',
      },
      placeholderColor: 'grey',
      underline: { borderTopWidth: 0 },
      icon: {
          position: 'absolute',
          backgroundColor: 'transparent',
          borderTopWidth: 5,
          borderTopColor: '#00000099',
          borderRightWidth: 5,
          borderRightColor: 'transparent',
          borderLeftWidth: 5,
          borderLeftColor: 'transparent',
          width: 0,
          height: 10,
          top: 18,
          right: 15,
      },
  };

    return (
      <ScrollView>

<Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:18, fontWeight:'bold', backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center'}} >Add New Property</Text>
     
{
  /*

        <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:18, fontWeight:'bold'}} >Add New Property</Text>
    
  */
}
       
        <View style={{ flexDirection: 'row', marginTop: 15, marginBottom: 10, justifyContent: 'center', width: WIDTH }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ borderBottomLeftRadius: 18, borderTopRightRadius: 18, backgroundColor: "#f1f1f1", fontFamily: "29LTKaff-SemiBold", borderWidth: 1, borderColor: '#2CA095', justifyContent: 'center', alignItems: 'center', marginRight: 10, width: 140, height: 120 }}>
              <Text style={{ fontFamily: "29LTKaff-Regular", color: '#2CA095', fontSize: 15, padding: 12, paddingBottom: 20, paddingTop: 20, fontWeight: 'bold', textAlign: 'center' }}>ADD PHOTO</Text>
            </View>
          
          <View style={{ justifyContent:'center', textAlign:'center', borderRadius:50, backgroundColor:'#2CA095', width:25, height:25, top:'auto', bottom:5, position:"absolute", right:15, left:'auto'}}>
        
          <TouchableOpacity onPress={this.selectImages}>
         <View style={{paddingStart:6}}> 
         <Ionicons name={'md-add'} size={20} color={'white'} />
       
         </View>
         
         </TouchableOpacity>
          </View>

           </View>
        </View>
   
       <Text style={{  backgroundColor:'transparent', fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 12, paddingBottom: 20, paddingTop: 10, fontWeight: 'bold', textAlign: 'center', backgroundColor: 'white' }}>Basic Information</Text>
      
     
      
      
        <View style={{ backgroundColor:'#f1f1f1',borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Price</Text>
          </View>
          <View style={{ backgroundColor:'#f1f1f1',width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'000'}
              onChangeText={(price) => this.setState({ price: price })}
              placeholderTextColor={'grey'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
  returnKeyType='done' 
  onSubmitEditing={Keyboard.dismiss}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
     
       {/* Input Field Area End  */}

        {/* Input Field Area Start  */}
       
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ backgroundColor:'transparent',fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Title</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Type Title Here'}
              onChangeText={(name) => this.setState({ name: name })}
              placeholderTextColor={'grey'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
      
      
        <View style={{ backgroundColor:'#f1f1f1',flexDirection: 'row', justifyContent: 'center' }}>
          <View elevation={3} style={{ marginTop: 10, marginBottom: 10, justifyContent: 'center', width: WIDTH - 40, height: 45, 
    borderBottomLeftRadius: 18,
    borderTopRightRadius: 18,
    borderColor: "#2CA095",
    borderWidth:1,
    fontFamily: "Kaffregularbold", backgroundColor: 'white' }}>
            
        <TextInput style={{ fontFamily: "29LTKaff-Regular", color: "grey", fontSize: 16, paddingBottom: 5, paddingTop: 10, fontWeight: "400", textAlign: "left", paddingStart: 20 }}
           placeholder={"Enter your Project"}
           placeholderTextColor={"grey"}
           returnKeyLabel='Done' 
           returnKeyType='done' 
           onSubmitEditing={Keyboard.dismiss}
           onChangeText={(projectName) => this.setState({ projectName: projectName })}
           underlineColorAndroid="transparent" />
          </View>
        </View>
     
        {/* Input Field Area Start  */}
    
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{  backgroundColor:'transparent', fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Location</Text>
          </View>
          <View style={{ backgroundColor:'#f1f1f1', width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Type Location Here'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(location) => this.setState({ location: location })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        {/* Input Field Area Start  */}
     
        <View style={{backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Description</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'More info'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(description) => this.setState({ description: description })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        <Text style={{ backgroundColor:'transparent',fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center', backgroundColor: 'white' }}>Details</Text>

        {/* Input Field Area Start  */}
       
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Type</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Property Type'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(type) => this.setState({ type: type })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        {/* Input Field Area Start  */}
     
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Land Area</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Optional'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(landArea) => this.setState({ landArea: landArea })}
              placeholderTextColor={'grey'}
              keyboardType = 'numeric'
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        {/* Input Field Area Start  */}
     
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>BUA</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(bua) => this.setState({ bua: bua })}
              placeholderTextColor={'grey'}
              keyboardType = 'numeric'
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
      
        <View style={{  backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Bathrooms</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(bathrooms) => this.setState({ bathrooms: bathrooms })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
       {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
      
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Rooms</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(rooms) => this.setState({ rooms: rooms })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
      
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Keywords</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'#'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(keywords) => this.setState({ keywords: keywords })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}


                  
        <Text style={{ backgroundColor:'#f1f1f1', fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center', backgroundColor: 'white' }}>Payment Details</Text>

        <Toast
                        ref="toast"
                        style={{ backgroundColor: '#59BF8F', width: WIDTH - 40, height: 50 }}
                        position='top'
                        positionValue={-50}
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        opacity={0.9}
                        textStyle={{ color: 'white', textAlign: 'center', paddingTop: 5 }}
                    />
      
      
        <View style={{ fontFamily: "29LTKaff-Regular", flexDirection: 'row', justifyContent: 'center', marginBottom: 15 }}>
    
            <View elevation={3} style={{ justifyContent: 'center', 
    borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    fontFamily: "29LTKaff-SemiBold'", width: WIDTH - 40, height: 45, marginBottom: 2, borderWidth: 1, borderColor: '#2CA095', backgroundColor: 'white' }}>
              <RNPickerSelect
                                placeholder={{
                                    label: 'Payment Type',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.items}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedtype: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;

                                    //underline: { opacity: 0 }, justifyContent: 'center', borderRadius: 50, width: WIDTH - 65, height: 40, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' 
                                }}
                            />
            </View>
         </View>

        {/* Input Field Area Start  */}
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>DownPayment</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(downpayment) => this.setState({ downpayment: downpayment })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Years</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'0'}
              keyboardType = 'numeric'
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(years) => this.setState({ years: years })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}
        {/* Input Field Area Start  */}
        <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
          <View style={{ width: WIDTH / 2 - 30 }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 38 }}>Comment</Text>
          </View>
          <View style={{ width: WIDTH / 2 + 30 }}>
            <TextInput
              style={{
                fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 13, fontWeight: '400', textAlign: 'left', paddingStart: 10
              }}
              placeholder={'Type your comments'}
              returnKeyLabel='Done' 
              returnKeyType='done' 
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={(comments) => this.setState({ comments: comments })}
              placeholderTextColor={'grey'}
              underlineColorAndroid='transparent'
            />
          </View>
        </View>
        {/* Input Field Area End  */}

        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity style={style.btnLogin}  onPress={() => this.CheckTextInput()}>
            <Text style={{ fontFamily: "29LTKaff-SemiBold", fontSize: 16, fontWeight: 'normal', color: 'black' }}>Add Property</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const style = StyleSheet.create({
  btnLogin: {
    width: 140,
    height: 45,
    borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: "#2CA095",
    fontFamily: "29LTKaff-SemiBold",
    marginTop: 15,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  }, container: {
    flex: 1
  },
  property: {
    height: 400,
    backgroundColor: 'green',
  },
  projectText: {
    textAlign: 'center'
  },
  input: {
    width: WIDTH / 2 + 30,
    height: 10,
    fontSize: 16,
    textAlign: 'center',
    fontFamily: "29LTKaff-Regular",
    color: '#ffffff',
    justifyContent: "center",
    alignItems: "stretch",
    borderBottomWidth: 2,
    height: 50,
    borderColor: "red",
    marginHorizontal: 25,

  }
});


export default Addproperty;