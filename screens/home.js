import React, { Component } from 'react';
import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  RefreshControl,
  FlatList,
  ActivityIndicator,
  Image,
  Dimensions,
  TouchableOpacity,
  NetInfo,
  ToastAndroid,
  AsyncStorage
} from 'react-native';

import Swiper from 'react-native-swiper';
import { ScrollView } from "react-native-gesture-handler";
const { width: WIDTH, height: HEIGHT } = Dimensions.get("window");
import { FlatGrid } from 'react-native-super-grid';

import firebase from 'react-native-firebase';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      refreshing: false,
      isLoading: true,
      dataSource: [],
      projectData: [],
      propertyOftheDay: [],
      isConnected: true,
      refreshing: false,
      searchTerm: '',
      selected: false,
      image:[]

    }
    
  }

   
async componentDidMount() {
  this.checkPermission();
  this.createNotificationListeners(); //add this line
  StatusBar.setHidden(true);
}


componentWillUnmount() {
  this.notificationListener();
  this.notificationOpenedListener();
}

async createNotificationListeners() {
  

  this.notificationListener = firebase.notifications().onNotification((notification) => {
    notification.setSound("default");

 
  const { title, body } = notification;


    const { data } = notification;
  alert("data"+data);
    //this.openArticle(title, body, data);
});

  /*
  * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
  * */
  this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
  });

  /*
  * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
  * */
  const notificationOpen = await firebase.notifications().getInitialNotification();
  if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      
      //this.showAlert(title, body);
      
  }
  /*
  * Triggered for data only payload in foreground
  * */
  this.messageListener = firebase.messaging().onMessage((message) => {
    //process data message
   alert(JSON.stringify(message));
  });
}


openArticle(title, body, articleId) {
  
  const id = JSON.parse(articleId['article_id']);
  if(id != 0){
    Alert.alert(
      title, body,
      [
          { text: 'OK', onPress: () => this.openNotificationPage(id) },
      ],
      { cancelable: false },
    );
  }
  else{
    Alert.alert(
      title, body,
      [
          { text: 'OK', onPress: () => console.log('Do nothing') },
      ],
      { cancelable: false },
    );
  }

}


showAlert(title, body) {
  Alert.alert(
    title, body,
    [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
    ],
    { cancelable: false },
  );
}

 //1
 async checkPermission() {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
      this.getToken();
  } else {
      this.requestPermission();
  }
}

//3
async getToken() {
  const userToken = await AsyncStorage.getItem('userToken');
  let fcmToken = await AsyncStorage.getItem('fcmToken');
  //alert(JSON.stringify(fcmToken));
  if(fcmToken != ''){
    fetch('https://bbn-eg.com/broker/api/user/fcmtoken?api_token='+userToken, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          fcm_token:fcmToken
        })

    }).then((response) => response.json())
        .then((responseJson) => {
          //alert(JSON.stringify(responseJson));
     })
        .catch((error) => {
            console.error(error);
        });
  }

  if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
          //alert("FCMToken" + JSON.stringify(fcmToken));
          await AsyncStorage.setItem('fcmToken', fcmToken);
      }
  }
}

  //2
async requestPermission() {
  try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
  } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
      alert('rejected');
  }
}


async componentWillMount() {
  NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  try {
    let response = await fetch(
      "https://bbn-eg.com/broker/api/banners"
    );
    let responseJson = await response.json();
    this.setState({
      dataSource: responseJson
    });
  // alert(JSON.stringify(this.state.dataSource))
  } catch (error) {
    console.log(error); 
  }

  try {
    let response = await fetch(
      "https://bbn-eg.com/broker/api/projects"
    );
    let responseJson = await response.json();
    this.setState({
      projectData: responseJson
    });
    this.componentWillUnmount();
   //alert(JSON.stringify(this.state.projectData[0]['photo']))
  } catch (error) {
    console.log(error);
  }

  try {
    let response = await fetch("https://bbn-eg.com/broker/api/potds/featured");
    let responseJson = await response.json();
    this.setState({
      isLoading: false,
      refreshing: false,
      propertyOftheDay: responseJson,
      pressStatus: false,
      
    });
  } catch (error) {
    console.log(error);
  }
}

  openProject(item) {
    this.props.navigation.navigate('Projectlocationdetails', { id: item })
  }


   openPotd(id){
    this.props.navigation.navigate('PropertyDetails', { id: id })
 
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.componentWillMount();
  } 

  handleConnectionChange = (isConnected) => {
    this.setState({ status: isConnected });
    if (`${this.state.status}` == 'true') {

      ToastAndroid.showWithGravity(
        'Internet Connection Available',
        ToastAndroid.LONG, 
        ToastAndroid.BOTTOM,
      );
    }
    else {
      AlertIOS.alert(
        'No Internet Connection!'
      )
    }
  }

  bannerDetails(id){
    this.props.navigation.navigate('BannerDetails', { id: id });
  }



  render() {

    const swiperItems = this.state.dataSource.map(item => {
      return (
         <TouchableOpacity onPress={() => this.bannerDetails(item.id)}>
      
        <Image
          source={{ uri: item.photo }}
          style={{ width: WIDTH, height: 230 }}
          key={item.id}
        />
        </TouchableOpacity>
      )   
    }) 

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#26a69a" />
        </View>
      );
    }
    else
    return (

      

      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          colors={['#2CA095']}
        />
      }> 

<View style={styles.fill}>
      
      <Swiper
              containerStyle={{ width: WIDTH }}
              key={this.state.dataSource.length}
              containerStyle={{ width: WIDTH, height: HEIGHT / 3}}
              autoplay={true}
              autoplayDelay={5}
              loop={true}
              index={0}
              activeDotColor={'#ffffff'}

              onMomentumScrollEnd={(e, state, context) => console.log('index:', state.index)}
              activeDotColor={'#ffffff'}
              dotColor={'#DCDCDC'}
              key={this.state.dataSource.length}
              loop={true}
              loadMinimal={true} loadMinimalSize={0}
              index={0}


              dotColor={'#ffffff'} >
              {swiperItems}
            </Swiper>
      </View>
  
        <View style={style.container}>

           <Text style={{ fontFamily: "29LTKaff-SemiBold", color: "black", fontSize: 18, paddingTop: 15, paddingBottom: 20, fontWeight: "bold", textAlign: "center" }}>
            Popular Projects
          </Text> 
           
        <FlatGrid style={{ width: WIDTH - 5, marginLeft: 4, marginRight: 0, paddingBottom:10 }}
          itemDimension={WIDTH / 2 - 20}
          items={this.state.projectData}
          renderItem={({ item }) => (
           <TouchableOpacity onPress={() => this.openProject(item.id)}>
             <View  style={{ borderRadius: 12, borderWidth: 2, borderColor: '#f1f1f1', justifyContent: 'center', alignItems: 'center', marginRight: 5, width: WIDTH / 2 - 20, height: WIDTH / 2 - 80 }}>
                <Image source={{ uri: item.photo }} style={{ resizeMode:'contain', width:100, height: 110 }} />
              </View>
            </TouchableOpacity>
            )}
          /> 
 
    <View elevation={1} style={{ borderBottomColor: '#f1f1f1', borderBottomWidth: 2 }} />
          
  
          <View style={style.property}> 
               <Text style={{ fontFamily: "29LTKaff-SemiBold", color: "black", fontSize: 18, paddingTop: 15, paddingBottom: 20, fontWeight: "bold", textAlign: "center" }} animation="slideInLeft" duration={1500} iterationCount={1} direction="alternate"> 
                Property of the day 
              </Text>    
          
              <ScrollView style={{ paddingStart: 10 }} horizontal={true} showsHorizontalScrollIndicator={false}>
                <FlatList horizontal pagingEnabled={true} style={{ flexDirection: "row", flex: 1 }} data={this.state.propertyOftheDay} renderItem={({ item }) => <View>
                
                <TouchableOpacity onPress={() => this.openPotd(item.potd.id)}>
                  <View style={{ flex: 1, height: null, width: null, resizeMode: "contain", paddingStart: 5, width: WIDTH - 35, height: 200 }}>
                     <Image source={{ uri: item.potd.photo }} style={{ marginRight: 15, width: WIDTH - 50, height:200 , flex: 1, borderRadius: 12 }} />
                  </View>
   
                  </TouchableOpacity>
                  <Text style={{ paddingTop: 10, fontFamily: "29LTKaff-SemiBold", color: "black", fontSize: 18, textAlign: "left", marginLeft: 10, fontWeight: "bold" }}>
                    {/*{item.potd.min_price} -  */}{item.potd.max_price} EGP
                   </Text>
                  <Text style={{ fontFamily: "29LTKaff-Regular", color: "black", fontSize: 16, textAlign: "left", marginLeft: 10, fontWeight: "900" }}>
                    {item.potd.title}
                  </Text>
                  <Text numberOfLines={3} ellipsizeMode ={'tail'} style={{ width: WIDTH / 2 + 120, paddingTop: 8, lineHeight: 18, fontFamily: "29LTKaff-Regular", marginBottom: 20, color: "black", fontSize: 14, fontWeight: "normal", textAlign: "justify", marginLeft: 10, margin: 0 }}>
                    {item.potd.details}
                  </Text>
                  
                </View>} keyExtractor={(item, index) => item.toString()} />
              </ScrollView>
          </View>
        </View>


        
      </ScrollView>



    
    );
  }
}
const style = StyleSheet.create({
  container: {
    flex: 1
  },welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: "#000066"
}, welcome: {
  fontSize: 30,
  textAlign: 'center',
  margin: 10,
  color: '#ffffff',
},
talk:{
  height: 200,
  width: 200,
  borderWidth: 5,
  backgroundColor: '#7395AE',
  borderRadius: 100,
  borderColor: '#557A95',
  justifyContent:'center',
  alignItems:'center',
  borderRadius: 100
},
  navBar: {
    height: 55,
    elevation: 1,
    paddingLeft: 155,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  rightNav: {},
  carousel: {
    backgroundColor: "gray"
  },
  projects: {
    backgroundColor: "orange"
  },
  property: {
    color: "black",
    textAlign: "justify",
    marginTop: 10,
  },
  projectText: {
    textAlign: "center"
  },
  projectDetails: {
    flexDirection: "row",
    width: 140,
    height: 100,
    borderRadius: 12,
    paddingTop: 10
  }
});
const styles = StyleSheet.create({
  fill: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 18,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
});