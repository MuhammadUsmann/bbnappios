import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { Dimensions, ScrollView, RefreshControl, Image, AsyncStorage, Text, View, StyleSheet } from 'react-native'
const { width: WIDTH } = Dimensions.get("window");
export default class drawerContentComponents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      image: '',
      refreshing: false,
    };
  }
  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.componentWillMount();
  }
  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    try {
      let response = await fetch(
        "https://bbn-eg.com/broker/api/user?api_token=" + userToken
      );
      let responseJson = await response.json();
      this.setState({
        userName: responseJson[0]['full_name'],
        image: responseJson[0]['photo'],
        dataSource: responseJson,
        refreshing: false
      });
    } catch (error) {
      console.error(error);
    }
  }

  navigateToScreen = (route) => (
    () => {
      const navigateAction = NavigationActions.navigate({
        routeName: route
      });
      this.props.navigation.dispatch(navigateAction);
    })

  render() {
    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          colors={['#2CA095']}
        />
      }>
        <View style={styles.container}>
          <View style={{ paddingBottom:30, paddingTop: 40, width:WIDTH/2 - 30 }}>
            <View style={{ paddingTop: 30, borderBottomColor: '#2CA095', borderBottomWidth: 3 }} />
            <View style={{ paddingTop: 25, flexDirection: 'row', justifyContent: 'center' }}>
              <Image source={{ uri: this.state.image }} style={{ borderColor:'grey', borderRadius: 50, width: 100, height: 100 }} />
            </View>
            <View style={{ paddingTop: 20, flexDirection: 'row', justifyContent: 'center', paddingBottom: 0 }}>
              <Text style={{ fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, fontWeight: 'bold', letterSpacing:0 }}>{this.state.userName}</Text>
            </View>
            <View style={{ paddingBottom:20, borderBottomColor: '#2CA095', borderBottomWidth: 3 }} />
          </View>

          <View style={styles.screenContainer}>
          <View style={styles.screenStyle}>
              <View style={{ width: 20, height: 20, position: 'absolute', left: "auto", right: 'auto', top: 'auto', bottom: 22 }}>
                <Image source={require('../src/assets/drawericons/profile.png')} style={{ position: 'absolute', left: 0, right: 'auto', top: 'auto', bottom: "auto", borderRadius: 10, borderWidth: 0.5, width: 20, height: 20 }} />
              </View>
              <Text style={{ marginLeft: 15, paddingStart: 20, paddingTop: 5, color: 'black', fontFamily: "29LTKaff-Regular", fontSize: 16, fontWeight: '400', textAlignVertical: "center", textAlign: "justify", justifyContent: 'center' }} onPress={this.navigateToScreen('Profile')}>Profile</Text>
          </View>
          <View style={styles.screenStyle}>
              <View style={{  width: 20, height: 20, position: 'absolute', left: "auto", right: 'auto', top: 'auto', bottom: 22 }}>
                <Image source={require('../src/assets/drawericons/help.png')} style={{ position: 'absolute', left: 0, right: 'auto', top: 'auto', bottom: "auto", borderRadius: 10, borderWidth: 0.5, width: 20, height: 20 }} />
              </View>
              <Text style={{ marginLeft: 15, paddingStart: 20, paddingTop: 5, color: 'black', fontFamily: "29LTKaff-Regular", fontSize: 16, fontWeight: '400', textAlignVertical: "center", textAlign: "justify", justifyContent: 'center' }} onPress={this.navigateToScreen('Help')}>Help</Text>
          </View>
          <View style={styles.screenStyle}>
              <View style={{ width: 20, height: 20, position: 'absolute', left: "auto", right: 'auto', top: 'auto', bottom: 22 }}>
                <Image source={require('../src/assets/drawericons/contactUs.png')} style={{ position: 'absolute', left: 0, right: 'auto', top: 'auto', bottom: "auto", borderRadius: 10, borderWidth: 0.5, width: 20, height: 20 }} />
              </View>
              <Text style={{ marginLeft: 15, paddingStart: 20, paddingTop: 5, color: 'black', fontFamily: "29LTKaff-Regular", fontSize: 16, fontWeight: '400', textAlignVertical: "center", textAlign: "justify", justifyContent: 'center' }} onPress={this.navigateToScreen('ContactUs')}>Contact Us</Text>
          </View>
          <View style={styles.screenStyle}>
              <View style={{ width: 20, height: 20, position: 'absolute', left: "auto", right: 'auto', top: 'auto', bottom: 22 }}>
                <Image source={require('../src/assets/drawericons/signOut.png')} style={{ position: 'absolute', left: 0, right: 'auto', top: 'auto', bottom: "auto", borderRadius: 10, borderWidth: 0.5, width: 20, height: 20 }} />
              </View>
              <Text style={{ marginLeft: 15, paddingStart: 20, paddingTop: 5, color: 'black', fontFamily: "29LTKaff-Regular", fontSize: 16, fontWeight: '400', textAlignVertical: "center", textAlign: "justify", justifyContent: 'center' }} onPress={this.navigateToScreen('Logout')}>Sign Out</Text>
          </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  headerContainer: {
    height: 150,
  },
  headerText: {
    color: '#fff8f8',
  },
  screenContainer: {
    paddingTop: 0
  },
  screenStyle: {
    height: 45,
    marginTop: 0,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  screenTextStyle: {
    fontSize: 20,
    marginLeft: 20
  },
  btnLogin: {
    width: 160,
    height: 50,
    borderRadius: 25,
    backgroundColor: "#59BF8F",
    fontFamily: "29LTKaff-Regular",
    marginTop: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },

});