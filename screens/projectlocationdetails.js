import React, { Component } from 'react';
import { RefreshControl, StatusBar, Animated, Platform, AsyncStorage, TouchableWithoutFeedback, FlatList, ActivityIndicator, Text, View, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get("window");
import { FlatGrid } from 'react-native-super-grid';
import {
  Linking
} from "react-native";
import Swiper from 'react-native-swiper';
import Toast, { DURATION } from 'react-native-easy-toast';
import ViewMoreText from 'react-native-view-more-text';

const HEADER_MAX_HEIGHT = 230;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class Projectlocationdetails extends React.Component {
  constructor(props) {
    super(props);
    this.openPdf = this.openPdf.bind(this);
    this.openVideo = this.openVideo.bind(this);
    this.excelFile = this.excelFile.bind(this);

    this.state = {
      scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      isLoading: true,
      banner: [],
      underlayColor:'black',
      photos: [],

      developer: [],
      projects: [],
      dataSource: [],

      details: [],
      bannerImages: [],
      pdfFile: [],
      constructionImage: [],
      excelAvailablity: [],
      videos: [],
      payments: [],
      pdfBtn: Boolean,
      videoBtn: Boolean,
      pdf: true,
      video: true,
      value:''
    };
  }
  componentDidMount() {
    StatusBar.setHidden(true);
}

  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    let projectDetailsId = JSON.stringify(this.props.navigation.state.params.id) || {}
    fetch('https://bbn-eg.com/broker/api/developers/project/' + projectDetailsId + '?api_token=' + userToken)
      .then((response) => response.json())
      .then((findresponse) => {
        this.setState({
          isLoading: false,
          details: findresponse[0]['project'],
          bannerImages: findresponse[0]['photos'],
          pdfFile: findresponse[0]['pdfs'],
          videos: findresponse[0]['videos'],
          payments: findresponse[0]['payments'],
          unitType: findresponse[0]['unit_types'],
          constructionImage: findresponse[0]['con_photos'],
          excelAvailablity: findresponse[0]['availability_file'][0],
          value: findresponse[0]['project'][0]['allow_comments'],
          lat:  findresponse[0]['project'][0]['lat'],
          long: findresponse[0]['project'][0]['long'],
          
      lat:'',
      long:'',
        })


        var a = Object.keys(this.state.pdfFile).length;
        var b = Object.keys(this.state.videos).length;

        // alert("Resopnse: "+ JSON.stringify(findresponse));

        if ((a == 0)) {
          this.setState({
            video: false,
            pdf: true
          })
        }
        if ((b == 0)) {
          this.setState({
            video: true,
            pdf: false
          })
        }
        if ((a == 1) && (b == 1)) {
          this.setState({
            video: false,
            pdf: false
          })
        }

        // alert('this.state.details' + JSON.stringify(this.state.details))


      })



  }

  commentsPage(item) {
    //alert("asd: "+ item);
    this.props.navigation.navigate('Comments', { id: item })
    //this.props.navigation.navigate('Comments')
  }


  getScreen(item) {
    //   alert(item);
    this.setState({
      underlayColor:'#2CA095'
    })
    this.props.navigation.navigate('Propertyunit', { id: item })
  }

  renderViewMore(onPress) {
    return (
      <Text style={{ textAlign: 'right', paddingRight: 5, fontSize: 12 }} onPress={onPress}>View more</Text>

    )
  }

  renderViewLess(onPress) {
    return (
      <Text style={{ textAlign: 'right', paddingRight: 5, fontSize: 12 }} onPress={onPress}>View less</Text>

    )
  }




  openPdf(data) {

    function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key))
          return false;
      }
      return true;
    }

    if (isEmpty(data)) {
      this.refs.toast.show('No PDF available!', 1500, () => {
      })
    } else {
      // alert("Pdf Data: "+ JSON.stringify(data));
      this.props.navigation.navigate('AvailablePdf', { id: data })
    }


  }

  openMap(lat, long){
    var data  = 'https://www.google.com/maps?q='+lat+','+long;
    Linking.openURL(data)
  }
  
  openVideo(data) {
    function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key))
          return false;
      }
      return true;
    }

    if (isEmpty(data)) {
      this.refs.toast.show('No Video available!', 1500, () => {
      })
    } else {
      this.props.navigation.navigate('AvailableVideos', { id: data })
    }
  }
 
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}

  excelFile(data) {

    function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key))
          return false;
      }
      return true;
    }

    if (isEmpty(data)) {
      this.refs.toast.show('No Excel File available!', 1500, () => {
      })
    } else {
      Linking.openURL(data)
    }


  }

  constructionPhotos(data) {

    function isEmpty(obj) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key))
          return false;
      }
      return true;
    }

    if (isEmpty(data)) {
      this.refs.toast.show('No Images available!', 1500, () => {
      })
    } else {
      //  alert("ConstructionPhoto: "+ JSON.stringify(data))
      this.props.navigation.navigate('ConstructionImages', { id: data })
    }

  }

  render() {
       const swiperItems = this.state.bannerImages.map(item => {
      return (
        <Image
          source={{ uri: item.url }}
          style={{ width: WIDTH, height: 230 }}
          key={item.id}
        />
      )
    })

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#26a69a" />
        </View>
      );
    }
    else
    return (


      <ScrollView>
      <View style={{paddingTop:0}}>
      <View style={styles.fill}>
       
            <Swiper
               key={this.state.bannerImages.length}
               style={{ width: WIDTH, height: 230, backgroundColor: 'grey' }}
               autoplay={true}
               autoplayDelay={5}
               loop={true}
               index={0}
               activeDotColor={'white'}
               dotColor={'white'} >
               {swiperItems}
             </Swiper>
      </View>
      <View style={style.container}>
         
         <Toast
           ref="toast"
           style={{ justifyContent: 'center', backgroundColor: '#2CA095', width: WIDTH - 40, height: 50 }}
           position='bottom'
           positionValue={100}
          
           fadeInDuration={750}
           fadeOutDuration={2500}
           opacity={0.9}
           textStyle={{ color: 'white', textAlign: 'center', paddingTop: 5 }}
         />

        
<FlatList style={{ flexDirection: "row", flex: 1 }} data={this.state.details} renderItem={({ item }) => <View>
           {/*  Projects Area starts from here, borderWidth:1, borderColor: '#bdbdbd', elevation={3}    */}
           <View style={{ paddingBottom: 20, borderBottomColor: '#bdbdbd', borderBottomWidth: 1, backgroundColor: '#ffffff', flexDirection: 'row', marginTop: 10, width: WIDTH, height: 'auto' }}>
        
             <View style={{ paddingStart: 10, flexDirection: 'row', width: WIDTH, height: 'auto' }}>
               <View style={{ borderRadius: 12, justifyContent: 'center', alignItems: 'center', marginTop: 15, width: 120, height: 110 }}>

               
                   <View style={{ width: 100, height: 100 }}>

                     <Image source={{ uri: item.photo }} style={{ paddingTop:0, borderRadius: 0, width: 120, height: 120, resizeMode: 'cover', flexWrap: 'wrap' }} />
                   </View>
               
               </View>
               <View style={{ marginLeft: 15, borderRadius: 12, width: WIDTH / 2 + 28, height: 'auto' }}>
              
              
                 <Text style={{ marginTop: 15, paddingStart: 10, fontFamily: "29LTKaff-SemiBold", fontSize: 16, color: 'black', fontWeight: '900' }}>{item.title}<Text style={{ fontFamily: "29LTKaff-Regular", fontSize: 14, color: 'black', fontWeight: 'normal', color: '#2CA095' }}>  {item.location}</Text></Text>

                 <View style={{ marginLeft: 10 }}>
                   <ViewMoreText style={{ fontFamily: "29LTKaff-Regular", height: 'auto' }}
                     numberOfLines={4}
                     renderViewMore={this.renderViewMore}
                     renderViewLess={this.renderViewLess}
                     textstyle={{ paddingStart: 30, fontSize: 13, color: 'black', fontWeight: 'normal', paddingTop: 2, textAlign: 'justify', padding: 10, margin: 0, lineHeight: 15 }}
                   >
                     <Text style={{ fontFamily: "29LTKaff-Regular", fontSize: 13, }}>
                       {item.details} </Text>
                   </ViewMoreText>

                 </View>

               </View>
             </View>
           </View>

           <View style={{ backgroundColor: '#f9f9f9', width: WIDTH - 10, paddingTop: 10, paddingBottom: 10, marginLeft: 'auto', marginRight: 'auto' }}>
         
               <View style={{ width: 30 }}>
               </View>


               <View style={{ width: 30 }}>
               </View>

               <View style={{ flex: 1, width: WIDTH - 10, flexWrap: 'wrap', padding: 10 }}>
              

              {
                /*
                  Linking.openURL(data)
                */
              }


{this.state.lat != 0 ? (   <TouchableOpacity

  onPress={() => this.openMap(this.state.lat, this.state.long)}>

    <Text style={{ marginLeft: 30, marginRight: 30, paddingStart: 10, fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 12, padding: 2, fontWeight: 'normal', textAlign: 'center' }}> <Image source={require('../src/assets/icons/locationIcon.png')} style={{  width: 11, height: 16, left: 30, right: 'auto', top:'auto' }} />
                 <Text style={{ paddingStart: 10, fontWeight: 'bold', fontSize: 15, color: '#2CA095' }}> Location</Text>   {item.map_location}</Text>
             
</TouchableOpacity>
    ) : <View>
        <Text style={{ marginLeft: 30, marginRight: 30, paddingStart: 10, fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 12, padding: 2, fontWeight: 'normal', textAlign: 'center' }}> <Image source={require('../src/assets/icons/locationIcon.png')} style={{  width: 11, height: 16, left: 30, right: 'auto', top:'auto' }} />
        <Text style={{ paddingStart: 10, fontWeight: 'bold', fontSize: 15, color: '#2CA095' }}> Location</Text>   {item.location}</Text>
             
      </View>}


               
             
             
                 <Text style={{ paddingStart: 40, fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 12, padding: 2, fontWeight: 'normal', textAlign: 'center', paddingTop: 3 }}>  <Image source={require('../src/assets/icons/landmark.png')} style={{ paddingRight:15, position: 'absolute', width: 18, height: 18, left: 90, right: 'auto', top:'auto', bottom:22 }} />
              <Text style={{ paddingStart: 10, fontWeight: 'bold', fontSize: 15, color: '#2CA095' }}> Landmark</Text> {item.landmarks}</Text>

               </View>
           </View>

         </View>} keyExtractor={(item, index) => index.toString()} />



         <Text style={{ paddingTop: 15, paddingBottom: 10, fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 2, fontWeight: 'bold', textAlign: 'center' }}>Unit Type</Text>



       
           <FlatGrid style={{ width: WIDTH - 5, marginRight: 0 }}
             itemDimension={WIDTH / 3 - 20}
             items={this.state.unitType}
             renderItem={({ item }) => (
               <TouchableOpacity

                 onPress={() => this.getScreen(item.id)}>
               
                 <View  style={{ justifyContent: 'center', alignItems: 'center', width: WIDTH / 3 - 10, height: 36 }}>
                   <Text style={{ textDecorationLine:'underline', padding: 2, justifyContent: 'center', height: 22, fontFamily: "29LTKaff-SemiBold", fontSize: 14, color: '#2CA095', fontWeight: 'bold', textAlign: 'justify' }}>{item.type}</Text>
  
                 </View>
  
               </TouchableOpacity>
             )} 
           />
       
         <FlatList style={{ flexDirection: "row", flex: 1 }} data={this.state.details} renderItem={({ item }) => <View>
           <View style={{ backgroundColor: '#f9f9f9', width: WIDTH }}>
             <Text style={{ paddingTop: 20, fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 2, fontWeight: 'bold', textAlign: 'center' }}>Price Range</Text>

             <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 16, padding: 2, fontWeight: 'normal', textAlign: 'center', paddingTop: 4, paddingBottom: 20 }}>{item.min_price} M  |  {item.max_price} M </Text>

           </View>
         </View>} keyExtractor={(item, index) => index.toString()} />

         <View style={{ height:30, marginTop: 20, width: WIDTH, paddingBottom: 12, flexDirection: 'row' }}>
           <View style={{ flexDirection: 'row',  marginLeft: 0, justifyContent: 'flex-start', width: WIDTH }}>
             <View style={{ flexDirection: 'row', width:WIDTH }}  >{/*this.openPdfFile(this.state.pdfFile)  Linking.openURL(this.state.videos[0]['url']) Linking.openURL(this.state.pdfFile[0]['url'])  */}
               {/*  disabled={this.state.pdf}  disabled={this.state.pdf}  */}

               <TouchableWithoutFeedback onPress={() => this.openPdf(this.state.pdfFile)}>
                 <View style={{  justifyContent:'center', width: WIDTH / 2, height: 36 }}>
                   <Text style={{ textDecorationLine:'underline',  paddingStart:20, justifyContent: 'center', height: 22, fontFamily: "29LTKaff-SemiBold", fontSize: 14, color: '#2CA095', fontWeight: 'bold', textAlign: 'justify' }}>Open PDFS</Text>
                 </View>
                
               </TouchableWithoutFeedback >
               <TouchableWithoutFeedback onPress={() => this.openVideo(this.state.videos)}>
                 <View style={{ alignItems: 'flex-start', width: WIDTH / 2 - 10, marginLeft: 5, height: 36 }}>
                 <Text style={{ paddingTop:5, textDecorationLine:'underline', paddingStart:10,  justifyContent: 'flex-start', height: 22, fontFamily: "29LTKaff-SemiBold", fontSize: 14,color: '#2CA095', fontWeight: 'bold', textAlign: 'justify' }}>View Video</Text>
                 </View>
               </TouchableWithoutFeedback >
             </View>
           </View>
         </View>

         <View style={{ height:30, marginTop: 5,  marginBottom:10, width: WIDTH, paddingBottom: 12, flexDirection: 'row' }}>
           <View style={{ flexDirection: 'row',  marginLeft: 0, justifyContent: 'flex-start', width: WIDTH }}>
             <View style={{ flexDirection: 'row', width:WIDTH }}  >{/*this.openPdfFile(this.state.pdfFile)  Linking.openURL(this.state.videos[0]['url']) Linking.openURL(this.state.pdfFile[0]['url'])  */}
               {/*  disabled={this.state.pdf}  disabled={this.state.pdf}  */}

               <TouchableWithoutFeedback onPress={() => this.constructionPhotos(this.state.constructionImage)}>
                 <View style={{  alignItems: 'flex-start', width: WIDTH / 2 , marginRight: 5, height: 36 }}>
                   <Text style={{  textDecorationLine:'underline',  paddingStart:20, justifyContent: 'flex-start', height: 22, fontFamily: "29LTKaff-SemiBold", fontSize: 14, color: '#2CA095', fontWeight: 'bold', textAlign: 'justify' }}>Construction Update</Text>
                 </View>
               </TouchableWithoutFeedback >
               <TouchableWithoutFeedback onPress={() => this.excelFile(this.state.excelAvailablity)}>
                 <View style={{ alignItems: 'flex-start', marginLeft: 0, width: WIDTH / 2 - 10, marginRight: 5, height: 36 }}>
                   <Text style={{ textDecorationLine:'underline',  paddingStart:10,  justifyContent: 'flex-start', height: 22, fontFamily: "29LTKaff-SemiBold", fontSize: 14,color: '#2CA095', fontWeight: 'bold', textAlign: 'justify' }}>Availablity</Text>
                 </View>
               </TouchableWithoutFeedback >

             </View>
           </View>
         </View>
         <View elevation={1} style={{ borderBottomColor: '#f1f1f1', borderBottomWidth: 2 }} />
  
         <Text style={{ backgroundColor: 'white', paddingTop: 15, paddingBottom: 15, fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 2, fontWeight: 'bold', textAlign: 'center' }}>Payment Plans</Text>
         <FlatList style={{ flexDirection: "row", flex: 1 }} data={this.state.payments} renderItem={({ item }) =>
           <View elevation={1} style={{ borderRadius:12, borderColor: '#f1f1f1', borderWidth:1,  borderBottomWidth: 2, backgroundColor: '#f9f9f9', width: WIDTH, paddingTop: 4 }}>
             <Text style={{ paddingStart: 30, fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 16, padding: 2, fontWeight: '700', textAlign: 'left' }}>{item.down_payment} Downpayment | {item.years} years | Equal</Text>
             <View style={{ marginLeft: 30, height: 'auto' }}>
             </View>

             <View style={{ marginLeft: 30, paddingRight: 15 }}>
               <ViewMoreText style={{ fontFamily: "29LTKaff-Regular", height: 'auto' }}
                 numberOfLines={1}
                 renderViewMore={this.renderViewMore}
                 renderViewLess={this.renderViewLess}
                 textstyle={{ paddingStart: 30, fontSize: 13, color: 'black', fontWeight: 'normal', paddingTop: 2, textAlign: 'justify', padding: 10, margin: 0, lineHeight: 15 }}
               >
                 <Text style={{ fontFamily: "29LTKaff-Regular", fontSize: 13, }}>
                   {item.details}</Text>
               </ViewMoreText>
             </View>
           </View>} keyExtractor={(item, index) => index.toString()}
         />

{this.state.value != 0 ? (     <FlatList style={{ flexDirection: "row", flex: 1 }} data={this.state.details} renderItem={({ item }) =>
           <View style={{ width: WIDTH, height: 'auto' }}>
             <TouchableWithoutFeedback onPress={() => this.commentsPage(item.id)}>
               <Text style={{ marginTop: 15, fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, padding: 2, fontWeight: 'bold', textAlign: 'center', textDecorationLine: 'underline' }}>Comments <Text style={{ fontFamily: "29LTKaff-Regular", color: 'gray', fontSize: 16, fontWeight: 'normal' }}>({item.comments})</Text></Text>
             </TouchableWithoutFeedback>
           </View>
         } keyExtractor={(item, index) => index.toString()} />
    ) : null}



       </View>
      </View>
      
      </ScrollView>

      
    
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 1,
    paddingLeft: 155,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightNav: {

  },
  carousel: {
    height: 200
  },
  projects: {
    height: 190,
    backgroundColor: 'orange',
  },
  property: {
    height: 400,
    backgroundColor: 'green',
  },
  projectText: {
    textAlign: 'center'
  },
  projectDetails: {
    flexDirection: 'row',
    width: 140,
    height: 100,
    borderRadius: 12,
    paddingTop: 10,
    backgroundColor: 'white'
  }
});

const styles = StyleSheet.create({
  fill: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',//'#03A9F4',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 18,
  },
  scrollViewContent: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default Projectlocationdetails;