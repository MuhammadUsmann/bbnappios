import React, { PureComponent } from 'react';
import { View, Text, NetInfo, Dimensions, StyleSheet, Platform,
    StatusBar } from 'react-native';

const { width } = Dimensions.get('window');
const statusBarHeight = (Platform.OS === 'ios') ? 20 : StatusBar.currentHeight;

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
} 

class OfflineNotice extends PureComponent {
  state = {
    isConnected: true
  };

  componentDidMount() {
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
  }

  handleConnectivityChange = networkStatus => {
    if (networkStatus) {
      this.setState({ isConnected: true });
    }
    else {
      this.setState({ isConnected: false });
    }
    return ;
  }

  render() {
    if (!this.state.isConnected) {
      return <MiniOfflineSign />;
    }
    return null;
  }
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      backgroundColor: '#393939',
      flexDirection: 'row',
      height: 42,
      justifyContent: 'center',
      marginTop: statusBarHeight,
      width: '100%'
    },
    text: {
      color: '#fefefe'
    }
  });

export default OfflineNotice;