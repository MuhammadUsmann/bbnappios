import React, { Component } from 'react';
import {  RefreshControl, StyleSheet, StatusBar, Animated, Platform,AsyncStorage, FlatList, ActivityIndicator, Text, View, Image, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
const { width: WIDTH, height: HEIGHT  } = Dimensions.get("window");
import Swiper from 'react-native-swiper';
import StarRating from 'react-native-star-rating';


const HEADER_MAX_HEIGHT = 230;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class Propertyunit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      isLoading: true,
      banner:[],
      dataSource: [],
      abc: '',
      title: JSON.stringify(this.props.navigation.state.params.id) || {}
    };
    navigationOptions = {
      title: this.state.title,
    };
  }
  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    let id = JSON.stringify(this.props.navigation.state.params.id) || {}
    fetch('https://bbn-eg.com/broker/api/developers/type/' + id + '?api_token=' + userToken)
      .then((response) => response.json())
      .then((findresponse) => {
        this.setState({
          isLoading: false,
          banner: findresponse.photos,
          dataSource: findresponse.type,
          abc: findresponse.photos
        })
      })
  }

  componentDidMount() {
    StatusBar.setHidden(true);
}

componentDidMount() {
  StatusBar.setHidden(true);
}

  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}
  
  render() {
    const swiperItems = this.state.banner.map(item => {
      return (
        <Image
          source={{ uri: item.url }}
          style={{ width: WIDTH, height: 230 }}
          key={item.id}
        />
      )
    })

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#26a69a" />
        </View>
      );
    }
    else
    return (

      
    
<ScrollView >
  
<View style={styles.fill}>
      
      <Swiper
        key={this.state.banner.length}
        style={{width: WIDTH, height: HEIGHT / 3}}
        autoplay={true}
        autoplayDelay={5}
        loop={true}
        index={0}
        activeDotColor={'#ffffff'}
        dotColor={'#ffffff'} >
        {swiperItems}
      </Swiper>
</View>



<FlatList style={{ flexDirection: "row", flex: 1, backgroundColor: '#f1f1f1' }} data={this.state.dataSource} renderItem={({ item }) =>
        <View style={{ width: WIDTH }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
              <View style={{ borderRadius: 12, width: WIDTH - 40 }}>
                <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-SemiBold", fontSize: 18, color: 'black', fontWeight: '900' }}>{item.title}</Text>
              </View>
          </View>
        <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'center', width: WIDTH, height: 40, paddingStart: 20 }}>
              <View style={{ width: WIDTH / 3 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 20 }}>Size</Text>
              </View>
              <View style={{ width: WIDTH / 3,  borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 20 }}>{item.min_size} M</Text>
              </View>
              <View style={{ width: WIDTH / 3, borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 15 }}>{item.max_size} M</Text>
              </View>
            </View>
       
            <View style={{ backgroundColor: '#f1f1f1', flexDirection: 'row', justifyContent: 'center', width: WIDTH, height: 40, paddingStart: 20 }}>
              <View style={{ width: WIDTH / 3 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 20 }}>Price</Text>
              </View>
              <View style={{ width: WIDTH / 3, borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 20 }}>{item.min_price} EGP</Text>
              </View>
              <View style={{ width: WIDTH / 3, borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 15 }}>{item.max_price} EGP</Text>
              </View>
            </View>
       
            <View style={{ backgroundColor: '#f1f1f1', flexDirection: 'row', justifyContent: 'center', width: WIDTH, height: 40, paddingStart: 20 }}>
              <View style={{ width: WIDTH / 3 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 20 }}>Rooms</Text>
              </View>
              <View style={{ width: WIDTH / 3, borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 20 }}>{item.rooms} Rooms</Text>
              </View>
              <View style={{ width: WIDTH / 3, borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 15 }}></Text>
              </View>
            </View>
       
            <View style={{ backgroundColor: '#f1f1f1', flexDirection: 'row', justifyContent: 'center', width: WIDTH, height: 40, paddingStart: 20 }}>
              <View style={{ width: WIDTH / 3, borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 20 }}>Available Units</Text>
              </View>
              <View style={{ width: WIDTH / 3, borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 20 }}>{item.availability}</Text>
              </View>
              <View style={{ width: WIDTH / 3, borderColor: '#bdbdbd', borderBottomWidth:1 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 15 }}></Text>
              </View>
            </View>
        </View>} keyExtractor={(item, index) => index.toString()} />

</ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  fill: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',//'#03A9F4',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 18,
  },
  scrollViewContent: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default Propertyunit;