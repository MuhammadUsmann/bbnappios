import React, { Component } from 'react';
import { AsyncStorage, ToastAndroid, ActivityIndicator, Picker, Text, View, StyleSheet, Button, Image, SearchBar, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');
import RNPickerSelect from 'react-native-picker-select';
import Loading from 'react-native-loader-overlay';
import Toast, { DURATION } from 'react-native-easy-toast';

class Requestinformation extends React.Component {
    static navigationOptions = {
        headerTitleStyle: { justifyContent: 'center' },
        headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
    }

    async componentDidMount() {
        const userToken = await AsyncStorage.getItem('userToken');
        fetch('https://bbn-eg.com/broker/api/projects/list?api_token=' + userToken)
            .then((response) => response.json())
            .then((findresponse) => {
                this.setState({
                    isLoading: false,
                    projects: findresponse.data,
                });
            })
    }

    async searchProperty() {
     
            this.loading = Loading.show({
            color: '#2CA095',
            size: 10,
            overlayColor: 'rgba(0,0,0,0.5)',
            closeOnTouch: false,
            loadingType: 'Bubbles'
          })
        const userToken = await AsyncStorage.getItem('userToken');
        fetch('https://bbn-eg.com/broker/api/requests/add?api_token=' + userToken, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                price: this.state.selectedprice,
                area: this.state.selectedland,//project,
                bua: this.state.selectedbua,
                keyword: this.state.keywords,
                location: this.state.location,
                type: this.state.selectedtype,
                project: this.state.itemValue
            })
        }).then((response) => response.json())
            .then((responseJson) => {
               var self = this;
                function isEmpty(obj) {
                    for (var key in obj) {
                        if (obj.hasOwnProperty(key))
                            return false;
                    }
                    return true;
                }
                if (isEmpty(responseJson.properties)) {

                    Loading.hide(self.loading);
                    self.refs.toast.show('No Property Found!', 3000, () => {
                        self.props.navigation.goBack();
                      
                    });

                } else {

                    self.refs.toast.show('Please wait ...', 1000, () => {
                      //  alert(JSON.stringify(responseJson.properties));
                        self.props.navigation.navigate('RequestedProperties', { id: responseJson.properties })
                   
                      
                    });

                    Loading.hide(this.loading);
                 
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }


    constructor(props) {
        super(props);
        this.searchProperty = this.searchProperty.bind(this);
        this.state = {
            projects: [],
            selectedtype: '',
            selectedproject: '',
            selectedland: '',
            selectedbua: '',
            selectedprice: '',
            keywords: '',
            location: '',
            itemValue: null,

            isLoading: true,

            items: [
                {
                    label: 'Red',
                    value: 'red',
                },
                {
                    label: 'Orange',
                    value: 'orange',
                },
                {
                    label: 'Blue',
                    value: 'blue',
                },
            ],
            favSport: undefined,

            lookingFor: [
                {
                    label: 'Chalet',
                    value: 'Chalet',
                },
                {
                    label: 'Apartment',
                    value: 'Apartment',
                },
                {
                    label: 'Duplex',
                    value: 'Duplex',
                },
                {
                    label: 'Penthouse',
                    value: 'Penthouse',
                },
                {
                    label: 'Town Villa',
                    value: 'Town Villa',
                },
                {
                    label: 'Twin Villa',
                    value: 'Twin Villa',
                },
                {
                    label: 'Stand Alone',
                    value: 'Stand Alone',
                },
                {
                    label: 'Studio',
                    value: 'Studio',
                }
            ],

            price: [
                {
                    label: '1M - 3M',
                    value: '1M - 3M',
                },
                {
                    label: '4M - 6M',
                    value: '4M - 6M',
                },
                {
                    label: '7M - 10M',
                    value: '7M - 10M',
                },
                {
                    label: '10M - 15M',
                    value: '10M - 15M',
                },
                {
                    label: '15M - 20M',
                    value: '15M - 20M',
                },
                {
                    label: '20M - 30M',
                    value: '20M - 30M',
                },
                {
                    label: '30M - 50M',
                    value: '30M - 50M',
                },
                {
                    label: '50M - 100M',
                    value: '50M - 100M',
                },
                {
                    label: '100M - ',
                    value: '100M',
                }
            ],
            land: [
                {
                    label: '0sqm - 100sqm',
                    value: '0sqm - 100sqm',
                },
                {
                    label: '100sqm - 200sqm',
                    value: '100sqm - 200sqm',
                },
                {
                    label: '200sqm - 300sqm',
                    value: '200sqm - 300sqm',
                },
                {
                    label: '300sqm - 400sqm',
                    value: '300sqm - 400sqm',
                },
                {
                    label: '400sqm - 500sqm',
                    value: '400sqm - 500sqm',
                },
                {
                    label: '500sqm - 700sqm',
                    value: '500sqm - 700sqm',
                },
                {
                    label: '700sqm - 900sqm',
                    value: '700sqm - 900sqm',
                },
                {
                    label: '900sqm - 1000sqm',
                    value: '900sqm - 1000sqm',
                },
                {
                    label: '1000sqm - 1500sqm',
                    value: '1000sqm - 1500sqm',
                },
                {
                    label: '1500sqm - 2000sqm',
                    value: '1500sqm - 2000sqm',
                },
                {
                    label: '200sqm - ',
                    value: '200sqm',
                }
            ],
            isLoading: true,
            dataSource: [],
            projectData: [],
            propertyOftheDay: [],
            isConnected: true,
            imageData: ''
        };
    }


    render() {

        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
                </View>
            );
        }
        const pickerStyle = {
            inputIOS: {
                color: 'black',
                paddingTop: 13,
                paddingHorizontal: 10,
                paddingBottom: 10,
                fontFamily: "29LTKaff-Regular",
                fontSize:14,
                height:38

            },
            inputAndroid: {
                color: 'black',
            },
            placeholderColor: 'grey',
            underline: { borderTopWidth: 0 },
            icon: {
                position: 'absolute',
                backgroundColor: 'transparent',
                borderTopWidth: 5,
                borderTopColor: '#00000099',
                borderRightWidth: 5,
                borderRightColor: 'transparent',
                borderLeftWidth: 5,
                borderLeftColor: 'transparent',
                width: 0,
                height: 10,
                top: 18,
                right: 15,
            },
        };

        return (
            <ScrollView>


                <Text style={{ backgroundColor: '#dbdcdd', fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center' }}>
                    <Text style={{ color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize: 18, fontWeight: 'bold' }} >Request Information</Text>
                </Text>


                <Toast
                        ref="toast"
                        style={{ marginBottom: 50, backgroundColor: '#59BF8F', width: WIDTH - 80, height: 'auto' }}
                        position='top'
                        fadeInDuration={750}
                        fadeOutDuration={1000}
                        opacity={0.9}
                        textStyle={{ color: 'white', textAlign: 'center', paddingTop: 0 }}
                    />
            <View style={{ flexDirection: 'row', marginTop: 40, marginBottom: 10, marginLeft: 5, justifyContent: 'center', width: WIDTH - 10 }}>
              
          
                    
              <View style={{ flexDirection: 'row' }}>

                <View style={{ justifyContent: 'center', borderBottomLeftRadius: 12, borderTopRightRadius: 12, fontFamily: "29LTKaff-SemiBold", fontWeight: '900', width: 160, height: 35, borderWidth: 1, borderColor: '#2CA095' }}>
                <RNPickerSelect
                                placeholder={{
                                    label: 'Looking For',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.lookingFor}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedtype: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;

                                    //underline: { opacity: 0 }, justifyContent: 'center', borderRadius: 50, width: WIDTH - 65, height: 40, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' 
                                }}
                            />
                </View>

                <View style={{ marginLeft: 15, justifyContent: 'center', borderBottomLeftRadius: 12, borderTopRightRadius: 12, fontFamily: "29LTKaff-SemiBold", fontWeight: '900', width: 130, height: 35, marginBottom: 2, marginTop: 2, borderWidth: 1, borderColor: '#2CA095' }}>
                <RNPickerSelect
                                placeholder={{
                                    label: 'Project',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.projects}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedtype: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;

                                    //underline: { opacity: 0 }, justifyContent: 'center', borderRadius: 50, width: WIDTH - 65, height: 40, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' 
                                }}
                            />
              

                </View>
              </View>
            </View>
            <View>
            </View>



          <View style={{ marginTop:30, flexDirection: 'row', fontFamily: "29LTKaff-Regular", justifyContent: 'center', marginBottom: 10 }}>
                  
                        <View elevation={3} style={{
                            justifyContent: 'center',
                            borderBottomLeftRadius: 12,
                            borderTopRightRadius: 12,
                            backgroundColor: "#2CA095",
                            fontFamily: "29LTKaff-SemiBold", width: WIDTH / 2 + 40, height: 35, marginBottom: 2, borderWidth: 1,borderWidth: 1, borderColor: '#2CA095', backgroundColor:'white'
                        }}>
                            <RNPickerSelect
                                placeholder={{
                                    label: 'Land',
                                    value: '',
                                    color: '#000000',
                                    marginStart: 20
                                }}
                                items={this.state.land}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedland: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                }}
                                style={pickerStyle}
                                ref={(el) => {
                                }}
                            />
                        </View>
                </View>

                <View style={{ flexDirection: 'row', fontFamily: "29LTKaff-Regular", justifyContent: 'center', marginBottom: 10 }}>
                 
                        <View elevation={3} style={{
                            justifyContent: 'center',
                            borderBottomLeftRadius: 12,
                            borderTopRightRadius: 12,
                            backgroundColor: "#2CA095",
                            fontFamily: "29LTKaff-SemiBold", width: WIDTH / 2 + 40, height: 35, marginBottom: 2, borderWidth: 1, borderWidth: 1, borderColor: '#2CA095', backgroundColor:'white'
                        }}>
                            <RNPickerSelect
                                placeholder={{
                                    label: 'BUA',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.land}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedbua: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;
                                }}
                            />
                        </View>
                </View>

                <View style={{ flexDirection: 'row', fontFamily: "29LTKaff-Regular", justifyContent: 'center', marginBottom: 10 }}>
                 
                        <View elevation={3} style={{
                            justifyContent: 'center',
                            borderBottomLeftRadius: 12,
                            borderTopRightRadius: 12,
                            backgroundColor: "#2CA095",
                            fontFamily: "29LTKaff-SemiBold", width: WIDTH / 2 + 40, height: 35, marginBottom: 2, borderWidth: 1, borderWidth: 1, borderColor: '#2CA095', backgroundColor: 'white'
                        }}>
                            <RNPickerSelect
                                placeholder={{
                                    label: 'Price Range',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.price}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedprice: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;
                                }}
                            />
                        </View>
                </View>

                   <View style={{ flexDirection: 'row', fontFamily: "29LTKaff-Regular", justifyContent: 'center', marginBottom: 10 }}>
                        <View elevation={3} style={{
                            justifyContent: 'center', borderBottomLeftRadius: 12,
                            borderTopRightRadius: 12,
                            backgroundColor: "#2CA095",
                            fontFamily: "29LTKaff-SemiBold", width: WIDTH / 2 + 40, height: 35, marginBottom: 2, borderWidth: 1, borderWidth: 1, borderColor: '#2CA095', backgroundColor: 'white'
                        }}>
                            <TextInput style={{ fontFamily: "29LTKaff-Regular", color: "grey", fontSize: 15, paddingBottom: 0, paddingTop: 10, fontWeight: "400", textAlign: "left", paddingStart: 10 }} placeholder={"Keywords"} placeholderTextColor={"grey"} underlineColorAndroid="transparent" onChangeText={(keywords) => this.setState({ keywords: keywords })} />

                        </View>
                    </View>

                <View style={{ marginTop:30, flexDirection: 'row', justifyContent: 'center' }}>
                  
                        <TouchableOpacity style={style.btnLogin} onPress={() => this.searchProperty()}>
                            <Text style={{ fontFamily: "29LTKaff-SemiBold", fontSize: 18, fontWeight: 'normal', color: 'black' }}>Search</Text>
                        </TouchableOpacity>
             
                </View>

            </ScrollView>
        );
    }
}

const style = StyleSheet.create({
    btnLogin: {
        width: 100,
        height: 40,
        borderBottomLeftRadius: 12,
        borderTopRightRadius: 12,
        backgroundColor: "#2CA095",
        fontFamily: "29LTKaff-SemiBold",
        marginTop: 10,
        marginBottom: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1
    },
    navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 1,
        paddingLeft: 155,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    }
});
export default Requestinformation;