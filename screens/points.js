import React, { Component } from 'react';
import { TouchableOpacity, AsyncStorage, RefreshControl, FlatList, ActivityIndicator, Text, View, Image, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');
import * as Animatable from 'react-native-animatable';


import { FlatGrid } from 'react-native-super-grid';

class Points extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      points: [],
      refreshing: false,
      achievers:[]
    };
  }

  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    fetch('https://bbn-eg.com/broker/api/user/points?api_token='+userToken)
      .then((response) => response.json())
      .then((findresponse) => {
        this.setState({
          isLoading: false,
          points: findresponse,
          achievers:findresponse[0]['achievers'],
          refreshing: false
        })
        })
        
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.componentWillMount();
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
        </View>
      );
    }
    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          colors={['#2CA095']}
        />
      }>

        <View style={{ flex: 1 }}>
        <Animatable.Text style={{ backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center' }} animation="slideInLeft" duration={1000} iterationCount={1} direction="alternate">
        <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:20, fontWeight:'bold'}} >Points</Text>
      </Animatable.Text>

      <FlatList style={{ flexDirection: "row", flex: 1 }} data={this.state.points} renderItem={({ item }) => <View>
            <Animatable.View animation="slideInLeft" iterationCount={1} direction="alternate">
              <View style={{ marginLeft: 20, justifyContent: 'center', width: WIDTH - 40, height: 30, marginTop:15, height:30 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: '#06374F', fontSize: 16, padding: 12, fontWeight: 'normal', textAlign: 'center' }}>You have {item.points} points</Text>
              </View>
            </Animatable.View>
          </View>} keyExtractor={(item, index) => item.toString()} />


          <Animatable.Text style={{ padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center' }} animation="slideInLeft" duration={1000} iterationCount={1} direction="alternate">
        <Text style={{color: '#06374F', fontFamily: "29LTKaff-SemiBold", fontSize:22, fontWeight:'bold'}} >Top Achievers</Text>
      </Animatable.Text>


      <Animatable.View animation="slideInLeft" duration={1500} iterationCount={1} direction="alternate">
            
            <FlatGrid style={{ width: WIDTH - 5, marginLeft: 4, marginRight: 0, paddingBottom:10 }}
              itemDimension={WIDTH / 2 - 20}
              items={this.state.achievers}
              renderItem={({ item }) => (
               <TouchableOpacity onPress={() => this.openProject(item.id)}>
                 
      <View style={{ marginLeft: 10, flexDirection: 'row', marginTop: 10, marginBottom: 10, justifyContent: 'center', width: WIDTH/2 - 30 }}>
              <View style={{  flexDirection: 'row' }}>
                <View  style={{ borderRadius: 12, borderWidth: 1, borderColor: '#cfcfcf', justifyContent: 'center', alignItems: 'center', marginRight: 10, width: WIDTH / 2 - 30, height: 250 }}>
                 <View  style={{width:WIDTH/2 - 30, height:90, top:0, bottom:'auto', left:'auto', right:'auto', borderTopLeftRadius: 12, borderTopRightRadius: 12,  backgroundColor:'#cfcfcf', position:'absolute', zIndex:-1000}}>

                 </View>
                  <Image source={{uri:item.photo}} style={{ borderRadius:40, position:'absolute', bottom:'auto', top:40, padding: 10, width: 80, height: 80, }} />
                  <Text style={{ fontFamily: "29LTKaff-SemiBold", fontSize:14, fontWeight:'bold', color:'black', position:'absolute', top:125, bottom:'auto', left:'auto', right:'auto'}}>{item.name}</Text>
                
                  <Text style={{ width:120, fontFamily: "29LTKaff-Regular", paddingStart:10, justifyContent:'center', fontSize:12, color:'black', fontWeight:'normal', position:'absolute', top:150, bottom:'auto', left:'auto', right:'auto', paddingLeft:5,}}>{item.designation}</Text>
                
                  <Text style={{ fontFamily: "29LTKaff-Regular", fontSize:20, color:'black', fontWeight:'bold', position:'absolute', top:'auto', bottom:15, left:'auto', right:'auto'}}>Points {item.points}</Text>
                
                </View> 
                 
              </View>
            </View>
            
                </TouchableOpacity>
                )}
              />
        </Animatable.View>       
        </View>

      </ScrollView>
    );
  }
}

export default Points;