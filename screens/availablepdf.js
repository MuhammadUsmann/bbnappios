import React, { Component } from 'react';
import { TouchableWithoutFeedback, FlatList, Text, View, StyleSheet, Button, Image, SearchBar, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get("window");

import {
    Linking
  } from "react-native";

class AvailablePdf extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data:[],
            dataIn6fo:false
        };
      }
    
      async componentWillMount() {  
        this.setState({ 
            data:this.props.navigation.state.params.id || []
        })
    } 

    static navigationOptions = {
      headerTitleStyle: { justifyContent: 'center' },
      headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
  }
 
  render() {
      return (
        <ScrollView>
  <View style={{  justifyContent: 'center', alignItems: 'center', backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center' }}>
        <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:18, fontWeight:'bold'}} >Available PDF</Text>
  </View>


 <FlatList style={{ flexDirection: "row", flex: 1, marginTop: 5, }} data={this.state.data} renderItem={({ item }) =>
       <TouchableWithoutFeedback   onPress={() => Linking.openURL(item.url)  }>
     
       <View style={{ paddingBottom: 5, borderBottomColor: '#bdbdbd', borderBottomWidth: 1, backgroundColor: '#ffffff', flexDirection: 'row', marginTop: 10, width: WIDTH, height: 100 }}>
         <View style={{ paddingStart: 20, flexDirection: 'row' }}>
           <View style={{ borderRadius: 12, justifyContent: 'center', alignItems: 'center', marginTop:0, width: 100, height: 100 }}>

             <View style={{ width: 100, height: 'auto' }}>

               <Image source={require('../src/assets/pdfimage.png')} style={{ borderRadius:12, width: 80, height: 90, resizeMode: 'contain' }} />
             </View>

           </View>
           <View style={{ marginLeft:0,  borderRadius: 12, width: WIDTH / 2 + 28, height: WIDTH / 2 - 70 }}>
               <Text style={{ marginTop: 15, paddingStart: 5, fontFamily: "Avenir", fontSize: 16, color: 'black', fontWeight: '900' }}>{item.title}</Text>
               <Text numberOfLines={3} style={{ paddingStart: 5, fontFamily: "Avenir", fontSize: 13, color: 'black', fontWeight: 'normal', paddingTop: 2, textAlign: 'justify', padding: 10, margin: 0, lineHeight: 15 }}>
                 {item.url}
               </Text>
           </View>
         </View>

       </View>
   </TouchableWithoutFeedback >   

} keyExtractor={(item, index) => index.toString()} />
   </ScrollView>);
  }
}
export default AvailablePdf;