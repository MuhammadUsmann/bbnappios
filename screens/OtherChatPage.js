import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  AsyncStorage,
  TextInput,
  FlatList,
  Dimensions, 
  AlertIOS, 
  ActivityIndicator,
  Keyboard
} from 'react-native';
import Loading from 'react-native-loader-overlay';
import { ScrollView } from 'react-native-gesture-handler';
const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');


class OtherChat extends React.Component {
    static navigationOptions = {
        headerTitleStyle: { justifyContent: 'center' },
        headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            comment: '',
            name: '',
            userId: '',
            token: '',
            comment: ''
        };
      }

      replyChat(id) {
            this.loading = Loading.show({
            color: '#2CA095',
            size: 10,
            overlayColor: 'rgba(0,0,0,0.5)',
            closeOnTouch: false,
            loadingType: 'Bubbles'
          })
        if (this.state.comment != '') {

            fetch('https://bbn-eg.com/broker/api/chats/?api_token=' + this.state.token, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    to: id,
                    message: this.state.comment
                })
            }).then((response) => response.json())
                .then((responseJson) => {
                  AlertIOS.alert(
                        responseJson['message']
                    );
                    Loading.hide(this.loading);
                    this.setState({
                        comment: ''
                    })
                    this.componentWillMount();
                })
                .catch((error) => {
                    console.error(error);
                });
        } else {
          AlertIOS.alert(
                "Please Enter Comment First!"
            );
           Loading.hide(this.loading);
        }
    }

    async componentWillMount() {
        const userToken = await AsyncStorage.getItem('userToken');
        this.state.token = userToken;
        let chatId = JSON.stringify(this.props.navigation.state.params.id) || {}
        this.state.token = userToken;
        fetch('https://bbn-eg.com/broker/api/chats/user/' + chatId + '?api_token=' + userToken)
            .then((response) => response.json())
            .then((findresponse) => {
                this.setState({
                    isLoading: false,
                    dataSource: findresponse['chats'],
                    name: findresponse['user'][0]['name'],
                    userId: findresponse['user'][0]['id']
                })
           //  alert("Response: " + JSON.stringify(this.state.dataSource));
            })
    }


    
      renderDate = (date) => {
        return(
          <Text style={styles.time}>
            {
                /*
                    {date}
                */
            }
          </Text>
        );
      }
    
      render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
                </View>
            );
        }

        return (

         
         

          <View style={styles.container}>
               <View style={{ marginBottom:10, borderBottomColor: 'black', borderBottomWidth: 0.5, justifyContent: 'center', flexDirection: 'row', width: WIDTH, height: 50 }}>
                    <Text style={{ marginTop: 15, marginBottom: 10, color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize: 18, fontWeight: 'bold' }}>{this.state.name}</Text>
               </View>
            <FlatList style={styles.list}
              data={this.state.dataSource}
              keyExtractor= {(item) => {
                return item.id;
              }}
              renderItem={(message) => {
                console.log(item);
                const item = message.item;
                let inMessage = item.type === 'in';
                let itemStyle = inMessage ? styles.itemIn : styles.itemOut;
                //let itemStyle = inMessage ? styles.itemOut : styles.itemIn;
                return (
                    <View style={[styles.item, itemStyle]}>
                      
                      {
                          /*
                            {!inMessage && this.renderDate(item.date)}
                      
                          */
                      }         
                       <View style={[styles.balloon]}>
                         <Text>{item.message}</Text>
                       </View>
                       {inMessage && this.renderDate(item.date)}
                          
                     </View>
                )
                
              }}/>
              
            <View style={styles.footer}>
              <View style={styles.inputContainer}>
                <TextInput style={styles.inputs}
                    placeholder="Enter your reply ..."
                    underlineColorAndroid='transparent'
                    value={this.state.comment}
                    returnKeyLabel='Done' 
                    returnKeyType='done' 
                    onSubmitEditing={Keyboard.dismiss}
                    onChangeText={(comment) => this.setState({ comment: comment })}/>
                    
              </View>
    
                <TouchableOpacity style={styles.btnSend} onPress={() => this.replyChat(this.state.userId)}>
                  <Image source={{uri:"https://png.icons8.com/small/75/ffffff/filled-sent.png"}} style={styles.iconSend}  />
                </TouchableOpacity>
            </View>
          </View>
          
        
        );
      }
}

export default OtherChat;



const styles = StyleSheet.create({
    container:{
      backgroundColor:'#f5f5f7',
      fontFamily: "29LTKaff-Regular",
      color:'black',
      flex:1
    },
    list:{
      paddingHorizontal: 20,
      marginTop:15 
    },
    footer:{
      flexDirection: 'row',
      height:60,
      backgroundColor: '#eeeeee',
      paddingHorizontal:10,
      padding:5,
      marginTop:20
    },
    btnSend:{
      backgroundColor:"#2CA095",
      width:37,
      height:37,
      borderRadius:360,
      alignItems:'center',
      justifyContent:'center',
      fontFamily: "29LTKaff-Regular",
      marginTop:8

   
    },
    iconSend:{
      width:25,
      height:25,
      alignSelf:'center',
      marginLeft:5,
    },
    inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      height:40,
      flexDirection: 'row',
      alignItems:'center',
      flex:1,
      fontFamily: "29LTKaff-Regular",
      marginRight:10,
      marginTop:5,
      paddingTop: 5
    },
    inputs:{
      height:50,
      marginLeft:30,
      paddingLeft:15,
      borderBottomColor: '#FFFFFF',
      fontFamily: "29LTKaff-Regular",
      flex:1,
    },
    balloon: {
      maxWidth: 250,
      padding: 10,
      borderRadius: 0,
      color:'black',
      fontFamily: "29LTKaff-Regular"
    },
    itemIn: {
      alignSelf: 'flex-start',
     // alignSelf: 'flex-end',
      fontFamily: "29LTKaff-Regular",
      marginTop:5,
      marginBottom:5
    },
    itemOut: {
      alignSelf: 'flex-end',
    // alignSelf: 'flex-start',
      backgroundColor:'#f2ffe5',
      fontFamily: "29LTKaff-Regular",
      marginTop:5,
      marginBottom:5
    },
    time: {
      alignSelf: 'flex-end',
      margin: 0,
      fontSize:12,
      color:"#808080",
      fontFamily: "29LTKaff-Regular",
      width:0,
    },
    item: {
      marginVertical: 15,
     // flex: 1,
      flexDirection: 'row',
      backgroundColor:"#ffffff",
      fontFamily: "29LTKaff-Regular",
      borderRadius:0,
    },
  });  