import React, { Component } from "react";
import {
  FlatList,
  ActivityIndicator,
  Slider,
  Text,
  View,
  StyleSheet,
  Button,
  Image,
  Dimensions,
  TouchableOpacity,
  Linking,
  Picker,
  NetInfo,
  ToastAndroid,
  AsyncStorage,
  Animated,
  Easing,
  TouchableWithoutFeedback,
  RefreshControl
} from "react-native";

const { width: WIDTH, height: HEIGHT } = Dimensions.get("window");

import { ScrollView } from "react-native-gesture-handler";

class BannerDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      title: "",
      image: "",
      date: "",
      description: "",
      dataSource: []
    };
  }
  async componentWillMount() {
    let bannerId = JSON.stringify(this.props.navigation.state.params.id) || {};
    fetch("https://bbn-eg.com/broker/api/banners/detail/" + bannerId)
      .then(response => response.json())
      .then(findresponse => {
        this.setState({
          isLoading: false,
          title: findresponse.banner["title"],
          image: findresponse.banner["image"],
          date: findresponse.banner["created_at"],
          description: findresponse.banner["description"]
        });

        //    alert(JSON.stringify(findresponse));
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator
            style={{
              flex: 1,
              justifyContent: "center",
              flexDirection: "row",
              justifyContent: "space-around",
              padding: 10
            }}
            size="large"
            color="#26a69a"
          />
        </View>
      );
    } else
      return (
        <ScrollView style={{ backgroundColor: "#dbdcdd" }}>
         
<View
                  style={{
                    backgroundColor: "#dbdcdd",
                    flexDirection: "row",
                    justifyContent: "center",
                    marginTop:10
                  }}
                >
                  <View
                    style={{
                      marginTop: 10,
                      borderRadius: 12,
                      width: WIDTH - 20
                    }}
                  >
                 
                    <View
                      style={{ paddingTop: 10, paddingLeft:10, marginBottom:15, paddingBottom:10, backgroundColor: "#f1f1f1" }}
                    >
                      <Text
                        style={{
                          paddingStart: 6,
                          fontFamily: "29LTKaff-Regular",
                          fontSize: 18,
                          color: "black",
                          fontWeight: "900"
                        }}
                      >
                        {this.state.title}
                      </Text>
                      <Text
                        style={{
                          paddingStart: 6,
                          fontFamily: "29LTKaff-Regular",
                          fontSize: 12,
                          color: "black",
                          fontWeight: "400",
                          marginLeft: 0
                        }}
                      >
                        {this.state.date}
                      </Text>
                    </View>
                  </View>
                </View>
          <View>
            <View style={{ marginLeft:10, marginBottom:15,  width: WIDTH, height: 210 }}>
              <Image
                source={{ uri: this.state.image }}
                style={{ flex: 1, width: WIDTH - 20, height: "auto" }}
              />
            </View>

</View>



<View
              style={{
                backgroundColor: "white",
                width: WIDTH - 20,
                marginLeft: 10,
                marginBottom: 10,
                textAlign: "justify",
                paddingLeft: 0,
                height:'auto',
                paddingTop: 0,
                margin: 0,
                paddingBottom:15
              }}
            >
              <Text
                style={{
                  fontFamily: "29LTKaff-Regular",
                  color: "black",
                  fontSize: 14,
                  fontWeight: "normal",
                  padding:15
                }}
              >
                {this.state.description}
              </Text>
            </View>

           
        </ScrollView>
      );
  }
 
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}

}
export default BannerDetails;
