import React, { Component } from 'react';
import { AsyncStorage, RefreshControl, StatusBar, Animated, Platform, ActivityIndicator, FlatList, Text, View, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH, height: HEIGHT  } = Dimensions.get('window');

class Propertydetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: [],
      bua: null,
      tokenId: "",
      checkUser: false,
      userToken: ""
    };
  }
  componentDidMount() {
    StatusBar.setHidden(true);
}

async componentDidMount() {
  const userToken = await AsyncStorage.getItem("userToken");
  this.state.userToken = userToken;
  let propertyId =
    JSON.stringify(this.props.navigation.state.params.id) || {};

  try {
    let response = await fetch(
      "https://bbn-eg.com/broker/api/property/" +
        propertyId +
        "?api_token=" +
        userToken
    );
    let responseJson = await response.json();
    this.setState({
      isLoading: false,
      dataSource: responseJson,
      tokenId: responseJson[0]["token"].replace(/['"]+/g, '')
    });

   // alert(JSON.stringify(this.state.dataSource));
    
  } catch (error) {
    alert(error);
  }
}

contact(item) {
  // alert(item);
  this.props.navigation.navigate("OtherChat", { id: item });
  //  this.props.navigation.navigate('', { id: item })
}
static navigationOptions = {
  headerTitleStyle: { justifyContent: 'center' },
  headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}



  render() {
    
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#26a69a" />
        </View>
      );
    }
    else
    return (




      <ScrollView> 
      <View style={{paddingTop:0}}>

      <View style={styles.fill}>
        
        <FlatList data={this.state.dataSource} renderItem={({ item }) =>
       <View>
           <View style={{ width: WIDTH, height: 230 }}>
             <Image source={{ uri: item.photos['0'] }} style={{ flex: 1, width: WIDTH, height: 'auto' }} />
           </View>
         </View>
     }
       keyExtractor={(item, index) => index.toString()} />


    
   </View>
   <ScrollView>
        <View style={{ paddingTop: 0 }}>
          <FlatList
            data={this.state.dataSource}
            renderItem={({ item }) => (
              <View>
                <View
                  style={{
                    backgroundColor: "#f1f1f1",
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      marginTop: 10,
                      borderRadius: 12,
                      width: WIDTH - 40
                    }}
                  >

                    <View
                      style={{ paddingTop: 10, backgroundColor: "#f1f1f1" }}
                    >
                      <Text
                        style={{
                          paddingStart: 6,
                          fontFamily: "29LTKaff-Regular",
                          fontSize: 18,
                          color: "black",
                          fontWeight: "900"
                        }}
                      >
                        {item.title}
                      </Text>
                      <Text
                        style={{
                          paddingStart: 6,
                          fontFamily: "29LTKaff-Regular",
                          fontSize: 15,
                          color: "black",
                          fontWeight: "400",
                          marginLeft: 0
                        }}
                      >
                        {item.max_price} EGP
                      </Text>
                      <Text
                        style={{
                          paddingStart: 6,
                          fontFamily: "29LTKaff-Regular",
                          fontSize: 15,
                          color: "black",
                          fontWeight: "normal",
                          marginLeft: 0
                        }}
                      >
                        {item.location}
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    backgroundColor: "#f1f1f1",
                    paddingTop: 10,
                    flexDirection: "row",
                    width: WIDTH,
                    height: "auto"
                  }}
                >
                  <View
                    style={{ justifyContent: "flex-start", width: WIDTH / 2 }}
                  >
                    <Text
                      style={{
                        padding: 5,
                        paddingStart: 50,
                        fontFamily: "Avenir",
                        fontSize: 15,
                        color: "black",
                        fontWeight: "normal"
                      }}
                    >
                      {item.rooms} Rooms
                    </Text>
                  </View>
                  <View
                    style={{ justifyContent: "flex-start", width: WIDTH / 2 }}
                  >
                    <Text
                      style={{
                        padding: 5,
                        paddingStart: 20,
                        fontFamily: "Avenir",
                        fontSize: 15,
                        color: "black",
                        fontWeight: "normal"
                      }}
                    >
                      {item.type}
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    paddingBottom: 10,
                    backgroundColor: "#f1f1f1",
                    flexDirection: "row",
                    width: WIDTH,
                    height: "auto"
                  }}
                >
                  <View
                    style={{ justifyContent: "flex-start", width: WIDTH / 2 }}
                  >
                    <Text
                      style={{
                        padding: 0,
                        paddingStart: 50,
                        fontFamily: "Avenir",
                        fontSize: 15,
                        color: "black",
                        fontWeight: "normal"
                      }}
                    >
                      {item.baths} Bathrooms
                    </Text>
                  </View>
                </View>

                <Text
                  style={{
                    paddingStart: 30,
                    marginTop: 10,
                    fontFamily: "29LTKaff-SemiBold",
                    color: "black",
                    fontSize: 18,
                    marginBottom: 5,
                    fontWeight: "bold"
                  }}
                >
                  Details
                </Text>

                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    width: WIDTH - 60,
                    height: 35,
                    paddingStart: 60
                  }}
                >
                  <View
                    style={{
                      borderBottomColor: "#bdbdbd",
                      borderBottomWidth: 1,
                      width: WIDTH / 2 - 50 - 30
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "29LTKaff-Regular",
                        color: "black",
                        fontSize: 14,
                        paddingBottom: 10,
                        paddingTop: 10,
                        fontWeight: "400",
                        textAlign: "left",
                        paddingStart: 0
                      }}
                    >
                      Land Area
                    </Text>
                  </View>
                  <View
                    style={{
                      borderBottomColor: "#bdbdbd",
                      borderBottomWidth: 1,
                      width: WIDTH / 2 + 50 - 30
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "29LTKaff-Regular",
                        color: "grey",
                        fontSize: 14,
                        paddingBottom: 10,
                        paddingTop: 10,
                        fontWeight: "400",
                        textAlign: "left",
                        paddingStart: 0
                      }}
                    >
                      {item.size} m.{" "}
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    marginBottom: 20,
                    flexDirection: "row",
                    justifyContent: "center",
                    width: WIDTH - 60,
                    height: 35,
                    paddingStart: 60
                  }}
                >
                  <View
                    style={{
                      borderBottomColor: "#bdbdbd",
                      borderBottomWidth: 1,
                      width: WIDTH / 2 - 50 - 30
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "29LTKaff-Regular",
                        color: "black",
                        fontSize: 14,
                        paddingBottom: 10,
                        paddingTop: 10,
                        fontWeight: "400",
                        textAlign: "left",
                        paddingStart: 0
                      }}
                    >
                      BUA
                    </Text>
                  </View>
                  <View
                    style={{
                      borderBottomColor: "#bdbdbd",
                      borderBottomWidth: 1,
                      width: WIDTH / 2 + 50 - 30
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "29LTKaff-Regular",
                        color: "grey",
                        fontSize: 14,
                        paddingBottom: 10,
                        paddingTop: 10,
                        fontWeight: "400",
                        textAlign: "left",
                        paddingStart: 0
                      }}
                    >
                      {item.area}

                    </Text>
                  </View>
                </View>

                <Text
                  style={{
                    paddingStart: 30,
                    backgroundColor: "#f1f1f1",
                    paddingTop: 15,
                    paddingBottom: 0,
                    fontFamily: "29LTKaff-SemiBold",
                    color: "black",
                    fontSize: 18,
                    fontWeight: "bold"
                  }}
                >
                  Property Description
                </Text>

                <View
                  style={{
                    backgroundColor: "#f1f1f1",
                    width: WIDTH,
                    marginBottom: 10,
                    textAlign: "justify",
                    paddingLeft: 30,
                    paddingTop: 20,
                    margin: 0
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "29LTKaff-Regular",
                      color: "black",
                      fontSize: 14,
                      fontWeight: "normal"
                    }}
                  >
                    {item.description}
                  </Text>
                </View>

                {this.state.userToken == this.state.TokenId ? (
                  <View
                    style={{ flexDirection: "row", justifyContent: "center" }}
                  >
                    <TouchableOpacity
                      onPress={() => this.contact(item.contact_id)}
                      style={style.btnLogin}
                    >
                      <Text
                        style={{
                          fontFamily: "29LTKaff-SemiBold",
                          fontSize: 18,
                          fontWeight: "normal",
                          color: "black"
                        }}
                      >
                        Contact
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null}
              </View>
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </ScrollView>
      </View>
     </ScrollView>

      
     
    );
  }
}
const style = StyleSheet.create({
  btnLogin: {
    width: 100,
    height: 40,
    
    borderBottomLeftRadius: 18,
    borderTopRightRadius: 18,
    backgroundColor: "#2CA095",
    fontFamily: "29LTKaff-SemiBold",
    marginTop: 15,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
const styles = StyleSheet.create({
  fill: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 18,
  },
  scrollViewContent: {
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default Propertydetails; 