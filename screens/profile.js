import React, { Component } from 'react';
import { Keyboard, AsyncStorage, RefreshControl, AlertIOS, ActivityIndicator, Text, View, StyleSheet, Button, Image, SearchBar, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');

import ImagePicker from 'react-native-image-crop-picker';

class ChatPage extends React.Component {
  constructor(props) {
    super(props);
    this.selectImages = this.selectImages.bind(this);
    this.state = {
      isLoading: false,
      avatarSource: '',
      userName: '',
      phoneNumber: '',
      about: '',
      position: '',
      companyName: '',
      email: '',
      bbnNumber: '',
      token:''
    };
  }
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}

  selectImages() {
    ImagePicker.openPicker({
      mediaType: 'photo',
      multiple: true,
      includeBase64: true,
      maxFiles: 3
    }).then(images => {
      this.setState({
        avatarSource: "data:image/png;base64," + images[0]['data']
      })
    })
  }

  updateProfile() {
    fetch('https://bbn-eg.com/broker/api/user?api_token='+this.state.token, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        photo: this.state.avatarSource,
        phone: this.state.phoneNumber,
        about: this.state.about,
        position: this.state.position,
        company: this.state.companyName,
        bbn_number: this.state.bbnNumber,
        full_name: this.state.userName
      })
    }).then(function (response) {
      return response.json();
    }).then(function (data) {
      if (data['message'] == 'Preferences Updated successfully...') {

        AlertIOS.alert(
          'Profile Updated Successfully!'
        )
        //ToastAndroid.show('Profile Updated Successfully!', ToastAndroid.SHORT);
      }
      else {
        AlertIOS.alert(
          'Profile Not Updated!'
        )
        //ToastAndroid.show('Profile Not Updated!', ToastAndroid.SHORT);
      }
      //  alert("Data"+JSON.stringify(data));   
    });
  }  

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.componentWillMount();
  }

  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    this.setState({
      token:userToken
    })
    fetch('https://bbn-eg.com/broker/api/user?api_token='+userToken).then((response) => response.json())
      .then((result) => {
        this.setState({
          isLoading: false,
          refreshing: false,
          imgUrl: result,
          avatarSource: result[0]['photo'],
          userName: result[0]['full_name'],
          phoneNumber: result[0]['phone'],
          about: result[0]['about'],
          position: result[0]['position'],
          companyName: result[0]['company'],
          email: result[0]['email'],
          bbnNumber: result[0]['bbn_number'],
        })
      })

  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
        </View>
      );
    }

    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          colors={['#2CA095']}
        />
      }>
      
        <View style={style.container}>
          <Text style={{ fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 20, paddingBottom: 0, paddingTop: 10, fontWeight: 'bold', textAlign: 'center' }}>Profile</Text>
        </View>

           <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.selectImages()}>
              <Image source={{ uri: this.state.avatarSource }} style={{ backgroundColor: 'grey', borderRadius: (WIDTH/2)/2, borderColor: 'grey', borderWidth: 0.5, width:WIDTH/2, height: WIDTH/2, }} />
            </TouchableOpacity>
          </View>
     
          <View style={{ backgroundColor:'#f1f1f1', flexDirection: 'row', marginTop: 0, justifyContent: 'center' }}>
            <Text style={{ fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, paddingTop: 10, fontWeight: 'bold', textAlign: 'center', letterSpacing:0 }}>{this.state.userName}</Text>
          </View>

          <View style={{ backgroundColor:'#f1f1f1', flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 16, paddingTop: 0, paddingBottom:10, fontWeight: '400', textAlign: 'center' }}>{this.state.email}</Text>
          </View>
     
          <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
            <View style={{ width: WIDTH / 2 - 30 }}>
              <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>User Name</Text>
            </View>
            <View style={{ width: WIDTH / 2 + 30 }}>
              <TextInput
                style={{backgroundColor:'#f1f1f1',
                  fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
                }}
                placeholder={'.com'}
                placeholderTextColor={'grey'}
                returnKeyLabel='Done' 
                returnKeyType='done' 
                onSubmitEditing={Keyboard.dismiss}
                underlineColorAndroid='transparent'
                value={this.state.userName}
                onChangeText={(userName) => this.setState({ userName: userName })}
              />
            </View>
          </View>
      
          <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
            <View style={{ width: WIDTH / 2 - 30 }}>
              <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>Phone number</Text>
            </View>
            <View style={{ width: WIDTH / 2 + 30 }}>
              <TextInput
                style={{
                  fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
                }}
                placeholder={'+92 123 23 234 2 34'}
                returnKeyLabel='Done' 
                returnKeyType='done' 
                onSubmitEditing={Keyboard.dismiss}
                placeholderTextColor={'grey'}
                underlineColorAndroid='transparent'
                value={this.state.phoneNumber}
                keyboardType='numeric'
                onChangeText={(phoneNumber) => this.setState({ phoneNumber: phoneNumber })}
              />
            </View>
          </View>
      
          <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
            <View style={{ width: WIDTH / 2 - 30 }}>
              <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>About</Text>
            </View>
            <View style={{ width: WIDTH / 2 + 30 }}>
              <TextInput
                style={{
                  fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
                }}
                placeholder={'Type something about you'}
                placeholderTextColor={'grey'}
                returnKeyLabel='Done' 
                returnKeyType='done' 
                onSubmitEditing={Keyboard.dismiss}
                underlineColorAndroid='transparent'
                value={this.state.about}
                onChangeText={(about) => this.setState({ about: about })}
              />
            </View>
          </View>
       
          <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
            <View style={{ width: WIDTH / 2 - 30 }}>
              <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>Position</Text>
            </View>
            <View style={{ width: WIDTH / 2 + 30 }}>
              <TextInput
                style={{
                  fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
                }}
                placeholder={'Type your title'}
                placeholderTextColor={'grey'}
                returnKeyLabel='Done' 
                returnKeyType='done' 
                onSubmitEditing={Keyboard.dismiss}
                underlineColorAndroid='transparent'
                value={this.state.position}
                onChangeText={(position) => this.setState({ position: position })}
              />
            </View>
          </View>
      
          <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
            <View style={{ width: WIDTH / 2 - 30 }}>
              <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>Company name</Text>
            </View>
            <View style={{ width: WIDTH / 2 + 30 }}>
              <TextInput
                style={{
                  fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
                }}
                placeholder={'Type your company name'}
                placeholderTextColor={'grey'}
                returnKeyLabel='Done' 
                returnKeyType='done' 
                onSubmitEditing={Keyboard.dismiss}
                underlineColorAndroid='transparent'
                value={this.state.companyName}
                onChangeText={(companyName) => this.setState({ companyName: companyName })}
              />
            </View>
          </View>
      
          <View style={{ backgroundColor:'#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', width: WIDTH, height: 40 }}>
            <View style={{ width: WIDTH / 2 - 30 }}>
              <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>BBN Number</Text>
            </View>
            <View style={{ width: WIDTH / 2 + 30 }}>
              <TextInput
                style={{
                  fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 2, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 10
                }}
                placeholder={'0000'}
                placeholderTextColor={'grey'}
                returnKeyLabel='Done' 
                returnKeyType='done' 
                onSubmitEditing={Keyboard.dismiss}
                underlineColorAndroid='transparent'
                keyboardType='numeric'
                onChangeText={(bbnNumber) => this.setState({ bbnNumber: bbnNumber })}
                value={`${this.state.bbnNumber}`}
              />
            </View>
          </View>
    
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => this.updateProfile()} style={style.btnLogin}>
              <Text style={{ fontFamily: "29LTKaff-SemiBold", fontSize: 16, fontWeight: 'normal', color: 'black' }}>Update Profile</Text>
            </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const style = StyleSheet.create({
  btnLogin: {
    width: 160,
    height: 50,
    borderBottomLeftRadius: 18,
    borderTopRightRadius: 18,
    backgroundColor: "#2CA095",
    fontFamily: "29LTKaff-SemiBold",
    marginTop: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1
  }
});

export default ChatPage; 