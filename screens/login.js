import React, { Component } from 'react';
import { AsyncStorage, Text, View, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
const { width: WIDTH } = Dimensions.get('window');
import { TextInput } from 'react-native-gesture-handler';
import bgImage from '../src/assets/LoginBg.png';
import Toast, { DURATION } from 'react-native-easy-toast';
class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      textInputData: '',
      getValue: true
    }
  }

  saveValueFunction = () => {
    AsyncStorage.setItem('Login', "false");
  };

  getValueFunction = () => {
    AsyncStorage.getItem('Login').then(value =>
      this.setState({ getValue: value })
    );
  };

  static navigationOptions = {
    headerTitle: (<Image style={{ marginRight: 56, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
  }

  CheckTextInput = () => {
    if (this.state.username != '') {
      if (this.state.password != '') {
        this.login();
      } else {
        this.refs.toast.show('Please enter a password!', 500, () => {
        })
      }
    } else {
      this.refs.toast.show('Please enter a username!', 500, () => {
      })
    }
  };

  _bootstrapAsync = async (data) => {
    //alert("Data"+data);
    await AsyncStorage.setItem('userToken', data);
    const userToken = await AsyncStorage.getItem('userToken');
     //alert("Data"+JSON.stringify(userToken));
   
    this.props.navigation.navigate(userToken ? 'Dashboard' : 'Welcome');
  };

  login() {
    fetch('https://bbn-eg.com/broker/api/login/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })

    }).then((response) => response.json())
      .then((responseJson) => {
        if (responseJson['msg'] == 'login successfull') {

          this.refs.toast.show(responseJson['msg'], 500, () => {
            this._bootstrapAsync(responseJson['token']);
          });
        }
        else {
          this.refs.toast.show("Error while Signin", 500, () => {
            this._bootstrapAsync(responseJson['token']);
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }


  render() {
    if (this.state.getValue) {
      return (
        <ImageBackground source={bgImage} style={loginStyles.backgroundContainer}>
          <View style={{ width: WIDTH - 20, height: 50 }}>
            <Toast
              ref="toast"
              style={{ backgroundColor: '#80cbc4', width: WIDTH - 40, height: 50 }}
              position='top'
              positionValue={40}
              fadeInDuration={750}
              fadeOutDuration={2500}
              opacity={0.9}
              textStyle={{ color: 'white', textAlign: 'center', paddingTop: 5 }}
            />
          </View>

            <View style={{ alignItems: 'center' }}>
              <Text style={loginStyles.logoText}>L O G I N</Text>
            </View>
      
            <View>
              <TextInput
                style={loginStyles.input}
                placeholder={'username'}
                onChangeText={(value) => this.setState({ username: value })}
                placeholderTextColor={'rgba(255,255,255,0.7)'}
              />
            </View>

            <View style={{ justifyContent:"center", flexDirection:"row",  }}>
            <View style={{borderBottomColor: '#2CA095', borderBottomWidth: 2, width:WIDTH/2 + 60}} />
            </View>
    
         <View style={{ paddingTop: 5 }}>
              <TextInput
                style={loginStyles.input}
                placeholder={'password'}
                onChangeText={(value) => this.setState({ password: value })}
                secureTextEntry={true}
                placeholderTextColor={'rgba(255,255,255,0.7)'}
                underlineColorAndroid='#2CA095'
              />
            </View>
            <View style={{ justifyContent:"center", flexDirection:"row",  }}>
            <View style={{borderBottomColor: '#2CA095', borderBottomWidth: 2, width:WIDTH/2 + 60}} />
            </View>
   
      
            <TouchableOpacity style={loginStyles.btnLogin} onPress={() => this.CheckTextInput()} >
              <Image style={{ flex: 1, width: null, height: null, resizeMode: 'cover' }} source={require('../src/assets/lgnBtn.png')} />
            </TouchableOpacity>
       
           <View style={{ paddingTop: 30 }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('NewAccount')}>
                <Text style={loginStyles.newaccount}>NEW ACCOUNT</Text>
              </TouchableOpacity>
            </View>


            <View style={{ paddingTop: 10 }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgetPassword')}>
                <Text style={loginStyles.forgetPassword}>FORGOT PASSWORD</Text>
              </TouchableOpacity>
            </View>
        </ImageBackground>
      );
    }

  }}

const loginStyles = StyleSheet.create({
  newaccount: {
    color: '#ffffff',
    fontSize: 14,
    textAlign: 'center',
    fontFamily: "29LTKaff-Regular",
    paddingTop: 10
  },
  forgetPassword: {
    color: '#ffffff',
    fontSize: 14,
    textAlign: 'center',
    fontFamily: "29LTKaff-Regular",
    paddingTop: 5
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: "29LTKaff-Regular",
    paddingTop: 22
  },
  btnLogin: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: "gray",
    fontFamily: "29LTKaff-Regular",
    marginTop: 30
  },
  input: {
    width: WIDTH - 100,
    height: 30,
    fontSize: 16,
    textAlign: 'center',
    fontFamily: "29LTKaff-Regular",
    marginTop: 5,
    paddingTop: 25,
    color: '#ffffff',
    paddingBottom: 0,
    justifyContent: "center",
    alignItems: "stretch",
    height: 50,
    marginHorizontal: 25
  },
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
 //   opacity: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: "29LTKaff-Regular",
  },
  logoText: {
    color: '#ffffff',
    fontFamily: "29LTKaff-SemiBold",
    fontSize: 20,
    marginTop: 50,
    paddingBottom: 40,
  }
})
export default Login;