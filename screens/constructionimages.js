import React, { Component } from "react";
import {
  FlatList,
  ActivityIndicator,
  Slider,
  Text,
  View,
  StyleSheet,
  Button,
  Image,
  Dimensions,
  TouchableOpacity,
  Linking,
  Picker,
  NetInfo,
  ToastAndroid,
  AsyncStorage,
  Animated,
  Easing,
  TouchableWithoutFeedback,
  RefreshControl
} from "react-native";

const { width: WIDTH, height: HEIGHT } = Dimensions.get("window");

import { ScrollView } from 'react-native-gesture-handler';


class ConstructionImages extends React.Component {
    constructor(props) {
        super(props); 
        this.state = {
          isLoading: true,
          dataSource: []
        }; 
      }
      async componentWillMount() {
        this.setState({ 
            dataSource:this.props.navigation.state.params.id || []
        })
      }
    
      
  render() {
    return (
      <ScrollView>
       <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center' }} animation="slideInLeft" duration={1500} iterationCount={1} direction="alternate">
        <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:18, fontWeight:'bold'}} >Construction Images</Text>
      </View>
           
            <ScrollView style={{ paddingStart: 10, paddingTop:20 }} horizontal={false} showsHorizontalScrollIndicator={false}>
              <FlatList pagingEnabled={true} style={{ flexDirection: "column", flex: 1 }} data={this.state.dataSource} renderItem={({ item }) => <View style={{ justifyContent:'center', flexDirection:'row'}}>
                <View style={{ paddingBottom:10, flex: 1, height: null, width: null, resizeMode: "contain", paddingStart: 5, width: WIDTH, height: 200 }}>
                   <Image source={{ uri: item.url }} style={{ marginRight: 15, width: WIDTH - 35, height:200 , flex: 1, borderRadius: 12 }} />
                </View>
 
              </View>} keyExtractor={(item, index) => item.toString()} />
            </ScrollView>
      </ScrollView>
    );
  }
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}
}
export default ConstructionImages;