import React, { Component } from 'react';
import { AsyncStorage, Text, View, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
const { width: WIDTH } = Dimensions.get('window');
import { TextInput } from 'react-native-gesture-handler';
import bgImage from '../src/assets/LoginBg.png';

import * as EmailValidator from 'email-validator';

import Toast, { DURATION } from 'react-native-easy-toast';

class NewAccount extends React.Component {

    constructor(props) {
        super(props);
        this.signUp = this.signUp.bind(this);
        this.state = {
            isLoading: true,
            username: '',
            password: '',
            phone: '',
            email: '',
            verifyEmail: false
        }
    }; 

    static navigationOptions = {
        headerTitle:(<Image style={{marginRight: 56,  width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
      }
    
    
    CheckTextInput = () => {
        let emailCheck = EmailValidator.validate(this.state.email)
         if (this.state.username != '' && (this.state.username.trim().length >= 6)) {
            if (this.state.password != '' && (this.state.username.trim().length >= 4)) {
                if (this.state.phone != '') {
                    if (emailCheck == true) {
                        this.signUp();
                    } else {
                        this.refs.toast.show('Please provide a valid Email!', 500, () => {
                        })
                    }
                } else {
                    this.refs.toast.show('Please Enter Phone Number!', 500, () => {
                    })
                }
            } else {
                this.refs.toast.show('Password must be at least four characters!', 500, () => {
                })
            }
        } else {
            this.refs.toast.show('Username must be at least six characters!', 500, () => {
            })
        }
    };


    _bootstrapAsync = async () => {
    
        await AsyncStorage.setItem('userToken', '');
        const userToken = await AsyncStorage.getItem('userToken');
        this.props.navigation.navigate(userToken ? 'Dashboard' : 'Welcome');
      };

    signUp() {
        fetch('https://bbn-eg.com/broker/api/user/new', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                phone: this.state.phone,
                email: this.state.email
            })

        }).then((response) => response.json())
            .then((responseJson) => {
                this.refs.toast.show(responseJson['message'], 500, () => {
                   this._bootstrapAsync();
                    // this.props.navigation.navigate('Login')
                    
                  //  this.props.navigation.goBack()
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }


    render() {
        return (

            <ImageBackground source={bgImage} style={loginStyles.backgroundContainer}>

                <View style={{ width: WIDTH - 20, height: 50 }}>
                    <Toast
                        ref="toast"
                        style={{ backgroundColor: '#80cbc4', width: WIDTH - 40, height: 50 }}
                        position='top'
                        positionValue={40}
                        fadeInDuration={750}
                        fadeOutDuration={2500}
                        opacity={0.9}
                        textStyle={{ color: 'white', textAlign: 'center', paddingTop: 5 }}
                    />
                </View>


          
                    <View style={{ alignItems: 'center' }}>
                        <Text style={loginStyles.logoText}>S I G N  U P</Text>
                    </View>
         

                    <View>
                        <TextInput
                            style={loginStyles.input}
                            placeholder={'username'}
                            onChangeText={(username) => this.setState({ username: username })}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            underlineColorAndroid='#2CA095'
                       
                        />
                    </View>
                    <View style={{ justifyContent:"center", flexDirection:"row",  }}>
            <View style={{borderBottomColor: '#2CA095', borderBottomWidth: 2, width:WIDTH/2 + 60}} />
            </View>
             
                    <View style={{ paddingTop: 5 }}>
                        <TextInput
                            style={loginStyles.input}
                            placeholder={'password'}
                            onChangeText={(password) => this.setState({ password: password })}
                            secureTextEntry={true}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            underlineColorAndroid='#2CA095'
                       
                        />
                    </View>
                    <View style={{ justifyContent:"center", flexDirection:"row",  }}>
            <View style={{borderBottomColor: '#2CA095', borderBottomWidth: 2, width:WIDTH/2 + 60}} />
            </View>
            
                    <View style={{ paddingTop: 5 }}>
                        <TextInput
                            style={loginStyles.input}
                            placeholder={'phone'}

                            onChangeText={(phone) => this.setState({ phone: phone })}
                            keyboardType='numeric'
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            underlineColorAndroid='#2CA095'
                       
                        />
                    </View>
                    <View style={{ justifyContent:"center", flexDirection:"row",  }}>
            <View style={{borderBottomColor: '#2CA095', borderBottomWidth: 2, width:WIDTH/2 + 60}} />
            </View>
            
                    <View style={{ paddingTop: 5 }}>
                        <TextInput
                            style={loginStyles.input}
                            placeholder={'email'}
                            onChangeText={(email) => this.setState({ email: email })}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            underlineColorAndroid='#2CA095'
                       
                        />
                    </View>
                    <View style={{ justifyContent:"center", flexDirection:"row",  }}>
            <View style={{borderBottomColor: '#2CA095', borderBottomWidth: 2, width:WIDTH/2 + 60}} />
            </View>
             
                    <TouchableOpacity style={loginStyles.btnLogin} onPress={() => this.CheckTextInput()} >
                        <Image style={{ flex: 1, width: null, height: null, resizeMode: 'cover' }} source={require('../src/assets/lgnBtn.png')} />

                    </TouchableOpacity>
            </ImageBackground>
        );
    }
}

const loginStyles = StyleSheet.create({
    newaccount: {
        color: '#ffffff',
        fontSize: 14,
        textAlign: 'center',
        fontFamily: "29LTKaff-Regular",
        paddingTop: 10
    },
    forgetPassword: {
        color: '#ffffff',
        fontSize: 14,
        textAlign: 'center',
        fontFamily: "29LTKaff-Regular",
        paddingTop: 10
    },
    text: {
        color: '#ffffff',
        fontSize: 16,
        textAlign: 'center',
        fontFamily: "29LTKaff-Regular",
        paddingTop: 22
    },
    btnLogin: {
        width: 50,
        height: 50,
        borderRadius: 50,
        backgroundColor: "gray",
        fontFamily: "29LTKaff-SemiBold",
        marginTop: 30
    },
    input: {
        width: WIDTH - 100,
        height: 30,
        paddingTop:25,
        fontSize: 16,
        textAlign: 'center',
        fontFamily: "29LTKaff-Regular",
        marginTop: 5,
        color: '#ffffff',
        justifyContent: "center",
        alignItems: "stretch",
        paddingBottom:0,
     //   borderBottomWidth: 2,
        height: 50,
     //   borderColor: "#2CA095",
        marginHorizontal: 25
    },
    backgroundContainer: {
        flex: 1,
        width: null,
        height: null,
     //   opacity: 0.9,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: "29LTKaff-Regular",
    },
    logoText: {
        color: '#ffffff',
        fontFamily: "29LTKaff-SemiBold",
        fontSize: 20,
        marginTop: 10,
        paddingBottom: 30,
    }
})
export default NewAccount;