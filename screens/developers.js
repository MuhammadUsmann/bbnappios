import React, { Component } from 'react';
import { StatusBar, Platform, Animated, AsyncStorage, Picker, RefreshControl, ActivityIndicator, Text, View, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH, height: HEIGHT } = Dimensions.get("window");
import { FlatGrid } from 'react-native-super-grid';
import Swiper from 'react-native-swiper';
import { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['locations.location', 'title'];

import RNPickerSelect from 'react-native-picker-select';

import * as Animatable from 'react-native-animatable';
const HEADER_MAX_HEIGHT = 230;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class Developers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      isLoading: true,
      banners: [],
      projects: [],
      refreshing: false,
      itemValue: null,
      searchProjects: null,
      searchTerm: '',
      locations:[],
      projectLocation: [
        {
          label: 'Project Location',
          value: '',
        },
        {
          label: 'Cairo, Egypt',
          value: 'Cairo, Egypt',
        },
        {
          label: 'Alexandria, Egypt',
          value: 'Alexandria, Egypt',
        },
        {
          label: 'Luxor, Egypt',
          value: 'Luxor, Egypt',
        },
        {
          label: 'Giza, Egypt',
          value: 'Giza, Egypt',
        },
        {
          label: 'Aswan, Egypt',
          value: 'Aswan, Egypt',
        },
        {
          label: 'Hurgada, Egypt',
          value: 'Hurgada, Egypt',
        },
        {
          label: 'Asyut, Egypt',
          value: 'Asyut, Egypt',
        }
      ]
    };
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }


  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    fetch('https://bbn-eg.com/broker/api/developers?api_token='+userToken)
      .then((response) => response.json())  
      .then((findresponse) => {
        this.setState({
       // banners: findresponse.banners,
          projects: findresponse.developers
        })
      })

      fetch('https://bbn-eg.com/broker/api/banners/developers/?'+userToken)
      .then((response) => response.json())  
      .then((findresponse) => {
        this.setState({
        banners: findresponse,
        })
     //   alert(JSON.stringify(this.state.banners));
      })

      fetch('https://bbn-eg.com/broker/api/projects/locations/?api_token='+userToken)
      .then((response) => response.json())  
      .then((findresponse) => {
        this.setState({
          isLoading: false,
          refreshing: false,
          locations: findresponse,
          })
      })
  }  

  getScreen(item) {
    this.props.navigation.navigate('ProjectDetails', { id: item })
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.componentWillMount();
  }
  
  searchData(id) {
    this.searchUpdated(id);
  }

  bannerDetails(id){
  //  alert(id);
    this.props.navigation.navigate('BannerDetails', { id: id });
    /*
      
    this.loading = Loading.show({
      color: '#2CA095',
      size: 20,
      overlayColor: 'rgba(0,0,0,0.5)',
      closeOnTouch: false,
      loadingType: 'Bars'
    })
    
    fetch('https://bbn-eg.com/broker/api/banners/detail/'+id)
    .then((response) => response.json())  
    .then((findresponse) => {
       
        Loading.hide(this.loading);
    })
    */
  }

  render() {
    const filteredEmails = this.state.projects.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
    const pickerStyle = {
      inputIOS: {
          color: 'black',
          paddingTop: 13,
          paddingHorizontal: 10,
          paddingBottom: 10,
          fontFamily: "29LTKaff-Regular",
          fontSize:14,
          height:38

      },
      inputAndroid: {
          color: 'black',
      },
      placeholderColor: 'grey',
      underline: { borderTopWidth: 0 },
      icon: {
          position: 'absolute',
          backgroundColor: 'transparent',
          borderTopWidth: 5,
          borderTopColor: '#00000099',
          borderRightWidth: 5,
          borderRightColor: 'transparent',
          borderLeftWidth: 5,
          borderLeftColor: 'transparent',
          width: 0,
          height: 10,
          top: 18,
          right: 15,
      },
  };


    const swiperItems = this.state.banners.map(item => {
      return (
        <TouchableOpacity onPress={() => this.bannerDetails(item.id)}>     
        <Image
          source={{ uri: item.photo }}
          style={{ width: WIDTH, height: 230}}
          key={item.id}
        />
        
       </TouchableOpacity>
      )
    })

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#26a69a" />
        </View>
      );
    }
    else
    return (


      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          colors={['#2CA095']}
        />
      }>
      <View style={styles.fill}>
            <Swiper
              key={this.state.banners.length}
              containerStyle={{width: WIDTH, height: HEIGHT / 3}}
              autoplay={true}
              autoplayDelay={5}
              loop={true}
              index={0}
              activeDotColor={'#ffffff'}
              dotColor={'#ffffff'} >
              {swiperItems}
            </Swiper>
      </View>
  <View>

        <Text  style={{ paddingTop: 10, fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 20, marginTop: 5, marginBottom: 10, fontWeight: 'bold', textAlign: 'center' }}>
        <Text>Developers</Text> 
        </Text>


        <View style={{ flexDirection: 'row', fontFamily: "29LTKaff-Regular", justifyContent: 'center', marginBottom: 15 }}>
             <View style={{  justifyContent: 'center', borderBottomLeftRadius: 12, borderTopRightRadius: 12, fontFamily: "29LTKaff-SemiBold", fontWeight:'900', width: 150, height: 30, marginBottom: 2, borderWidth: 1, borderColor: '#2CA095'}}>
            <RNPickerSelect
                                placeholder={{
                                    label: 'Locations',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.locations}
                                onValueChange={(value) => {
                                    this.setState({
                                      searchTerm: value,
                                    });
                                }}
                                onUpArrow={() => {
                                   // this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;

                                    //underline: { opacity: 0 }, justifyContent: 'center', borderRadius: 50, width: WIDTH - 65, height: 40, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' 
                                }}
                            />


            </View>
        </View>

      

        <ScrollView>
            <FlatGrid
              itemDimension={WIDTH / 3 - 10}
              items={filteredEmails}
              
              spacing={0}
              renderItem={({ item }) => (
           
                <TouchableOpacity onPress={() => this.getScreen(item.id)}>
                  <View style={{ marginTop:10, borderWidth: 1, borderColor: '#bdbdbd', alignContent: 'center', flex: 1, width: WIDTH / 3, height: 120 }}>
                    <Image source={{ uri: item.photo }} style={{ flex: 1, resizeMode: 'contain' }} />
                  </View>
                </TouchableOpacity>
              )} />
          </ScrollView>
        </View>
      </ScrollView>


      
      
    );
  }
}

export const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  fill: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',//'#03A9F4',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 18,
  },
  scrollViewContent: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },

  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  child: { 
    height: height * 0.5,
    width,
    justifyContent: 'center'
  },
  text: {
    fontSize: width * 0.5,
    textAlign: 'center'
  }
})
export default Developers;