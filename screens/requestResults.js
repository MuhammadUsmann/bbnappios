import React, { Component } from 'react';
import { TouchableHighlight, FlatList, AsyncStorage, Text, View, Picker, Image, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');

import RNPickerSelect from 'react-native-picker-select';
import * as Animatable from 'react-native-animatable';
import { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['location', 'max_price'];

class RequestResults extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: [], locations: [], projects: [],
      itemValue: null,
      searchTerm: '',
      price: [
        {
          label: '1M - 3M',
          value: '1M - 3M',
        },
        {
          label: '4M - 6M',
          value: '4M - 6M',
        },
        {
          label: '7M - 10M',
          value: '7M - 10M',
        },
        {
          label: '10M - 15M',
          value: '10M - 15M',
        },
        {
          label: '15M - 20M',
          value: '15M - 20M',
        },
        {
          label: '20M - 30M',
          value: '20M - 30M',
        },
        {
          label: '30M - 50M',
          value: '30M - 50M',
        },
        {
          label: '50M - 100M',
          value: '50M - 100M',
        },
        {
          label: '100M - ',
          value: '100M',
        }
      ],
    };
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}




  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    this.state.userToken = await AsyncStorage.getItem('userToken');
    fetch('https://bbn-eg.com/broker/api/requests/?api_token=' + userToken)
      // fetch('https://bbn-eg.com/broker/api/projects/list?api_token='+userToken) //UHK1oavegjOTRvyfpHjRaBxQVRKlWOao006pJi3KZPMXqAjnfkTTPfN0s7Ka'
      .then((response) => response.json())
      .then((findresponse) => {
        this.setState({
          //  isLoading: false,
          dataSource: findresponse.requests,
        })
        //    alert(JSON.stringify(findresponse.requests));
      })

    fetch('https://bbn-eg.com/broker/api/projects/list?api_token=' + userToken)
      // fetch('https://bbn-eg.com/broker/api/projects/list?api_token='+userToken) //UHK1oavegjOTRvyfpHjRaBxQVRKlWOao006pJi3KZPMXqAjnfkTTPfN0s7Ka'
      .then((response) => response.json())
      .then((findresponse) => {
        this.setState({
          isLoading: false,
          projects: findresponse.data
          //       dataSource: findresponse.requests,
        })
     //   alert(JSON.stringify(this.state.projects));
      })
  }

  contactChat(item){
    
    this.props.navigation.navigate('OtherChat', { id: item })
  //  alert(JSON.stringify(item));
  }

  render() {
    const filteredEmails = this.state.dataSource.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
    const pickerStyle = {
      inputIOS: {
          color: 'black',
          paddingTop: 13,
          paddingHorizontal: 10,
          paddingBottom: 10,
          fontFamily: "29LTKaff-Regular",
          fontSize:14,
          height:38

      },
      inputAndroid: {
          color: 'black',
      },
      placeholderColor: 'grey',
      underline: { borderTopWidth: 0 },
      icon: {
          position: 'absolute',
          backgroundColor: 'transparent',
          borderTopWidth: 5,
          borderTopColor: '#00000099',
          borderRightWidth: 5,
          borderRightColor: 'transparent',
          borderLeftWidth: 5,
          borderLeftColor: 'transparent',
          width: 0,
          height: 10,
          top: 18,
          right: 15,
      },
  };
    return (
      <ScrollView>
        <View style={{ flex: 1 }}>
          <Text style={{ backgroundColor: '#dbdcdd', fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center' }} animation="slideInLeft" duration={1500} iterationCount={1} direction="alternate">
            <Text style={{ color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize: 18, fontWeight: 'bold' }} >All Requests</Text>
          </Text>

            <View style={{ flexDirection: 'row', marginTop: 30, marginBottom: 10, marginLeft: 5, justifyContent: 'center', width: WIDTH - 10 }}>
              <View style={{ flexDirection: 'row' }}>

                <View style={{ marginRight: 15, justifyContent: 'center', borderBottomLeftRadius: 12, borderTopRightRadius: 12, fontFamily: "29LTKaff-SemiBold", fontWeight: '900', width: 130, height: 35, marginBottom: 2, borderWidth: 1, borderColor: '#2CA095' }}>
                <RNPickerSelect
                                placeholder={{
                                    label: 'Project',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.projects}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedtype: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={ pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;

                                    //underline: { opacity: 0 }, justifyContent: 'center', borderRadius: 50, width: WIDTH - 65, height: 40, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' 
                                }}
                            />
          
                </View>

                <View style={{ marginLeft: 15, justifyContent: 'center', borderBottomLeftRadius: 12, borderTopRightRadius: 12, fontFamily: "29LTKaff-SemiBold", fontWeight: '900', width: 120, height: 35, marginBottom: 2, marginTop: 2, borderWidth: 1, borderColor: '#2CA095' }}>
                <RNPickerSelect
                                placeholder={{
                                    label: 'Price',
                                    value: '',
                                    color: '#000000',
                                }}
                                items={this.state.price}
                                onValueChange={(value) => {
                                    this.setState({
                                        selectedtype: value,
                                    });
                                }}
                                onUpArrow={() => {
                                    this.inputRefs.name.focus();
                                }}
                                onDownArrow={() => {
                                    //   this.inputRefs.picker2.togglePicker();
                                }}
                                style={ pickerStyle}
                                //    value={this.state.favColor}
                                ref={(el) => {
                                    //  this.inputRefs.picker = el;

                                    //underline: { opacity: 0 }, justifyContent: 'center', borderRadius: 50, width: WIDTH - 65, height: 40, marginBottom: 2, borderWidth: 1, borderColor: '#bdbdbd', backgroundColor: 'white' 
                                }}
                            />
              
                </View>
              </View>
            </View>
            <View>
            </View>
      
            <View   style={{ marginTop: 20, marginBottom: 10}} />



          <FlatList  style={{ flexDirection: "row", flex: 1, backgroundColor:'#f5f5f7'  }} data={filteredEmails} renderItem={({ item }) =>
            

            <View  elevation={3}    style={{  backgroundColor:'white', flexDirection: 'row', paddingTop:5,  marginTop: 5, marginBottom: 5, marginLeft: 0, justifyContent: 'center', width: WIDTH }}>

                   
              <View style={{  backgroundColor:'white',  flexDirection: 'row', justifyContent: "flex-start", width: WIDTH - 25, minHeight:140 }}>
           
                <Text style={{ position: 'absolute', left: 'auto', right: 0, bottom: 'auto', top: 5, paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 12, color: 'black', fontWeight: 'bold', marginRight:25, marginTop:10  }}>{item.requested_on}</Text>



                {this.state.userToken != item.user_token ? (
                 <TouchableOpacity style={{ height:30, width:'auto', position: 'absolute', left: 'auto', right: 0, bottom: 'auto', top:60,  paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 13, color: 'black', fontWeight: 'bold', marginRight:25  }} underlayColor='transparent' onPress={() => this.contactChat(item.user_id)}>
                 <View>
                   <Text style={{ marginStart:40, marginLeft:40, fontFamily: "29LTKaff-Regular", textAlign: 'right', marginTop:8, fontSize: 15, color: 'black', fontWeight: 'bold'}}>{item.user}</Text> 
                   <View style={{ marginEnd:15, backgroundColor:'grey',flexDirection: 'row', justifyContent: 'flex-end', backgroundColor:'#2CA095', height:35, width:35, borderRadius:17, position: 'absolute',  left:0, right:  'auto'}}>
                   <Image source={require('../src/assets/contactbtn.png')} style={{ margin:9, width: 17, height: 16}} />
                 </View>
                 </View>
               </TouchableOpacity>
                ) : 
                
                <View style={{ height:30, width:'auto', position: 'absolute', left: 'auto', right: 0, bottom: 'auto', top:60,  paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 13, color: 'black', fontWeight: 'bold', marginRight:25  }}>
                  <View>
                    <Text style={{ marginStart:15, marginLeft:40, fontFamily: "29LTKaff-Regular", textAlign: 'right', marginTop:6, fontSize: 15, color: 'black', fontWeight: 'bold'}}>{item.user}</Text> 
                  {
                    /*
                        <View style={{ marginEnd:10, backgroundColor:'grey',flexDirection: 'row', justifyContent: 'flex-end', backgroundColor:'#2CA095', height:35, width:35, borderRadius:17, position: 'absolute', left:0, right:  'auto', bottom: 'auto'}}>
                    <Image source={require('../src/assets/contactbtn.png')} style={{ margin:9, width: 17, height: 16}} />
                  </View>
                      */
                  }
                  </View>
                </View>
                
                }


                

      
                <View style={{  width: WIDTH / 2 + 100, minHeight:130 }}>
                <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-SemiBold", fontSize: 17, color: 'black', fontWeight: '900', marginTop:10 }}>{item.type}</Text>
               
                  <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-SemiBold", fontSize: 17, color: 'black', fontWeight: '900', marginBottom:5 }}>{item.project}</Text>     
                
                  <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 15, color: 'black', fontWeight: 'bold', marginTop:2 }}>Price</Text>
                  <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 13, color: 'black', fontWeight: 'normal', lineHeight:15 }}>{item.min_price} - {item.max_price}</Text>


                  <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 15, color: 'black', fontWeight: 'bold', marginTop:2 }}>Size</Text>
                  <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 13, color: 'black', fontWeight: 'normal', lineHeight:15  }}>{item.min_area} - {item.max_area}</Text> 
                 </View>

              </View>
           

            </View>


          
          } keyExtractor={(item, index) => index.toString()} />

        
          
        </View>

      </ScrollView>
    );
  }
}
export default RequestResults;