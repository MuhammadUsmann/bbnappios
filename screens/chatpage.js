import React, { Component } from 'react';
import { TouchableHighlight, RefreshControl, TextInput, FlatList, ScrollView, AsyncStorage, ActivityIndicator, Text, View, StyleSheet, Button, Image, SearchBar, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
const { width: WIDTH } = Dimensions.get("window");

class ChatPage extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource:[],
      refreshing: false,
    }; 
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.componentWillMount();
  } 

  async componentWillMount() {
    const userToken = await AsyncStorage.getItem('userToken');
    fetch('https://bbn-eg.com/broker/api/chats/?api_token=' + userToken)
        .then((response) => response.json())
        .then((findresponse) => {
            this.setState({
                isLoading: false,
                dataSource: findresponse['chats'],
                refreshing: false
            })
      //   alert("Response: " + JSON.stringify(this.state.dataSource));
        })
}


getScreen(item) {

  //alert(item);
  // OtherChat  //SearchedPropertyDetails
  this.props.navigation.navigate('OtherChat', { id: item })
}

render() {
    
  

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
        </View>
      );
    }

    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          colors={['#2CA095']}/>}>

   
   <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:20, fontWeight:'bold',backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, marginBottom:15, fontWeight: 'bold', textAlign: 'center'}} >Chat</Text>
          
      <FlatList listKey="2.1" style={{ flex: 1 }} data={this.state.dataSource} renderItem={({ item }) => <View>
          <View elevation={2} style={{ paddingTop: 5, paddingStart: 5, width: WIDTH - 30, marginBottom:10, marginLeft:15}}>
          <TouchableHighlight
                  onPress={() => this.getScreen(item.sender_id)}
                  underlayColor='transparent'>
           
              <View elevation={2} style={{ backgroundColor:'#f5f5f7', minHeight:70, maxHeight:150, paddingBottom: 10, flexDirection: 'row', justifyContent: 'flex-start', paddingStart: 10, width: WIDTH - 35 }}>
                  <View>
                      <Image source={{ uri: item.photo }} style={{ marginTop:10, padding: 10, width: 52, height: 52, borderRadius: 26 }} />
                  </View>
                  <View style={{ flex: 1, flexWrap: 'wrap' }}>
                      <Text style={{ paddingStart: 15, marginTop: 22, lineHeight:20, fontFamily: "29LTKaff-Regular", fontWeight: 'normal', fontSize: 16, color: 'black' }}><Text style={{ fontFamily: "29LTKaff-SemiBold", fontSize: 18, fontWeight: 'bold', color: 'black' }}>{item.user_name}</Text>{/*  {item.chat} */}
                      </Text>

                      <Text style={{paddingStart:15, lineHeight:14, fontFamily: "29LTKaff-Regular", fontSize:12 }}>{item.dated}</Text>
                  </View>
              </View>
          </TouchableHighlight>
          </View>
      </View>} listKey={(item, index) => 'Nested' + index.toString()} />
  </ScrollView>
    );
  }
}
export default ChatPage;
