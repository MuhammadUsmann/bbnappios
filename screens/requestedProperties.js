import React, { Component } from 'react';
import { FlatList, RefreshControl, TouchableHighlight, ActivityIndicator, Text, View, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');

import StarRating from 'react-native-star-rating';

class RequestedProperties extends React.Component {
  constructor(props) {
    super(props);
      
    this.state =({
        isLoading: true, 
        searchTerm:'',
        refreshing: false,
        dataSource: []
    })
      
  }
  async componentWillMount() {

   // var data  =  this.props.navigation.state.params.id || []
    this.setState({ 
        dataSource:this.props.navigation.state.params.id || [],
        isLoading:false,
        refreshing: false,
    })

  //  alert(JSON.stringify(dataSource));

  }

  getScreen(item) {
    navigationOptions = {
      title: 'Welcome',
    };
    this.props.navigation.navigate('SearchedPropertyDetails', { id: item.id })
  }


  _onRefresh = () => {
    this.setState({refreshing: true});
   
    this.componentWillMount();
  } 

  
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
}

  render() {

  //  alert(JSON.stringify(this.state.dataSource))
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#59BF8F" />
        </View>
      );
    }


    

    return <ScrollView refreshControl={
      <RefreshControl
        refreshing={this.state.refreshing}
        onRefresh={this._onRefresh}
        colors={['#26a69a']}
      />
    }>

        <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:18, fontWeight:'bold', backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, fontWeight: 'bold', textAlign: 'center'}} >Requested Properties</Text>
 
      <ScrollView >

      <FlatList style={{ flexDirection: "row", flex: 1 }} data={this.state.dataSource} renderItem={({ item }) => 
          <View elevation={3} style={{  flexDirection: 'row', marginLeft: 0, justifyContent: 'center', width: WIDTH - 0, height:140, marginBottom:10 }}>
               
          <View style={{ shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 2,
},
shadowOpacity: 0.23,
shadowRadius: 2.62,

elevation: 4, backgroundColor:'#f5f5f7', justifyContent: 'flex-start', flexDirection: 'row', width: WIDTH - 15, height: 'auto', paddingBottom:10 }}>
    
          <TouchableHighlight
          onPress={() => this.getScreen(item)}
          underlayColor='transparent'>
            <View style={{ backgroundColor:'grey',  paddingLeft: 0,  width: 150, height: 135, padding: 0, marginStart:0, marginTop:3, borderRadius:0 }}>
              <Image source={{ uri: item.photo }} style={{ borderRadius: 0, flex: 1, height: 135, width: 150 }} />
            </View>
            </TouchableHighlight>

            <View style={{  marginTop:15, marginStart:0, borderRadius: 0, width: WIDTH/2 + 45, minHeight:120, maxHeight:'auto', height:120, paddingBottom:5 }}>
             
           
              <View >   
           
                  <TouchableHighlight underlayColor='transparent' style={{ marginLeft: 5, height: 'auto', width: WIDTH / 2 - 30}} onPress={() => this.getScreen(item)}>
               

                <View style={{  marginLeft: 10, height: 'auto', width: WIDTH / 2 - 15, paddingBottom: 0 }}>
                 
                <Text style={{fontFamily: "29LTKaff-Regular", fontSize: 14, color: '#141414', fontWeight: 'normal', marginLeft: 0, lineHeight:17, marginTop:16 }}>{item.type}</Text>
                 
                  <Text style={{fontFamily: "29LTKaff-SemiBold", fontSize: 18, color: 'black', fontWeight: 'bold', height:18, marginBottom:5, marginLeft: 0, lineHeight:19, }}>{item.max_price > 0 ? item.max_price : 0} EGP</Text>
                 
                  <Text style={{ height:30, fontFamily: "29LTKaff-Regular", fontSize: 12, color: '#141414', fontWeight: 'normal', marginLeft: 0, lineHeight:15 }}>{item.project}, {item.payment_type}, {item.location}</Text>
                
                
                  <View style={{ flexDirection: 'row', justifyContent: 'flex-start', width:WIDTH/2 - 15, height:30 }}>

                      <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                     
                     
                      <Image style={{position:"absolute", left:0, top:12}}source={require('../src/assets/rooms.png')}  color={'black'} size={22} />
               
                     <Text style={{position:"absolute", left:27, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.rooms > 0 ? item.rooms : 0}</Text>

                      </View>
                      <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                      <Image  style={{position:"absolute", left:0, top:5, height:20}}source={require('../src/assets/washrooms.png')}  color={'black'} size={20} />
               
                     <Text style={{position:"absolute", left:27, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.baths > 0 ? item.baths : 0}</Text>

                      </View>
                      <View style={{ justifyContent: 'flex-start', width:(WIDTH/2 - 15)/3, height:30}}>
                      <Image style={{position:"absolute", left:2, top:8}}source={require('../src/assets/area.png')}  color={'black'} size={22} />
               
               <Text style={{position:"absolute", left:27, top:8, fontSize:16, fontWeight:"bold", fontFamily: "29LTKaff-SemiBold"}}>{item.area > 0 ? item.area : 0}</Text>


                      </View>

                  </View>

                </View> 

               

                </TouchableHighlight>

                </View>
         
               
            </View>
            
          </View>
  

      </View>
        
       } keyExtractor={(item, index) => index.toString()} />
      </ScrollView>
    </ScrollView>
  }
}
export default RequestedProperties;