import React, { Component } from 'react';
import { Platform, Text, View, StyleSheet, Button, Image, SearchBar, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';


import HomePage from '../screens/home';
import PropertiesPage from '../screens/properties';
import DevelopersPage from '../screens/developers';
import PointsPage from '../screens/points';
import ChatPage from '../screens/chatpage';
import ProfilePage from '../screens/profile';
import PropertiesDetailsPage from '../screens/propertydetails';
import ProjectDetailsPage from '../screens/projectdetails';
import AddPropertyPage from '../screens/addproperty';
import RequestInformationPage from '../screens/requestinformation';
import RequestResultsPage from '../screens/requestResults';
import ProjectlocationdetailsPage from '../screens/projectlocationdetails';
import PropertyunitPage from '../screens/propertyunit';

import LoginPage from '../screens/login';
const { width: WIDTH } = Dimensions.get('window');
import {
  createSwitchNavigator,
  createAppContainer,
  createDrawerNavigator,
  createBottomTabNavigator,
  createStackNavigator
} from 'react-navigation';

import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../src/config.json';
const Icons = createIconSetFromFontello(fontelloConfig);




class AfterLogin extends Component {
  render() {
    
   
    return <AppContainer />;
  }
}
export default AfterLogin;

class DashboardScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>DashboardScreen</Text>
      </View>
    );
  }
}

class Help extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
         <Text>Help Page</Text>
      </View>
    );
  }
}

class ContactUs extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>ContactUs Page</Text>
      </View>
    );
  }
}


const Detail = props => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text>Detail</Text>
  </View>
);

const Home = createStackNavigator(
  {
    Home: {
      screen: HomePage,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle:(<Image style={{marginStart:36, width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
          headerRight: (  
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Image style={{marginRight:10, width:24, height: 24, flex: 1 }} resizeMode="contain" source={require('../src/assets/icons/MenuBtn.png')}/>
            </TouchableOpacity>
            //  <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };     
      }  
    },
    Profile:{
      screen:ProfilePage
    },
    Login:{
      screen:LoginPage
    },
    Help:{
      screen:Help
    },
    ContactUs:{
      screen:ContactUs
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);
const Properties = createStackNavigator(
  {
    Properties: {
      screen: PropertiesPage, 
      navigationOptions: ({ navigation }) => {
        return {
          //headerTitle: <LogoTitle />,
          headerTitle:(<Image style={{marginStart:36, width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
          headerTitleStyle:{
            alignContent: 'center',
          },
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    },
    PropertyDetails:{
      screen:PropertiesDetailsPage
    },
    Profile:{
      screen:ProfilePage
    },
    AddProperty:{
      screen:AddPropertyPage
    },
    RequestInformation:{
      screen:RequestInformationPage
    },
    RequestResults: {
      screen:RequestResultsPage
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  } 
);

const PropertyDetails = createStackNavigator(
  {
    PropertyDetails: {
      screen: PropertiesDetailsPage, 
      navigationOptions: ({ navigation }) => {
        return {
          //headerTitle: <LogoTitle />,
          headerTitle:(<Image style={{marginStart:36, width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
          headerTitleStyle:{
            alignContent: 'center',
          },
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const Developers = createStackNavigator(
  {
    Developers: {
      screen: DevelopersPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle:(<Image style={{marginStart:36, width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    },
    ProjectDetails: {
      screen: ProjectDetailsPage
    },
    Projectlocationdetails:{
      screen:ProjectlocationdetailsPage
    },
    Propertyunit:{
      screen:PropertyunitPage
    },

  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const Login = createStackNavigator(
  {
    Login: {
      screen: LoginPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle:(<Image style={{marginStart:36, width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    },
    Home: {
      screen: HomePage
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);


const Points = createStackNavigator(
  {
    Points: {
      screen: PointsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle:(<Image style={{marginStart:36, width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    },
    Detail: {
      screen: Detail,
      
    },
    Profile:{
      screen:ProfilePage
    }

  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);
const Chat = createStackNavigator(
  {
    Chat: {
      screen: ChatPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle:(<Image style={{marginStart:36, width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    },
    Detail: {
      screen: Detail
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }  
);


const DashboardTabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      fontFamily: "Avenir",
      navigationOptions: {
        backgroundColor: 'blue',

        tabBarIcon: ({tintColor}) => <Icons name='home' color={tintColor} size={40}/>,
        tabBarOptions: {
          tabStyle:{
            borderRightColor:'#676767',
            borderRightWidth: 2
        }, 
          style: {  
            backgroundColor: 'black',
            height:75
          },
          labelStyle: { 
            fontSize: 12, 
            paddingTop: 0 ,
            marginBottom: 8,
            fontFamily: "Avenir"
          },
          activeTintColor: '#59BF8F',
          inactiveTintColor: 'white',   
        //  activeBackgroundColor: '#59BF8F',
        
        },
      },
    },
    Properties: {
      screen: Properties, 
   //   screen: RequestResults,
      navigationOptions: {

        tabBarIcon: ({tintColor}) => <Icons name='properties' color={tintColor} size={40}/>,
        tabBarOptions: {
          tabStyle:{
            borderRightColor:'#676767',
            borderRightWidth: 2,
          }, 
          style: {
            backgroundColor: 'black',
            height:75
          },
          labelStyle: {
            fontSize: 12,
            paddingTop: 0 ,
            marginBottom: 8,
            fontFamily: "Avenir"
          },
          activeTintColor: '#59BF8F',
          inactiveTintColor: 'white'
        }
      },
    },
    Developers: { 
      screen: Developers,
    // screen: RequestResults,
//  screen:Projectdetails,
      navigationOptions: { 
        tabBarIcon: ({tintColor}) => <Icons name='developers' color={tintColor} size={40}/>,
        tabBarOptions: {
          tabStyle:{
            borderRightColor:'#676767',
            borderRightWidth: 2,
            elevation:3
          }, 
          style: {
            backgroundColor: 'black',
            height:75
          },
          labelStyle: {
            fontSize: 12,
            paddingTop: 0 ,
            marginBottom: 8,
            fontFamily: "Avenir"
          },
          activeTintColor: '#59BF8F',
          inactiveTintColor: 'white',
        }
      },
    },
    Points: {
      screen: Points,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => <Icons name='points' color={tintColor} size={40}/>,
        tabBarOptions: {
          tabStyle:{
            borderRightColor:'#676767',
            borderRightWidth: 2,
            elevation:3
          }, 
          style: {
            backgroundColor: 'black',
            height:75
          },
          labelStyle: {
            fontSize: 12,
            paddingTop: 0 ,
            marginBottom: 8,
            fontFamily: "Avenir"
          },
          activeTintColor: '#59BF8F',
          inactiveTintColor: 'white'
        }
      },
    },
   
    Chat: {
      screen: ChatPage,
     //screen: Login,
    // screen: Profile,
   //screen:Addproperty,
 //   screen:Requestinformation,
   //screen:Propertydetails,
    //screen:Propertyunit,
  // screen: Projectlocationdetails,
   //   screen:Comments,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => <Icons name='chaticon' color={tintColor} size={40}/>,
        tabBarOptions: {  
          tabStyle:{
            borderRightColor:'#676767',
            borderRightWidth: 2,
            elevation:3
          }, 
          style: {
            backgroundColor: 'black',
            height:75
          },
          labelStyle: {
            fontSize: 12,
            paddingTop: 0 ,
            marginBottom: 8,
            fontFamily: "Avenir"
          },
          activeTintColor: '#59BF8F',
          inactiveTintColor: 'white'
        }
      },  
    },
  
  //  Properties,
  //  Developers,
  //  Points, 
  //  Chat

  },
  {
    navigationOptions: ({ navigation }) => {
      
      const { routeName } = navigation.state.routes[navigation.state.index];
      return {
        header: null,
        headerTitle: routeName
      };
    }
  }
);
const DashboardStackNavigator = createStackNavigator(
  {
    DashboardTabNavigator: DashboardTabNavigator
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerRight: (
          <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
        )
      };
    }
  }
);

const AppDrawerNavigator = createDrawerNavigator({
  
  Dashboard: {
    screen: DashboardStackNavigator
  },
  
  Profile:{
    screen:ProfilePage
  },
  Login:{
    screen:LoginPage
  },
    Help:{
      screen:Help
    },
    ContactUs:{
      screen:ContactUs
    } 
},
  {
    initialRouteName: 'Profile',
    drawerPosition: 'right',
      drawerWidth: WIDTH/2,
      drawerPosition: Platform.OS == 'ios' ? 'left' : 'right',
      contentOptions:{
        labelStyle:{ fontWeight: '600', fontFamily: "Avenir" }
    },
  
  
  });

const AppContainer = createAppContainer(AppDrawerNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});