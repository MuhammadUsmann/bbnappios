import React, { Component } from 'react';
import { AsyncStorage, Text, View, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
const { width: WIDTH } = Dimensions.get('window');
import { TextInput } from 'react-native-gesture-handler';
import bgImage from '../src/assets/LoginBg.png';

import * as EmailValidator from 'email-validator';

import Toast, { DURATION } from 'react-native-easy-toast';
class ForgetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      email: null
    }
  };

  static navigationOptions = {
    headerTitle:(<Image style={{marginRight: 56,  width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
  }
  _bootstrapAsync = async () => {
    
    await AsyncStorage.setItem('userToken', '');
    const userToken = await AsyncStorage.getItem('userToken');
    this.props.navigation.navigate(userToken ? 'Dashboard' : 'Welcome');
  };


  CheckTextInput = () => {
    let emailCheck = EmailValidator.validate(this.state.email)
    if (emailCheck == true) {
      this.resetPassword();
    } else {
      this.refs.toast.show('Please provide a valid Email Address!', 500, () => {
      })
    }
  };




  resetPassword() {
    fetch('https://bbn-eg.com/broker/api/reset', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email
      })

    }).then((response) => response.json())
      .then((responseJson) => {
        this.refs.toast.show(responseJson['message'], 500, () => {
         // this.props.navigation.goBack()
         this._bootstrapAsync();
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <ImageBackground source={bgImage} style={loginStyles.backgroundContainer}>

        <View style={{ width: WIDTH - 20, height: 50 }}>
          <Toast
            ref="toast"
            style={{ backgroundColor: '#80cbc4', width: WIDTH - 40, height: 50 }}
            position='top'
            positionValue={40}
            fadeInDuration={750}
            fadeOutDuration={2500}
            opacity={0.9}
            textStyle={{ color: 'white', textAlign: 'center', paddingTop: 5 }}
          />
        </View>

       
          <View style={{ alignItems: 'center' }}>
            <Text style={loginStyles.logoText}>R E S E T  P A S S W O R D</Text>
          </View>
   
          <View> 
            <TextInput
              style={loginStyles.input}
              placeholder={'email'}
              onChangeText={(email) => this.setState({ email: email })}
              placeholderTextColor={'rgba(255,255,255,0.7)'}
              underlineColorAndroid='#2CA095'
         
            />
          </View>
          <View style={{ justifyContent:"center", flexDirection:"row",  }}>
            <View style={{borderBottomColor: '#2CA095', borderBottomWidth: 2, width:WIDTH/2 + 60}} />
            </View>
  
          <TouchableOpacity style={loginStyles.btnLogin} onPress={() => this.resetPassword()} >
            <Image style={{ flex: 1, width: null, height: null, resizeMode: 'cover' }} source={require('../src/assets/lgnBtn.png')} />

          </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const loginStyles = StyleSheet.create({
  text: {
    color: '#ffffff',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: "29LTKaff-Regular",
    paddingTop: 22
  },
  btnLogin: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: "gray",
    fontFamily: "29LTKaff-SemiBold",
    marginTop: 26
  },
  input: {
    width: WIDTH - 100,
    height: 30,
        paddingTop:25,
    fontSize: 16,

    textAlign: 'center',
    fontFamily: "29LTKaff-Regular",
    marginTop: 5,
    color: '#ffffff',
    paddingBottom:0,
    justifyContent: "center",
    alignItems: "stretch",
    height: 50,
    marginHorizontal: 25
  },
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
   // opacity: 1,
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: "29LTKaff-Regular",
  },
  logoText: {
    color: '#ffffff',
    fontFamily: "29LTKaff-SemiBold",
    fontSize: 20,
    marginTop: 30,
    paddingBottom: 10,
  }
})
export default ForgetPassword;