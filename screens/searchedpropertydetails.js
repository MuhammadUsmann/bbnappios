import React, { Component } from 'react';
import { AsyncStorage, RefreshControl, ToastAndroid, ActivityIndicator, FlatList, Text, View, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
const { width: WIDTH } = Dimensions.get('window');

class SearchedPropertyDetails extends React.Component {
    constructor(props) {
        super(props);
        this.onStarRatingPress = this.onStarRatingPress.bind(this);
        this.state = {
            isLoading: true,
            dataSource: [],
            id: null,
            starCount: null,
            refreshing: false,
            token:''
        };
    }
    static navigationOptions = {
        headerTitle:(<Image style={{marginRight: 56,  width:40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')}/>),
      }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        this.componentWillMount();
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        }, () => {
            fetch('https://bbn-eg.com/broker/api/property/add/rating?api_token='+this.state.token, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: this.state.id,
                    rating: this.state.starCount
                })
            }).then((response) => response.json())
                .then((responseJson) => {
                    ToastAndroid.showWithGravity(
                        responseJson['message'],
                        ToastAndroid.LONG,
                        ToastAndroid.CENTER,
                    );
                    this._onRefresh();
                })
                .catch((error) => {
                    console.error(error);
                });
        });
    }

    async componentWillMount() {
        const userToken = await AsyncStorage.getItem('userToken');
        this.setState({ token: userToken})
        let propertyId = JSON.stringify(this.props.navigation.state.params.id) || {}
        try {
            let response = await fetch(
                "https://bbn-eg.com/broker/api/property/"+propertyId+'?api_token='+userToken
            );
            let responseJson = await response.json();
            this.setState({
                isLoading: false,
                refreshing: false,
                dataSource: responseJson,
                starCount: responseJson[0]['rating'],
                id: propertyId
            });

           // alert(JSON.stringify(this.state.dataSource));

        } catch (error) {
            alert(error);
        }
    }

    contact(item) {
      // alert(item);
        this.props.navigation.navigate('OtherChat', { id: item })
    }

    static navigationOptions = {
        headerTitle: (<Image style={{ marginRight: 56, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('../src/assets/AppLogo.png')} />),
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', justifyContent: 'space-around', padding: 10 }} size="large" color="#2CA095" />
                </View>
            );
        }
        return (
            <ScrollView refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    colors={['#26a69a']}
                />
            }>
             
                <FlatList data={this.state.dataSource} renderItem={({ item }) =>
                    <View>
                            <View style={{ width: WIDTH, height: 200 }}>
                                <Image source={{ uri: item.photos[0] }} style={{ backgroundColor: 'grey', flex: 1, width: WIDTH, height: 'auto' }} />
                            </View>
                     
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 0 }}>
                                <View style={{ backgroundColor: '#f1f1f1' , borderRadius: 12, width: WIDTH }}>
                                   {
                                       /*
                                             <View style={{ paddingTop:10, width: 80, position: 'absolute', left: 'auto', right: 10, top: 0, zIndex: 30 }}>
                                        <StarRating
                                            rating={this.state.starCount}
                                            reversed={true}
                                            starSize={20}
                                            disabled={false}
                                            maxStars={5}
                                            selectedStar={(rating) => this.onStarRatingPress(rating)}
                                            emptyStarColor={'#2CA095'}
                                            halfStarColor={'#2CA095'}
                                            fullStarColor={'#2CA095'}
                                        />
                                    </View>
                                       */
                                   }

                                    <View style={{ backgroundColor: '#f1f1f1' }}>
                                        <Text style={{ marginTop:10, paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 16, color: 'black', fontWeight: '900' }}>{item.title}</Text>
                                        <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 15, color: 'black', fontWeight: '400', marginLeft: 0 }}>{item.min_price} - {item.max_price}</Text>
                                        <Text style={{ paddingStart: 6, fontFamily: "29LTKaff-Regular", fontSize: 15, color: 'black', fontWeight: 'normal', marginLeft: 0 }}>{item.location}</Text>
                                    </View>
                                </View>
                            </View>
                      
                            <View style={{ backgroundColor: '#f1f1f1', paddingTop: 20, flexDirection: 'row', width: WIDTH, height: 'auto' }}>
                                <View style={{ justifyContent: 'flex-start', width: WIDTH / 2 }}>
                                    <Text style={{ padding: 5, paddingStart: 50, fontFamily: "29LTKaff-Regular", fontSize: 15, color: 'black', fontWeight: 'normal' }}>{item.rooms} Rooms</Text>
                                </View>
                                <View style={{ justifyContent: 'flex-start', width: WIDTH / 2 }}>
                                    <Text style={{ padding: 5, paddingStart: 20, fontFamily: "29LTKaff-Regular", fontSize: 15, color: 'black', fontWeight: 'normal', }}>{item.type}</Text>
                                </View>
                            </View>

                            <View style={{ backgroundColor: '#f1f1f1', flexDirection: 'row', width: WIDTH, height: 'auto' }}>
                                <View style={{ justifyContent: 'flex-start', width: WIDTH / 2 }}>
                                    <Text style={{ padding: 0, paddingStart: 50, fontFamily: "29LTKaff-Regular", fontSize: 15, color: 'black', fontWeight: 'normal' }}>{item.baths} Bathrooms</Text>
                                </View>
                            </View>
                            <Text style={{ marginTop: 10, fontFamily: "29LTKaff-SemiBold", color: 'black', fontSize: 18, marginBottom: 5, fontWeight: 'bold', textAlign: 'center', backgroundColor: 'transparent' }}>
                                Details
                            </Text>
                     
                            <View style={{ backgroundColor: '#f1f1f1', borderWidth: 0.5, marginTop: 10, borderColor: '#bdbdbd', flexDirection: 'row', justifyContent: 'center', width: WIDTH, height: 40, paddingStart: 30 }}>
                                <View style={{ width: WIDTH / 2 - 30 }}>
                                    <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>Size</Text>
                                </View>
                                <View style={{ width: WIDTH / 2 + 30 }}>
                                    <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 40 }}>{item.size} m. </Text>
                                </View>
                            </View>
                       
                          <View style={{ backgroundColor: '#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', justifyContent: 'center', width: WIDTH, height: 40, paddingStart: 30 }}>
                                <View style={{ width: WIDTH / 2 - 30 }}>
                                    <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>Land Area</Text>
                                </View>
                                <View style={{ width: WIDTH / 2 + 30 }}>
                                    <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 40 }}>{item.area} m. </Text>
                                </View>
                            </View>
                   
                       <View style={{ backgroundColor: '#f1f1f1', borderWidth: 0.5, borderColor: '#bdbdbd', flexDirection: 'row', justifyContent: 'center', width: WIDTH, height: 40, paddingStart: 30 }}>
                                <View style={{ width: WIDTH / 2 - 30 }}>
                                    <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 30 }}>BUA</Text>
                                </View>
                                <View style={{ width: WIDTH / 2 + 30 }}>
                                    <Text style={{ fontFamily: "29LTKaff-Regular", color: 'grey', fontSize: 14, paddingBottom: 10, paddingTop: 10, fontWeight: '400', textAlign: 'left', paddingStart: 40 }}>{(item.size * 100) / item.area} %</Text>
                                </View>
                            </View>
                    

                            <Text style={{ backgroundColor: 'transparent', marginTop: 10, marginBottom:10, fontFamily: "Avenir", color: 'black', fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>
                                Property Description
                            </Text>
                    
                            <View style={{ backgroundColor: '#f1f1f1', width: WIDTH, marginBottom: 10, textAlign: 'justify', padding: 30, margin: 0 }}>
                                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, fontWeight: 'normal' }}>
                                    {item.description}
                                </Text>
                            </View>
                  
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => this.contact(item.contact_id)} style={style.btnLogin} >
                                    <Text style={{ fontFamily: "29LTKaff-SemiBold", fontSize: 18, fontWeight: 'normal', color: 'black' }}>Contact</Text>
                                </TouchableOpacity>
                            </View>
                    </View>
                }
                    keyExtractor={(item, index) => index.toString()} />
           </ScrollView>
        );
    }
}

const style = StyleSheet.create({
    btnLogin: {
        width: 100,
        height: 40,
        borderBottomLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: "#2CA095",
    fontFamily: "29LTKaff-SemiBold",
        marginTop: 10,
        marginBottom: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1
    },
    navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 1,
        paddingLeft: 155,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    rightNav: {

    }
});
export default SearchedPropertyDetails;