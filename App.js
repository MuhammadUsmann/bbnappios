import React, { Component } from 'react';
import {TouchableHighlight, AsyncStorage, ActivityIndicator, StatusBar, Text, View, StyleSheet, Button, Image, Platform, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LoginPage from './screens/login';
import HomePage from './screens/home';
import PropertiesPage from './screens/properties';
import DevelopersPage from './screens/developers';
import PointsPage from './screens/points';
import ChatPage from './screens/chatpage';
import ProfilePage from './screens/profile';
import PropertiesDetailsPage from './screens/propertydetails';
import ProjectDetailsPage from './screens/projectdetails';
import AddPropertyPage from './screens/addproperty';
import RequestInformationPage from './screens/requestinformation';
import RequestResultsPage from './screens/requestResults';
import ProjectlocationdetailsPage from './screens/projectlocationdetails';
import PropertyunitPage from './screens/propertyunit';
import AfterLoginPage from './screens/afterlogin';
import ForgetPasswordPage from './screens/forgetpassword';
import NewAccountPage from './screens/newaccount';
import AvailablePdfPage from './screens/availablepdf';
import AvailableVideosPage from './screens/availableVideos';
import ConstructionImagesPage from './screens/constructionimages';
import ExcelAvailablityPage from './screens/excelAvailablity';
import RequestedPropertiesPage from './screens/requestedProperties';
import SearchedPropertyDetailsPage from './screens/searchedpropertydetails';
import editpropertyPage from './screens/editproperty';

import BannerDetailsPage from './screens/bannerdetails';

import OtherChatPage from './screens/OtherChatPage';


import CommentsPage from './screens/comments';

import CustomDrawerContentComponent from './screens/drawerContentComponents';


import * as Animatable from 'react-native-animatable';

const { width: WIDTH } = Dimensions.get('window');
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './src/config.json';
const Icons = createIconSetFromFontello(fontelloConfig);



import PotdPage from './screens/potd';


import {
  createSwitchNavigator,
  createAppContainer,
  createDrawerNavigator,
  createBottomTabNavigator,
  createStackNavigator,
  DrawerItems
} from 'react-navigation';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:'',
      image:''
    };
  }
  async componentWillMount(){
    
    const userToken = await AsyncStorage.getItem('userToken');

    if(userToken){
      try { 
        let response = await fetch(
          "http://bbn.flash-lead.com/broker/api/user?api_token="+userToken
        );
        let responseJson = await response.json();
        this.setState({
          name:responseJson[0]['full_name'],
          image:responseJson[0]['photo'],
          dataSource: responseJson,
        });
      } catch (error) {
        console.error(error);
      }

    }
    else{
     // alert("Can't get Token");
    }
    
  } 

  render() {
    return <AppContainer />;
  }
}
export default App;

class Logout extends React.Component {
  constructor() {
    super();
    this._signOutAsync();
  }
  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('AuthLoading');
  };
  render() {
    return (<View> 
      
       </View>);
  }
}

class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');
    this.props.navigation.navigate(userToken ? 'Dashboard' : 'Welcome');
  };
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

class Help extends Component {
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
}
  render() {
   return (
       <View>
         <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:20, fontWeight:'bold',backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, marginBottom:15, fontWeight: 'bold', textAlign: 'center'}} >Help</Text>
   
       
              <View style={{ backgroundColor: '#FAF9F9', width: WIDTH, marginBottom: 10, textAlign: 'justify', padding: 30, margin: 0 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, fontWeight: 'normal' }}>
                  
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                

                </Text>
              </View>
        </View>
    );
  }
}
class ContactUs extends Component {
  static navigationOptions = {
    headerTitleStyle: { justifyContent: 'center' },
    headerTitle: (<Image style={{ marginBottom:20, flex: 1, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
}
  constructor(props) {
    super(props);
  
  }
  render() {
 
    return (
      <View>

        <Text style={{color: 'black', fontFamily: "29LTKaff-SemiBold", fontSize:20, fontWeight:'bold',backgroundColor:'#dbdcdd',  fontSize: 18, padding: 12, paddingBottom: 10, paddingTop: 10, marginBottom:15, fontWeight: 'bold', textAlign: 'center'}} >Contact Us</Text>
   
       
              <View style={{ backgroundColor: '#FAF9F9', width: WIDTH, marginBottom: 10, textAlign: 'justify', padding: 30, margin: 0 }}>
                <Text style={{ fontFamily: "29LTKaff-Regular", color: 'black', fontSize: 14, fontWeight: 'normal' }}>
                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.   
                </Text>
              </View>
        </View>
    );
  }
}

class WelcomeScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button title="Login" onPress={() => this.props.navigation.navigate('Dashboard')} />
        <Button title="Sign Up" onPress={() => alert('button pressed')} />
      </View>
    );
  }
}

class DashboardScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>DashboardScreen</Text>
      </View>
    );
  }
}

class Feed extends Component {
  render() {
    return (
      <HomePage />
    );
  }
}

class Settings extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Settings</Text>
      </View>
    );
  }
}

class Profile extends Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Profile</Text>
      </View>
    );
  }
}

const Detail = props => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text>Detail</Text>
  </View>
);

const Home = createStackNavigator(
  {
    Home: {
      screen: HomePage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerRight: (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Image style={{ marginBottom:20, marginRight: 15, width: 24, height: 24, flex: 1 }} resizeMode="contain" source={require('./src/assets/icons/MenuBtn.png')} />
            </TouchableOpacity>
          ),
          headerLeft: (
              <Image style={{ marginBottom:20, marginLeft: 15, width: 35, height: 40, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />
          )
        }; 
      }
    },
    Profile: {
      screen: ProfilePage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    Login: {
      screen: LoginPage
    },
    Help: {
      screen: Help,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
            headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    ContactUs: {
      screen: ContactUs,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    ProjectDetails: {
      screen: ProjectDetailsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerTitle: (<Image style={{ marginBottom:20, marginRight: 56, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    Projectlocationdetails: {
      screen: ProjectlocationdetailsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    Propertyunit: {
      screen: PropertyunitPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
         headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    PropertyDetails: {
      screen: PropertiesDetailsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
           headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    AvailablePdf: {
      screen: AvailablePdfPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    AvailableVideos: {
      screen: AvailableVideosPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    ConstructionImages: {
      screen: ConstructionImagesPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    ExcelAvailablity: {
      screen: ExcelAvailablityPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    BannerDetails: {
      screen: BannerDetailsPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095'
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);
const Properties = createStackNavigator(
  {
    Properties: {
      screen: PropertiesPage,
      navigationOptions: ({ navigation }) => {
        return {
        //  headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
            <Image style={{ marginBottom:20, marginLeft: 15, width: 35, height: 40, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />
        ), 
        headerTitleStyle: {
            alignContent: 'center',
          },
          headerRight: (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Image style={{ marginBottom:20, marginRight: 15, width: 24, height: 24, flex: 1 }} resizeMode="contain" source={require('./src/assets/icons/MenuBtn.png')} />
            </TouchableOpacity>
          ),
        };
      }
    },
    PropertyDetails: {
      screen: PropertiesDetailsPage,
      navigationOptions: ({ navigation }) => {
        return {
           headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    Profile: {
      screen: ProfilePage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    AddProperty: {
      screen: AddPropertyPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
           headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    RequestInformation: {
      screen: RequestInformationPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    RequestResults: {
      screen: RequestResultsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerTitle: (<Image style={{ marginRight: 56, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    RequestResults: {
      screen: RequestResultsPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    EditProperty: {
      screen: editpropertyPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    RequestedProperties: {
      screen: RequestedPropertiesPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    SearchedPropertyDetails: {
      screen: SearchedPropertyDetailsPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    /*
        Chat: {
      screen: ChatPage,
      navigationOptions: ({ navigation }) => ({
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginLeft: 10, width: 35, height: 40, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    */
    OtherChat: {
      screen: OtherChatPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },

  },
  { 
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const PropertyDetails = createStackNavigator(
  {
    PropertyDetails: {
      screen: PropertiesDetailsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095'
          },
          headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerTitleStyle: {
            alignContent: 'center',
          },
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);


const AfterLogin = createStackNavigator(
  {
    AfterLogin: {
      screen: AfterLoginPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerTitleStyle: {
            alignContent: 'center',
          },
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const Developers = createStackNavigator(
  {
    Developers: {
      screen: DevelopersPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
              <Image style={{ marginBottom:20,marginLeft: 15, width: 35, height: 40, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />
          ), 
          //headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerRight: (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Image style={{ marginBottom:20,marginRight: 15, width: 24, height: 24, flex: 1 }} resizeMode="contain" source={require('./src/assets/icons/MenuBtn.png')} />
            </TouchableOpacity>
          ),
        };
      }
    },
    ProjectDetails: {
      screen: ProjectDetailsPage,
      navigationOptions: ({ navigation }) => {
        
        return {
          headerStyle: {
            backgroundColor: '#2CA095'
          },
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20,marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    Projectlocationdetails: {
      screen: ProjectlocationdetailsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095'
          },
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20,marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    Propertyunit: {
      screen: PropertyunitPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095'
          },
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginBottom:20,marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    AvailablePdf: {
      screen: AvailablePdfPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095'
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20,marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    AvailableVideos: {
      screen: AvailableVideosPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095'
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20,marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    ConstructionImages: {
      screen: ConstructionImagesPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095'
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20,marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    ExcelAvailablity: {
      screen: ExcelAvailablityPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095'
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20,marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    Comments: {
      screen: CommentsPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095',
          height:40
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginBottom:20, marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
    BannerDetails: {
      screen: BannerDetailsPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095'
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    }

  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const Login = createStackNavigator(
  {
    Login: {
      screen: LoginPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const NewAccount = createStackNavigator(
  {
    Login: {
      screen: NewAccountPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerRight: (
            <Icon style={{ paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#59BF8F'} />
          )
        };
      }
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const ForgetPassword = createStackNavigator(
  {
    Login: {
      screen: ForgetPasswordPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerTitle: (<Image style={{ marginBottom:20, marginStart: 56, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerRight: (
            <Icon style={{ marginBottom:20, paddingRight: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} color={'#ffffff'} />
          )
        };
      }
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);




const Potd = createStackNavigator(
  {
    Potd: {
      screen: PotdPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
              <Image style={{ marginBottom:20,marginLeft: 15, width: 35, height: 40, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />
          ), 
          //headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerRight: (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Image style={{ marginBottom:20,marginRight: 15, width: 24, height: 24, flex: 1 }} resizeMode="contain" source={require('./src/assets/icons/MenuBtn.png')} />
            </TouchableOpacity>
          ),
        };
      }
    },
    PropertyDetails: {
      screen: PropertiesDetailsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095'
          },
          headerTitle: (<Image style={{ marginRight: 56, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
          headerLeft: (
            <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
              <Image style={{ marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
            </TouchableHighlight>
          )
        };
      }
    },
    OtherChat: {
      screen: OtherChatPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095'
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
); 



const Points = createStackNavigator(
  {
    Points: {
      screen: PointsPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
              <Image style={{ marginBottom:20, marginLeft: 15, width: 35, height: 40, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />
          ), 
        //  headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
        headerRight: (
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Image style={{ marginBottom:20, marginRight: 15, width: 24, height: 24, flex: 1 }} resizeMode="contain" source={require('./src/assets/icons/MenuBtn.png')} />
          </TouchableOpacity>
        ),
        };
      }
    },
    Detail: {
      screen: Detail,

    },
    Profile: {
      screen: ProfilePage
    }

  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
); 

const Chat = createStackNavigator(
  {
    Chat: {
      screen: ChatPage,
      navigationOptions: ({ navigation }) => {
        return {
          headerStyle: {
            backgroundColor: '#2CA095',
            height:40
          },
          headerLeft: (
              <Image style={{ marginBottom:20, marginLeft: 15, width: 35, height: 40, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />
          ),  
         // headerTitle: (<Image style={{ marginStart: 36, width: 40, height: 42, flex: 1 }} resizeMode="contain" source={require('./src/assets/AppLogo.png')} />),
         headerRight: (
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Image style={{ marginBottom:20, marginRight: 15, width: 24, height: 24, flex: 1 }} resizeMode="contain" source={require('./src/assets/icons/MenuBtn.png')} />
          </TouchableOpacity>
        ),
        };
      }
    },
    Detail: {
      screen: Detail,

    },
    Profile: {
      screen: ProfilePage
    }, OtherChat: {
      screen: OtherChatPage,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#2CA095'
        },
        headerLeft: (
          <TouchableHighlight underlayColor='transparent' onPress={() => navigation.goBack(null)} >
            <Image style={{ marginLeft: 10, width: 20, height: 28, flex: 1 }} resizeMode="contain" source={require('./src/assets/BackBtn.png')} />
          </TouchableHighlight>
        )
      })
    },

  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const DashboardTabNavigator = createBottomTabNavigator(
  {
    
    Home: {
      screen: Home,
      fontFamily: "29LTKaff-Regular",
      navigationOptions: {
        backgroundColor: 'blue',
        tabBarIcon: ({ tintColor }) => (tintColor == '#FFFFFF' ?
      <Image
        source={require('./src/assets/tabicons/HomeIconactive.png')}
        style={[styles.icon, { width:22, height:22, tintColor: tintColor}]}
      />
      :
      <Image
        source={require('./src/assets/tabicons/HomeIconinactive.png')}
        style={[styles.icon, { width:22, height:22, tintColor: tintColor}]}
      />
    ),
       // tabBarIcon: ({ tintColor }) => <Icons name='homeiconactive' color={tintColor} size={25} />,
        tabBarOptions: {
          
          style: {
            backgroundColor: '#26A69A',
            height: 56,
            paddingTop:8,
          },
          labelStyle: {
            fontSize: 11,
            
            paddingTop: 5,
            
           marginBottom: 2,
            fontFamily: "29LTKaff-Regular"
          },
          activeTintColor: '#FFFFFF',
          inactiveTintColor: '#F1F1F1',
        },
      },
    },
    Properties: {
      screen: Properties,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (tintColor == '#FFFFFF' ?
        <Image
          source={require('./src/assets/tabicons/PropertyIconsActive.png')}
          style={[styles.icon, { width:22, height:22, tintColor: tintColor}]}
        />
        :
        <Image
          source={require('./src/assets/tabicons/PropertyIconsinActive.png')}
          style={[styles.icon, { width:22, height:22, tintColor: tintColor}]}
        />
      ),
        tabBarOptions: {
          style: {
            backgroundColor: '#26A69A',
            height: 56,
            paddingTop:8,
          },
          labelStyle: {
            fontSize: 11,
            
            paddingTop: 5,
            
           marginBottom: 2,
            fontFamily: "29LTKaff-Regular"
          },
          activeTintColor: '#FFFFFF',
          inactiveTintColor: '#F1F1F1',
        }
      },
    },
    Developers: {
      screen: Developers,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (tintColor == '#FFFFFF' ?
        <Image
          source={require('./src/assets/tabicons/developersactive.png')}
          style={[styles.icon, { width:22, height:22, tintColor: tintColor}]}
        />
        :
        <Image
          source={require('./src/assets/tabicons/developersinactive.png')}
          style={[styles.icon, { width:22, height:22, tintColor: tintColor}]}
        />
      ),
        tabBarOptions: {
          style: {
            backgroundColor: '#26A69A',
            height: 56,
            paddingTop:8,
          },
          labelStyle: {
            fontSize: 11,
            
            paddingTop: 5,
            
           marginBottom: 2,
            fontFamily: "29LTKaff-Regular"
          },
          activeTintColor: '#FFFFFF',
          inactiveTintColor: '#F1F1F1',
        }
      },
    },
    POTDs: {
        screen: Potd,
       
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (tintColor == '#FFFFFF' ?
          <Image
            width={25} height={25}
           // source={require('./src/assets/tabicons/PointsIconactive.png')}
           
           source={require('./src/assets/tabicons/propertyActive.png')}
            style={[styles.icon, { width:22, height:22, tintColor: tintColor}]}
          />
          :
          <Image
            //source={require('./src/assets/tabicons/PointsIconinactive.png')}
            source={require('./src/assets/tabicons/propertyinActive.png')}
            
            style={[styles.icon, { width:22, height:22, tintColor: tintColor}]}
          />
        ),
          tabBarOptions: {
            style: {
              backgroundColor: '#26A69A',
              height: 56,
              paddingTop:8,
            },
            labelStyle: {
              fontSize: 11,
              
              paddingTop: 5,
              
             marginBottom: 2,
              fontFamily: "29LTKaff-Regular"
            },
            activeTintColor: '#FFFFFF',
            inactiveTintColor: '#F1F1F1'
          }
        },
    },
     
    Chat: { 
      screen: Chat,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (tintColor == '#FFFFFF' ?
        <Image
          width={25} height={25}
          source={require('./src/assets/tabicons/chatactive.png')}
          style={[styles.icon, {  width:21, height:20, tintColor: tintColor}]}
        />
        :
        <Image 
          source={require('./src/assets/tabicons/chatinactive.png')}
          style={[styles.icon, {  width:21, height:20, tintColor: tintColor}]}
        />
      ),
        tabBarOptions: {
          style: {
            backgroundColor: '#26A69A',
            height: 56,
            paddingTop:8,
          },
          labelStyle: {
            fontSize: 11,
            
            paddingTop: 5,
            
           marginBottom: 2,
            fontFamily: "29LTKaff-Regular"
          },
          activeTintColor: '#FFFFFF',
          inactiveTintColor: '#F1F1F1'
        }
      },
    },
  },   
  {
    navigationOptions: ({ navigation }) => {
      const { routeName } = navigation.state.routes[navigation.state.index];
      return {
        header: null,
        headerTitle: routeName
      };
    }
  }
);

const DashboardStackNavigator = createStackNavigator(
  {
    DashboardTabNavigator: DashboardTabNavigator
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerLeft: (
          <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
        )
      };
    }
  }
);

const AppDrawerNavigator = createDrawerNavigator(
  {
  Home: {
    screen: DashboardStackNavigator
  },
  Profile: {
    screen: ProfilePage
  },
  Help: {
    screen: Help
  }, 
  ContactUs: {
    screen: ContactUs
  },
  Logout: {
    screen: Logout
  }
}, 
  {
    drawerPosition: 'right',
    drawerWidth: WIDTH / 2, 
    
    contentComponent: CustomDrawerContentComponent, 
   
    drawerPosition: Platform.OS == 'ios' ? 'right' : 'right',
    contentOptions: {
      labelStyle: { fontWeight: '600', fontFamily: "29LTKaff-Regular" }
    },
  });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
});
 

const AppSwitchNavigator = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Welcome: { screen: LoginPage },
  ForgetPassword: { screen: ForgetPasswordPage },
  NewAccount: { screen: NewAccountPage },
  Dashboard: { screen: AppDrawerNavigator }
},
  {
    initialRouteName: 'AuthLoading',
  });

const AppContainer = createAppContainer(AppSwitchNavigator);
